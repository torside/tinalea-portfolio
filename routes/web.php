<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('/work');
})
    ->name('default');

Route::group(['prefix' => 'auth'], function () {
    Auth::routes([
        'register' => false
    ]);

    Route::get('/images/work-preview/{idWork}', 'Auth\Images\WorkPreviewController@index')
        ->name('images.work-preview')
        ->middleware('auth');

    Route::get('/home', 'Auth\HomeController@index')
        ->name('home')
        ->middleware('auth');

    Route::get('/watermarks', function () {
        return view('auth.watermarks');
    })
        ->name('auth.watermarks')
        ->middleware('auth');

    Route::get('/work-sections', function () {
        return view('auth.work-sections');
    })
        ->name('auth.work-sections')
        ->middleware('auth');

    Route::get('/work-categories', function () {
        return view('auth.work-categories');
    })
        ->name('auth.work-categories')
        ->middleware('auth');

    Route::get('/work', function () {
        return view('auth.work');
    })
        ->name('auth.work')
        ->middleware('auth');

    Route::get('/work-editor', function () {
        return view('auth.work-editor');
    })
        ->name('auth.work-editor')
        ->middleware('auth');
});

Route::group(['prefix' => 'api'], function () {
    Route::get('/auth/get-user', 'Api\AuthController@getUser');

    Route::post('/watermark/update', 'Api\WatermarkController@update')->middleware(['appState.update']);
    Route::post('/watermark/list', 'Api\WatermarkController@list');
    Route::post('/watermark/upload-image', 'Api\WatermarkController@uploadImage')->middleware(['appState.update']);

    Route::get('/work/index/{idWork}', 'Api\WorkController@index');
    Route::post('/work/create', 'Api\WorkController@create')->middleware(['appState.update']);
    Route::post('/work/update', 'Api\WorkController@update')->middleware(['appState.update']);
    Route::post('/work/delete', 'Api\WorkController@delete')->middleware(['appState.update']);
    Route::post('/work/list', 'Api\WorkController@list');
    Route::get('/work/portfolio', 'Api\WorkController@portfolio');
    Route::post('/work/upload-image', 'Api\WorkController@uploadImage')->middleware(['appState.update']);
    Route::post('/work/generate-formats', 'Api\WorkController@generateFormats');

    Route::post('/work-category/create', 'Api\WorkCategoryController@create')->middleware(['appState.update']);
    Route::post('/work-category/update', 'Api\WorkCategoryController@update')->middleware(['appState.update']);
    Route::post('/work-category/list', 'Api\WorkCategoryController@list');

    Route::post('/work-section/create', 'Api\WorkSectionController@create')->middleware(['appState.update']);
    Route::post('/work-section/update', 'Api\WorkSectionController@update')->middleware(['appState.update']);
    Route::post('/work-section/list', 'Api\WorkSectionController@list');
});

Route::get('/{pageSlug}/{categorySlug?}/{sectionSlug?}/{idWork?}', 'Portfolio')
    ->where('pageSlug', 'work|about|contact');

Route::fallback(function () {
    return redirect('/work');
});
