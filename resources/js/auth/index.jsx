import 'bootstrap';
import React from 'react';
import ReactDOM from 'react-dom';
import { QueryParamProvider } from 'use-query-params';
import Watermarks from './components/Watermarks/index.jsx';
import WorkCategories from './components/WorkCategories/index.jsx';
import WorkSections from './components/WorkSections/index.jsx';
import Work from './components/Work/index.jsx';
import WorkEditor from './components/WorkEditor/index.jsx';

const csrfTokenMeta = document.head.querySelector('meta[name="csrf-token"]');

let csrfToken = '';

if (csrfTokenMeta) {
    csrfToken = csrfTokenMeta.content
}

if (document.getElementById('watermarks')) {
    ReactDOM.render(
        <QueryParamProvider>
            <Watermarks csrfToken={csrfToken} />
        </QueryParamProvider>,
        document.getElementById('watermarks')
    );
}

if (document.getElementById('work-categories')) {
    ReactDOM.render(
        <QueryParamProvider>
            <WorkCategories />
        </QueryParamProvider>,
        document.getElementById('work-categories')
    );
}

if (document.getElementById('work-sections')) {
    ReactDOM.render(
        <QueryParamProvider>
            <WorkSections />
        </QueryParamProvider>,
        document.getElementById('work-sections')
    );
}

if (document.getElementById('work')) {
    ReactDOM.render(
        <QueryParamProvider>
            <Work csrfToken={csrfToken} />
        </QueryParamProvider>,
        document.getElementById('work')
    );
}

if (document.getElementById('work-editor')) {
    ReactDOM.render(
        <QueryParamProvider>
            <WorkEditor />
        </QueryParamProvider>,
        document.getElementById('work-editor')
    );
}
