import React, { useState } from 'react';
import axios from 'axios';
import * as PropTypes from 'prop-types';
import Toggle from 'react-toggle';

const WorkCategoryListItem = props => {
    const {
        loadWorkCategoryList,
        loading,
        setLoading,
        idWorkCategory,
        workCategorySlug
    } = props;

    const [workCategoryName, setWorkCategoryName] = useState(props.workCategoryName);
    const [active, setActive] = useState(props.active);
    const [order, setOrder] = useState(props.order);

    const updateWorkCategoryItem = data => {
        setLoading(true);

        axios.post('/api/work-category/update', data)
            .then(() => {
            })
            .catch(() => {
            })
            .then(() => {
                setLoading(false);
                loadWorkCategoryList();
            });
    };

    const onActiveChange = e => {
        const checked = e.target.checked === true;

        setActive(checked);

        updateWorkCategoryItem({
            idWorkCategory: idWorkCategory,
            active: checked
        });
    };

    const onOrderKeyDown = e => {
        if (e.keyCode === 13) {
            e.target.blur();
        }
    };

    const onOrderChange = e => setOrder(parseInt(e.target.value));

    const onOrderBlur = e => updateWorkCategoryItem({
        idWorkCategory: idWorkCategory, order: parseInt(e.target.value)
    });

    const onWorkCategoryNameKeyDown = e => {
        if (e.keyCode === 13) {
            e.target.blur();
        }
    };

    const onWorkCategoryNameChange = e => setWorkCategoryName(e.target.value);

    const onWorkCategoryNameBlur = e => updateWorkCategoryItem({
        idWorkCategory: idWorkCategory,
        workCategoryName: e.target.value
    });

    return (
        <div className="row mb-2">
            <div className="col-4">
                <input
                    disabled={loading}
                    className="form-control"
                    type="text"
                    value={workCategoryName}
                    onKeyDown={e => onWorkCategoryNameKeyDown(e)}
                    onChange={e => onWorkCategoryNameChange(e)}
                    onBlur={e => onWorkCategoryNameBlur(e)} />
            </div>
            <div className="col-2 d-flex align-items-center">
                {workCategorySlug}
            </div>
            <div className="col-2 d-flex align-items-center">
                <Toggle
                    disabled={loading}
                    checked={active}
                    name='burritoIsReady'
                    value='yes'
                    onChange={e => onActiveChange(e)} />
            </div>
            <div className="col-4">
                <input
                    disabled={loading}
                    className="form-control"
                    type="number"
                    value={order}
                    min={0}
                    onKeyDown={e => onOrderKeyDown(e)}
                    onChange={e => onOrderChange(e)}
                    onBlur={e => onOrderBlur(e)} />
            </div>
        </div>
    );
};

WorkCategoryListItem.propTypes = {
    loadWorkCategoryList: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    setLoading: PropTypes.func.isRequired,
    idWorkCategory: PropTypes.number.isRequired,
    workCategoryName: PropTypes.string.isRequired,
    workCategorySlug: PropTypes.string.isRequired,
    active: PropTypes.bool.isRequired,
    order: PropTypes.number.isRequired
};

export default WorkCategoryListItem;
