import React, { useState } from 'react';
import axios from 'axios';
import * as PropTypes from 'prop-types';

const AddNewWorkCategory = props => {
    const {
        loadWorkCategoryList,
        loading,
        setLoading
    } = props;

    const [workCategoryName, setWorkCategoryName] = useState('');

    const onWorkCategoryNameChange = e => {
        setWorkCategoryName(e.target.value)
    };

    const onWorkCategorySubmitClick = () => {
        setLoading(true);

        const data = {
            workCategoryName: workCategoryName
        };

        axios.post('/api/work-category/create', data)
            .then(() => {
                setWorkCategoryName('');
                loadWorkCategoryList();
            })
            .catch(() => {
            })
            .then(() => setLoading(false));
    };

    const onWorkCategoryNameKeyDown = e => {
        if (e.keyCode === 13) {
            onWorkCategorySubmitClick();
            e.target.blur();
        }
    };

    return (
        <div className="row">
            <div className="col-12 col-sm-6 col-md-4 mb-3">
                <input
                    type="text"
                    className="form-control"
                    value={workCategoryName}
                    onChange={onWorkCategoryNameChange}
                    onKeyDown={onWorkCategoryNameKeyDown}
                    disabled={loading}
                    placeholder="Zadaj názov novej kategórie" />
            </div>
            <div className="col-12 col-sm-6 offset-md-4 col-md-4 offset-lg-6 col-lg-2 mb-3">
                <button
                    type="button"
                    className="btn btn-primary btn-block"
                    disabled={loading}
                    onClick={onWorkCategorySubmitClick}>
                    Pridať kategóriu
                </button>
            </div>
        </div>
    );
};

AddNewWorkCategory.propTypes = {
    loadWorkCategoryList: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    setLoading: PropTypes.func.isRequired
};

export default AddNewWorkCategory;
