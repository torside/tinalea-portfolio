import React, { useEffect, useState } from 'react';
import axios from 'axios';
import WorkCategoryListItem from './components/WorkCategoryListItem/index.jsx';
import AddNewWorkCategory from './components/AddNewWorkCategory/index.jsx';

const WorkCategories = () => {
    const [loading, setLoading] = useState(false);
    const [workCategoryList, setWorkCategoryList] = useState([]);

    const loadWorkCategoryList = () => {
        setLoading(true);

        const data = {
            length: 50,
            order: [
                {
                    dir: 'asc',
                    column: 0
                }
            ]
        };

        axios.post('/api/work-category/list', data)
            .then(response => setWorkCategoryList(response.data.data.data))
            .catch(() => {
            })
            .then(() => setLoading(false));
    };

    const workCategories = workCategoryList.map(l => (
        <WorkCategoryListItem
            loadWorkCategoryList={loadWorkCategoryList}
            loading={loading}
            setLoading={setLoading}
            key={l.idWorkCategory}
            idWorkCategory={l.idWorkCategory}
            workCategoryName={l.workCategoryName}
            workCategorySlug={l.workCategorySlug}
            active={l.active}
            order={l.order} />
    ));

    useEffect(() => {
        loadWorkCategoryList();
    }, []);

    return (
        <>
            <AddNewWorkCategory
                loadWorkCategoryList={loadWorkCategoryList}
                loading={loading}
                setLoading={setLoading} />
            <hr className="mt-0 mb-2" />
            <div className="row mb-2">
                <div className="col-4">Názov</div>
                <div className="col-2 d-flex align-items-center">URL</div>
                <div className="col-2 d-flex align-items-center">Aktívna</div>
                <div className="col-4">Poradie</div>
            </div>
            {workCategories}
        </>
    );
};

export default WorkCategories;
