import React, { useEffect, useState } from 'react';
import * as PropTypes from 'prop-types';
import axios from 'axios';
import Uploader from './components/Uploader/index.jsx';
import ListItem from './components/ListItem';

const Watermarks = props => {
    const {
        csrfToken
    } = props;

    const [loading, setLoading] = useState(false);
    const [watermarkList, setWatermarkList] = useState([]);

    const loadWatermarkList = () => {
        setLoading(true);

        const data = {
            length: 500,
            order: [
                {
                    dir: 'desc',
                    column: 0
                }
            ]
        };

        axios.post('/api/watermark/list', data)
            .then(response => setWatermarkList(response.data.data.data))
            .catch(() => {
            })
            .then(() => setLoading(false));
    };

    useEffect(() => {
        loadWatermarkList();
    }, []);

    const watermarks = watermarkList.map(l => {
        return (
            <ListItem
                key={l.idWatermark}
                loading={loading}
                setLoading={setLoading}
                idWatermark={l.idWatermark}
                imageFileName={l.imageFileName}
                title={l.title} />
        )
    });

    return (
        <>
            <div className="watermarks__uploader">
                <div className="row">
                    <div className="col-12">
                        <Uploader
                            csrfToken={csrfToken}
                            loading={loading}
                            setLoading={setLoading}
                            loadWatermarkList={loadWatermarkList} />
                    </div>
                </div>
            </div>
            <div className="watermarks__list">
                {watermarks}
            </div>
        </>
    );
};

Watermarks.propTypes = {
    csrfToken: PropTypes.string.isRequired
};

export default Watermarks;
