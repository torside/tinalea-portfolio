import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import axios from 'axios';

const ListItem = props => {
    const {
        loading,
        setLoading,
        idWatermark,
        imageFileName,
        title
    } = props;

    const [titleValue, setTitleValue] = useState(title);

    const itemSettings = [
        { backgroundColor: 'red' },
        { backgroundColor: 'green' },
        { backgroundColor: 'blue' },
        { backgroundColor: 'black' },
        { backgroundColor: 'grey' },
        { backgroundColor: 'white' }
    ];

    const update = () => {
        const data = {
            idWatermark: idWatermark,
            title: titleValue
        };

        setLoading(true);

        axios.post('/api/watermark/update', data)
            .then(() => {
            })
            .catch(() => {
            })
            .then(() => {
                setLoading(false);
            });
    };

    const onKeyDown = e => {
        if (e.key === 'Enter') {
            update();
        }
    };

    const src = '/images/image-service/watermarks/2000/' + imageFileName;

    const image = <img src={src} alt={imageFileName} width="100%" />;

    const items = itemSettings.map(i => {
        return (
            <div key={i.backgroundColor} className="images__image" style={{ backgroundColor: i.backgroundColor }}>
                {image}
            </div>
        );
    });

    return (
        <div className="watermarks__list__row mt-5">
            <div className="row_editor">
                <div className="row">
                    <div className="col-12">
                        <div className="form-group">
                            <label htmlFor="watermark__title">
                                Názov vodoznaku
                            </label>
                            <input
                                id="watermark__title"
                                type="text"
                                className="form-control"
                                disabled={loading}
                                value={titleValue}
                                onChange={e => setTitleValue(e.target.value)}
                                onBlur={update}
                                onKeyDown={onKeyDown}
                                placeholder="Zadaj názov vodoznaku..." />
                        </div>
                    </div>
                </div>
            </div>
            <div className="row_images mb-2">
                {items}
            </div>
        </div>
    );
};

ListItem.propTypes = {
    loading: PropTypes.bool.isRequired,
    setLoading: PropTypes.func.isRequired,
    idWatermark: PropTypes.number.isRequired,
    imageFileName: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
};

export default ListItem;
