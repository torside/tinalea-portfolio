import React from 'react';
import * as PropTypes from 'prop-types';

const Uploader = props => {
    const {
        csrfToken,
        loading,
        setLoading,
        loadWatermarkList
    } = props;

    const onChange = e => {
        setLoading(true);

        const fd = new FormData();

        Array.from(e.target.files)
            .forEach(file => {
                fd.append('images[]', file);
            });

        const xhr = new XMLHttpRequest();

        xhr.open('POST', '/api/watermark/upload-image', true);
        xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);
        xhr.onreadystatechange = () => {
            setLoading(false);

            if (xhr.readyState === 4 && xhr.status === 200) {
                loadWatermarkList();
            }
        };

        xhr.send(fd);
    };

    return (

        <div className="custom-file">
            <input
                disabled={loading}
                onChange={onChange}
                type="file"
                className="custom-file-input"
                id="images"
                name="images" />
            <label className="custom-file-label" htmlFor="images">
                Zvoľ súbor
            </label>
        </div>
    );
};

Uploader.propTypes = {
    csrfToken: PropTypes.string.isRequired,
    loading: PropTypes.bool.isRequired,
    setLoading: PropTypes.func.isRequired,
    loadWatermarkList: PropTypes.func.isRequired
};

export default Uploader;
