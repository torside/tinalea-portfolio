import React from 'react';
import { useQueryParam, NumberParam } from 'use-query-params';
import WorkEditorComponent from '../../../common/components/WorkEditorComponent/index.jsx';

const WorkEditor = () => {
    const [idWork] = useQueryParam('idWork', NumberParam);

    return <WorkEditorComponent idWork={idWork} />;
};

export default WorkEditor;
