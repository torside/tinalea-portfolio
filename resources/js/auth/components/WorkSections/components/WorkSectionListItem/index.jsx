import React, { useState } from 'react';
import axios from 'axios';
import * as PropTypes from 'prop-types';
import Toggle from 'react-toggle';

const WorkSectionListItem = props => {
    const {
        loadWorkSectionList,
        loading,
        setLoading,
        idWorkSection,
        workSectionSlug
    } = props;

    const [workSectionName, setWorkSectionName] = useState(props.workSectionName);
    const [active, setActive] = useState(props.active);
    const [order, setOrder] = useState(props.order);

    const updateWorkSectionItem = data => {
        setLoading(true);

        axios.post('/api/work-section/update', data)
            .then(() => {
            })
            .catch(() => {
            })
            .then(() => {
                setLoading(false);
                loadWorkSectionList();
            });
    };

    const onActiveChange = e => {
        const checked = e.target.checked === true;

        setActive(checked);

        updateWorkSectionItem({
            idWorkSection: idWorkSection,
            active: checked
        });
    };

    const onOrderKeyDown = e => {
        if (e.keyCode === 13) {
            e.target.blur();
        }
    };

    const onOrderChange = e => setOrder(parseInt(e.target.value));

    const onOrderBlur = e => updateWorkSectionItem({
        idWorkSection: idWorkSection, order: parseInt(e.target.value)
    });

    const onWorkSectionNameKeyDown = e => {
        if (e.keyCode === 13) {
            e.target.blur();
        }
    };

    const onWorkSectionNameChange = e => setWorkSectionName(e.target.value);

    const onWorkSectionNameBlur = e => updateWorkSectionItem({
        idWorkSection: idWorkSection,
        workSectionName: e.target.value
    });

    return (
        <div className="row mb-2">
            <div className="col-4">
                <input
                    disabled={loading}
                    className="form-control"
                    type="text"
                    value={workSectionName}
                    onKeyDown={e => onWorkSectionNameKeyDown(e)}
                    onChange={e => onWorkSectionNameChange(e)}
                    onBlur={e => onWorkSectionNameBlur(e)} />
            </div>
            <div className="col-2 d-flex align-items-center">
                {workSectionSlug}
            </div>
            <div className="col-2 d-flex align-items-center">
                <Toggle
                    disabled={loading}
                    checked={active}
                    name='burritoIsReady'
                    value='yes'
                    onChange={e => onActiveChange(e)} />
            </div>
            <div className="col-4">
                <input
                    disabled={loading}
                    className="form-control"
                    type="number"
                    value={order}
                    min={0}
                    onKeyDown={e => onOrderKeyDown(e)}
                    onChange={e => onOrderChange(e)}
                    onBlur={e => onOrderBlur(e)} />
            </div>
        </div>
    );
};

WorkSectionListItem.propTypes = {
    loadWorkSectionList: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    setLoading: PropTypes.func.isRequired,
    idWorkSection: PropTypes.number.isRequired,
    workSectionName: PropTypes.string.isRequired,
    workSectionSlug: PropTypes.string.isRequired,
    active: PropTypes.bool.isRequired,
    order: PropTypes.number.isRequired
};

export default WorkSectionListItem;
