import React, { useState } from 'react';
import axios from 'axios';
import * as PropTypes from 'prop-types';

const AddNewWorkSection = props => {
    const {
        loadWorkSectionList,
        loading,
        setLoading
    } = props;

    const [workSectionName, setWorkSectionName] = useState('');

    const onWorkSectionNameChange = e => {
        setWorkSectionName(e.target.value)
    };

    const onWorkSectionSubmitClick = () => {
        setLoading(true);

        const data = {
            workSectionName: workSectionName
        };

        axios.post('/api/work-section/create', data)
            .then(() => {
                setWorkSectionName('');
                loadWorkSectionList();
            })
            .catch(() => {
            })
            .then(() => setLoading(false));
    };

    const onWorkSectionNameKeyDown = e => {
        if (e.keyCode === 13) {
            onWorkSectionSubmitClick();
            e.target.blur();
        }
    };

    return (
        <div className="row">
            <div className="col-12 col-sm-6 col-md-4 mb-3">
                <input
                    type="text"
                    className="form-control"
                    value={workSectionName}
                    onChange={onWorkSectionNameChange}
                    onKeyDown={onWorkSectionNameKeyDown}
                    disabled={loading}
                    placeholder="Zadaj názov novej sekcie" />
            </div>
            <div className="col-12 col-sm-6 offset-md-4 col-md-4 offset-lg-6 col-lg-2 mb-3">
                <button
                    type="button"
                    className="btn btn-primary btn-block"
                    disabled={loading}
                    onClick={onWorkSectionSubmitClick}>
                    Pridať sekciu
                </button>
            </div>
        </div>
    );
};

AddNewWorkSection.propTypes = {
    loadWorkSectionList: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    setLoading: PropTypes.func.isRequired
};

export default AddNewWorkSection;
