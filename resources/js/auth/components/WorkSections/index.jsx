import React, { useEffect, useState } from 'react';
import axios from 'axios';
import WorkSectionListItem from './components/WorkSectionListItem/index.jsx';
import AddNewWorkSection from './components/AddNewWorkSection/index.jsx';

const WorkSections = () => {
    const [loading, setLoading] = useState(false);
    const [workSectionList, setWorkSectionList] = useState([]);

    const loadWorkSectionList = () => {
        setLoading(true);

        const data = {
            length: 50,
            order: [
                {
                    dir: 'asc',
                    column: 4
                }
            ]
        };

        axios.post('/api/work-section/list', data)
            .then(response => setWorkSectionList(response.data.data.data))
            .catch(() => {
            })
            .then(() => setLoading(false));
    };

    const workSections = workSectionList.map(l => (
        <WorkSectionListItem
            loadWorkSectionList={loadWorkSectionList}
            loading={loading}
            setLoading={setLoading}
            key={l.idWorkSection}
            idWorkSection={l.idWorkSection}
            workSectionName={l.workSectionName}
            workSectionSlug={l.workSectionSlug}
            active={l.active}
            order={l.order} />
    ));

    useEffect(() => {
        loadWorkSectionList();
    }, []);

    return (
        <>
            <AddNewWorkSection
                loadWorkSectionList={loadWorkSectionList}
                loading={loading}
                setLoading={setLoading} />
            <hr className="mt-0 mb-2" />
            <div className="row mb-2">
                <div className="col-4">Názov</div>
                <div className="col-2 d-flex align-items-center">URL</div>
                <div className="col-2 d-flex align-items-center">Aktívna</div>
                <div className="col-4">Poradie</div>
            </div>
            {workSections}
        </>
    );
};

export default WorkSections;
