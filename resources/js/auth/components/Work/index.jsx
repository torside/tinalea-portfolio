import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import WorkUploader from './components/WorkUploader/index.jsx';
import WorkList from './components/WorkList/index.jsx';
import WorkEditModal from './components/WorkEditModal/index.jsx';

const Work = props => {
    const { csrfToken } = props;

    const [loading, setLoading] = useState(false);
    const [idWorkToEdit, setIdWorkToEdit] = useState(null);

    return (
        <>
            <WorkUploader
                csrfToken={csrfToken}
                loading={loading}
                setLoading={setLoading} />
            <hr />
            <WorkList
                setLoading={setLoading}
                setIdWorkToEdit={setIdWorkToEdit} />
            <WorkEditModal
                idWorkToEdit={idWorkToEdit}
                setIdWorkToEdit={setIdWorkToEdit} />
        </>
    );
};

Work.propTypes = {
    csrfToken: PropTypes.string
};

Work.defaultProps = {
    csrfToken: ''
};

export default Work;
