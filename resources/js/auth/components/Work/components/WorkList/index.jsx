import React, { useState, useEffect } from 'react';
import * as PropTypes from 'prop-types';
import axios from 'axios';
import WorkListItem from './components/WorkListItem/index.jsx';

const WorkList = props => {
    const {
        setLoading,
        setIdWorkToEdit
    } = props;

    const [workList, setWorkList] = useState([]);

    useEffect(() => {
        setLoading(true);

        const data = {
            start: 0,
            length: 5000,
            order: [
                {
                    dir: 'desc',
                    column: 0
                }
            ]
        };

        axios.post('/api/work/list', data)
            .then(response => setWorkList(prevState => [...prevState].concat(response.data.data.data)))
            .catch(() => {
            })
            .then(() => setLoading(false));
    }, [setLoading]);

    const workListItems = workList.map(w => {
        return (
            <WorkListItem
                key={w.idWork}
                idWork={w.idWork}
                imageFileName={w.imageFileName}
                setIdWorkToEdit={setIdWorkToEdit} />
        )
    });

    return (
        <>
            <div className="row">
                <div className="col-12">
                    <div className="workListItems">
                        {workListItems}
                    </div>
                </div>
            </div>
        </>
    );
};

WorkList.propTypes = {
    setLoading: PropTypes.func.isRequired,
    setIdWorkToEdit: PropTypes.func.isRequired
};

export default WorkList;
