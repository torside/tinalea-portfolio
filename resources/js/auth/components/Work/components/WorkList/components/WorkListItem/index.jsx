import React from 'react';
import * as PropTypes from 'prop-types';

const WorkListItem = props => {
    const {
        idWork,
        imageFileName,
        setIdWorkToEdit
    } = props;

    return (
        <>
            <div
                className="workListItem"
                style={{
                    backgroundImage: 'url(/images/image-service/work/xs/' + imageFileName + ')'
                }}
                onClick={() => setIdWorkToEdit(idWork)} />
        </>
    );
};

WorkListItem.propTypes = {
    idWork: PropTypes.number.isRequired,
    imageFileName: PropTypes.string.isRequired,
    setIdWorkToEdit: PropTypes.func.isRequired
};

export default WorkListItem;
