import React from 'react';
import * as PropTypes from 'prop-types';
import { Modal } from 'react-bootstrap';
import WorkEditorComponent from '../../../../../common/components/WorkEditorComponent';

const WorkEditModal = props => {
    const {
        idWorkToEdit,
        setIdWorkToEdit
    } = props;

    return (
        <Modal
            size="xl"
            show={null !== idWorkToEdit}
            onHide={() => setIdWorkToEdit(null)}
            animation={false}>
            <Modal.Body>
                {null !== idWorkToEdit && <WorkEditorComponent idWork={idWorkToEdit} />}
            </Modal.Body>
        </Modal>
    );
};

WorkEditModal.propTypes = {
    idWorkToEdit: PropTypes.number,
    setIdWorkToEdit: PropTypes.func.isRequired
};

WorkEditModal.defaultProps = {
    idWorkToEdit: null
};

export default WorkEditModal;
