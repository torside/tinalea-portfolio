import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import ImageUploader from 'react-images-upload';

const WorkUploader = props => {
    const { csrfToken, loading, setLoading } = props;

    const imageUploadMaxFileSize = 30;
    const imageUploadAllowedExtensions = ['.jpg'];

    const [imagesToUpload, setImagesToUpload] = useState([]);

    const onImageUploadChange = images => setImagesToUpload(images);

    const onImageUploadTrigger = () => {
        setLoading(true);

        const fd = new FormData();

        imagesToUpload.forEach(imageToUpload => {
            fd.append('images[]', imageToUpload);
        });

        const xhr = new XMLHttpRequest();

        xhr.open('POST', '/api/work/upload-image', true);
        xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);
        xhr.onreadystatechange = () => {
            setLoading(false);

            if (xhr.readyState === 4 && xhr.status === 200) {
                window.location.reload();
            }
        };

        xhr.send(fd);
    };

    return (
        <>
            <div className="row">
                <div className="col-12">
                    <ImageUploader
                        withPreview={true}
                        maxFileSize={imageUploadMaxFileSize * 1024 * 1024}
                        label={'Maximálna veľkosť súboru: ' + imageUploadMaxFileSize + 'mb. Povolené formáty: ' + imageUploadAllowedExtensions.join(', ') + '.'}
                        withIcon={true}
                        buttonText='Zvoľ obrázky'
                        onChange={onImageUploadChange}
                        imgExtension={imageUploadAllowedExtensions} />
                </div>
            </div>
            <div className="row">
                <div className="col-12 text-center">
                    <button type="button" className="upload-button" onClick={onImageUploadTrigger} disabled={loading}>
                        Nahrať obrázky
                    </button>
                </div>
            </div>
        </>
    );
};

WorkUploader.propTypes = {
    csrfToken: PropTypes.string.isRequired,
    loading: PropTypes.bool.isRequired,
    setLoading: PropTypes.func.isRequired
};

export default WorkUploader;
