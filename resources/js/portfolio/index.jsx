import 'bootstrap';
import React from 'react';
import ReactDOM from 'react-dom';
import HeadProvider from './../providers/HeadProvider/provider.jsx';
import Application from './components/Application/index.jsx';

if (document.getElementById('application')) {
    ReactDOM.render(
        <HeadProvider>
            <Application />
        </HeadProvider>,
        document.getElementById('application')
    );
}
