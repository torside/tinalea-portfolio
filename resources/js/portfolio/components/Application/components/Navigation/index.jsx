import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';

const Navigation = () => {
    return (
        <Navbar expand="lg">
            <Navbar.Brand href="/work" className="e">
                Tinalea
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav">
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
            </Navbar.Toggle>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link as={Link} to="/work">Work</Nav.Link>
                    <span>&bull;</span>
                    <Nav.Link as={Link} to="/about">About</Nav.Link>
                    <span>&bull;</span>
                    <Nav.Link as={Link} to="/contact">Contact</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};

export default Navigation;
