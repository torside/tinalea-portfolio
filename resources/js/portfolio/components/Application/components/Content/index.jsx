import React, { useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import GalleryProvider from './../../../../../providers/GalleryProvider/provider.jsx';
import About from './components/About/index.jsx';
import Work from './components/Work/index.jsx';
import { useHistory } from 'react-router-dom';

const Content = () => {
    const history = useHistory();

    const getLocationId = ({ pathname, search, hash }) => {
        return pathname + (search ? '?' + search : '') + (hash ? '#' + hash : '');
    };

    useEffect(() => {
        history.block(
            (location, action) =>
                action !== 'PUSH' ||
                getLocationId(location) !== getLocationId(history.location)
        );
    }, [history]);

    return (
        <Switch>
            <Route path="/work/:category?/:section?/:idWork?">
                <GalleryProvider>
                    <Work />
                </GalleryProvider>
            </Route>
            <Route path="/about">
                <About />
            </Route>
            <Route path="/contact">
                <div>Komponent - kontakt</div>
            </Route>
        </Switch>
    );
};

export default Content;
