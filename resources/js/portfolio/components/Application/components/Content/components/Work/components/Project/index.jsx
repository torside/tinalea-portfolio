import React from 'react';
import CategoryHeader from '../Common/components/CategoryHeader/index.jsx';

const Project = () => {
    return (
        <>
            <div className="container">
                <CategoryHeader inverted category="Project" />
                <div className="row">
                    <div className="col-12">
                        <a
                            className="project d-flex justify-content-start flex-column flex-md-row"
                            href="https://www.podpsa.sk/"
                            target="_blank"
                            rel="noopener noreferrer">
                            <div className="project__logo mr-md-4" />
                            <div className="project__body">
                                <div className="project__body__title text-center mt-4 mb-4 mt-md-0">
                                    Projekt Podpsa
                                </div>
                                <div className="project__body__description">
                                    Táto stránka vznikla za účelom pomoci psíkom. Zahŕňa v sebe dva projekty a to
                                    projekt SOS MERLE a projekt PODPSA. Prvý sa venuje najmä problematike psíkov s génom
                                    merle. Projekt PODPSA je všeobecnejší a jeho hlavnou úlohou je možnosť finančne
                                    podporovať psíky, ktoré to akútne potrebujú.
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Project;