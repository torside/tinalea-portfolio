import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

const CategoryHeader = props => {
    const {
        inverted,
        category
    } = props;

    return (
        <div className={classNames(
            'category__header',
            { 'category__header--inverted': inverted === true }
        )}>
            <div className="header__title">
                THE SELECTED OF
            </div>
            <div className="header__description">
                {category}
            </div>
        </div>
    );
};

CategoryHeader.propTypes = {
    inverted: PropTypes.bool,
    category: PropTypes.string.isRequired,
};

CategoryHeader.defaultProps = {
    inverted: false
};

export default CategoryHeader;
