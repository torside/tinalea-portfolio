import React from 'react';
import Photos from './components/Photos/index.jsx';

const Photography = () => {
    return (
        <>
            <Photos />
        </>
    );
};

export default Photography;
