import React, { useContext, useEffect } from 'react';
import HeadContext from '../../../../../../../providers/HeadProvider/context';
import Header from './components/Header/index.jsx';
import Info from './components/Info/index.jsx';
import Knowledge from './components/Knowledge/index.jsx';
import Reference from './components/Reference/index.jsx';
import Social from './components/Social/index.jsx';
import Work from './components/Work/index.jsx';

const About = () => {
    const {
        setTitleParts
    } = useContext(HeadContext);

    useEffect(() => {
        setTitleParts(['Tinalea Portfolio', 'About'])
    }, [setTitleParts]);

    return (
        <div className="about-wrapper">
            <div className="about">
                <Header />
                <Info />
                <Work />
                <Knowledge />
                <Reference />
                <div className="about__delimeter" />
                <Social />
            </div>
        </div>
    );
};

export default About;
