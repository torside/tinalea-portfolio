import React from 'react';

const Reference = () => {
    return (
        <div className="about__section">
            <div className="about__section__title">
                Referencie
            </div>
            <div className="about__section__text">
                <div className="about__section__text__description">
                    <p className="paragraph--indent">
                        Tina bola významnou súčasťou môjho kreatívneho tímu v Zľavomate. Dennodenne sme spolu pracovali
                        na rozmanitých projektoch a vždy som sa mohla spoľahnúť na jej nadhľad i grafické spracovanie
                        zadaní. Tina sa výborne orientuje v práci s úpravou fotografií, tvorením originálnej grafiky pre
                        online i print a nikdy jej neunikne žiadny deadline. Vyzdvihla by som tiež jej vlastnú autorskú
                        prácu, do ktorej vkladá celé srdce a výsledkom sú jedinečné obrázky, v ktorých je do dokonalosti
                        dotiahnutý každý detail. Jej všestrannosť a flexibilita sa odráža aj v ďalších kreatívnych
                        sférach, venuje sa aj fotografovaniu i písaniu blogových článkov. Tina je tímový hráč, no vyniká
                        aj v samostatnom plnení úloh a to vždy k spokojnosti nadriadených alebo klienta. Ak by som opäť
                        mala možnosť vyskladať kreatívny tím, Tina by u mňa nepochybne mala svoje miesto.
                    </p>
                </div>
                <div className="about__section__text__signature">
                    - Mária Roštecká, teamleader kreatívneho tímu
                </div>
            </div>
            <div className="about__section__text">
                <div className="about__section__text__description">
                    <p className="paragraph--indent">
                        Za svojí pracovní kariéru jsem poznala hodně grafických designérů a sazečů. Sama jsem pracovala
                        v grafickém studiu a troufám si říci, že poznám kvalitní práci s nápadem. Jako každý grafik má
                        Martina svůj specifický styl. Je detailista, dokud vše neladí, tak práci neodevzdá.
                    </p>
                    <p className="paragraph--indent">Pokud hledáte někoho na obalový design, hravý grafický návrh, svěží
                        fotografie nebo konzultaci, neuděláte krok vedle, pokud Martině napíšete. Za vaší spokojenost
                        ručím.
                    </p>
                </div>
                <div className="about__section__text__signature">
                    - Ladislava Šušková, marketingová ředitelka
                </div>
            </div>
        </div>
    );
};

export default Reference;