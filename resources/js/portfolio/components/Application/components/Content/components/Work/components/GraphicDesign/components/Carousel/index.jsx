import React, { useContext } from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from 'react-slick';
import GalleryContext from './../../../../../../../../../../../providers/GalleryProvider/context.jsx';
import { useHistory } from 'react-router';

const Carousel = props => {
    const history = useHistory();

    const {
        getWorkListByParams
    } = useContext(GalleryContext);

    const { inverted } = props;

    const responsive = [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 576,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }
    ];

    const itemElements = getWorkListByParams('graphic-design', 'default').map(i => {
        return (
            <div key={i.idWork} className="custom-slider__item__container">
                <div
                    className="custom-slider__item"
                    style={{ backgroundImage: 'url("/images/image-service/work/md/' + i.imageFileName + '")' }}
                    onClick={() => {
                        history.push('/work/graphic-design/default/' + i.idWork);
                    }} />
            </div>
        );
    });

    return (
        <div className={classNames(
            'custom-slider',
            { 'custom-slider--inverted': inverted === true }
        )}>
            <Slider
                responsive={responsive}
                arrows={false}
                lazyLoad={true}
                dots={true}
                infinite={false}
                speed={500}
                slidesToShow={4}
                slidesToScroll={4}>
                {itemElements}
            </Slider>
        </div>
    );
};

Carousel.propTypes = {
    inverted: PropTypes.bool
};

Carousel.defaultProps = {
    inverted: false
};

export default Carousel;
