import React from 'react';
import CategoryHeader from '../Common/components/CategoryHeader/index.jsx';
import Carousel from './components/Carousel/index.jsx';

const GraphicDesign = () => {
    return (
        <>
            <div className="container">
                <CategoryHeader inverted category="Graphic Design" />
                <Carousel inverted />
            </div>
        </>
    );
};

export default GraphicDesign;
