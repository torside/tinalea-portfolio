import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInstagram, faFacebookSquare } from '@fortawesome/free-brands-svg-icons';
import { faEnvelopeSquare } from '@fortawesome/free-solid-svg-icons';

const Social = () => {
    return (
        <div className="about__social">
            <a
                className="social__icon"
                target="_blank"
                rel="noreferrer noopener"
                href="https://www.instagram.com/tinalea_life/">
                <FontAwesomeIcon icon={faInstagram} color="white" size="2x" />
            </a>
            <a
                className="social__icon"
                target="_blank"
                rel="noreferrer noopener"
                href="https://www.facebook.com/tina.kizekova">
                <FontAwesomeIcon icon={faFacebookSquare} color="white" size="2x" />
            </a>
            <a
                className="social__icon"
                href="mailto:tinakizekova@gmail.com">
                <FontAwesomeIcon icon={faEnvelopeSquare} color="white" size="2x" />
            </a>
        </div>
    );
};

export default Social;