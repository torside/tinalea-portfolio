import React from 'react';
import CategoryHeader from '../Common/components/CategoryHeader/index.jsx';
import Carousel from './components/Carousel/index.jsx';

const Art = () => {
    return (
        <>
            <div className="container">
                <CategoryHeader category="Art" />
                <Carousel />
            </div>
        </>
    );
};

export default Art;
