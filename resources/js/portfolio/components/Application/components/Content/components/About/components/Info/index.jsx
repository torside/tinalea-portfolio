import React from 'react';

const Info = () => {
    return (
        <div className="about__section d-none">
            <div className="about__section__title">
                O mne
            </div>
            <div className="about__section__text">
                <div className="about__section__text__description">
                    <p className="paragraph--indent">
                        Som kreatívny človek, ktorý hľadá inšpiráciu všade vôkol seba. V mojom svete sú veci prepojené.
                        Umenie ma fascinuje vo všetkých svojich sférach, aj to je dôvod prečo sa venujem širokej škále
                        činností. Od výtvarného a grafického umenia, cez fotografiu, blogy, články, marketing až po
                        vlastné projekty.
                    </p>
                </div>
            </div>
        </div>
    );
};

export default Info;