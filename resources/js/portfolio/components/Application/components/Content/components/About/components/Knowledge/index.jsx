import React from 'react';

const Knowledge = () => {
    return (
        <div className="about__section">
            <div className="about__section__title">
                Štúdium
            </div>
            <div className="about__section__text">
                <div className="about__section__text__description">
                    <p className="mb-3">
                        2003 - 2006 Základná umelecká škola, Čadca
                    </p>
                    <p className="mb-0">
                        2006 - 2010 Stredná umelecká škola, Nižná
                    </p>
                    <p className="mb-3">
                        študijný odbor: Propagačná grafika
                    </p>
                    <p className="mb-0">
                        2011 - 2016 Univerzita Mateja Bela, Banská Bystrica
                    </p>
                    <p className="mb-3">
                        študijný odbor: Učiteľstvo výtvarného umenia
                    </p>
                </div>
            </div>
        </div>
    );
};

export default Knowledge;