import React from 'react';

const Work = () => {
    return (
        <div className="about__section d-none">
            <div className="about__section__title">
                Tvorba
            </div>
            <div className="about__section__text">
                <div className="about__section__text__description">
                    <p className="paragraph--indent">
                        V sekcii fotografia nájdete prevažne produktovú fotografiu, exteriérovú fotogafiu, či fotografie
                        z mojich ciest. Nájdete tu však aj svadobnú fotografiu, či fotky domácich miláčikov.
                    </p>
                </div>
            </div>
        </div>
    );
};

export default Work;