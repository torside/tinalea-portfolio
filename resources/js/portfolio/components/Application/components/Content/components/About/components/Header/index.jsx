import React from 'react';

const Header = () => {
    return (
        <>
            <div className="about__avatar" />
            <div className="about__name">
                Martina Kizeková
            </div>
        </>
    );
};

export default Header;