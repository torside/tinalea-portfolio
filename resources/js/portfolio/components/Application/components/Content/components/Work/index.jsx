import React, { useContext, useEffect, useRef } from 'react';
import HeadContext from './../../../../../../../providers/HeadProvider/context.jsx';
import Header from './components/Header/index.jsx';
import Navigation from './components/Navigation/index.jsx';
import Photography from './components/Photography/index.jsx';
import GraphicDesign from './components/GraphicDesign/index.jsx';
import Art from './components/Art/index.jsx';
import Project from './components/Project/index.jsx';
import Gallery from '../../../../../../../common/components/Gallery/index.jsx';
import GalleryContext from './../../../../../../../providers/GalleryProvider/context.jsx';

const Work = () => {
    const {
        setTitleParts
    } = useContext(HeadContext);

    const {
        activeWorkList,
        category,
        section
    } = useContext(GalleryContext);

    const categoryRefs = {
        'photography': useRef(null),
        'graphic-design': useRef(null),
        'art': useRef(null),
        'project': useRef(null)
    };

    useEffect(() => {
        setTitleParts(['Tinalea Portfolio', 'Work'])
    }, [setTitleParts]);

    useEffect(() => {
        setTitleParts([
            'Tinalea Portfolio',
            'Work',
            category || null,
            section || null,
        ])
    }, [setTitleParts, category, section]);

    useEffect(() => {
        const element = document.getElementById('work-category--' + category);
        if (typeof (element) != 'undefined' && element != null) {
            if (typeof categoryRefs[category] !== 'undefined') {
                window.scrollTo({
                    top: categoryRefs[category].current.offsetTop,
                    left: 0,
                    behavior: 'smooth'
                });
            }
        }
    }, [category, categoryRefs]);

    return (
        <>
            <div className="work-category work-category--header">
                <Header />
            </div>
            <div className="work-category work-category--navigation">
                <Navigation />
            </div>
            <div
                ref={categoryRefs['photography']}
                className="work-category work-category--photography work-category--spacing"
                id="work-category--photography">
                <Photography />
            </div>
            <div
                ref={categoryRefs['graphic-design']}
                className="work-category work-category--graphic-design work-category--spacing"
                id="work-category--graphic-design">
                <GraphicDesign />
            </div>
            <div
                ref={categoryRefs['art']}
                className="work-category work-category--art work-category--spacing"
                id="work-category--art">
                <Art />
            </div>
            <div
                ref={categoryRefs['project']}
                className="work-category work-category--project work-category--spacing"
                id="work-category--project">
                <Project />
            </div>
            {(activeWorkList.length > 0) && (
                <Gallery />
            )}
        </>
    );
};

export default Work;
