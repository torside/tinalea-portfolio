import React from 'react';
import { useHistory } from 'react-router-dom';

const Navigation = () => {
    const history = useHistory();

    const navigation = [
        {
            key: 1,
            label: 'Photography',
            target: 'photography'
        },
        {
            key: 2,
            label: 'Graphic design',
            target: 'graphic-design'
        },
        {
            key: 3,
            label: 'Art',
            target: 'art'
        },
        {
            key: 4,
            label: 'Project',
            target: 'project'
        }
    ];

    const items = navigation.map(n => {
        return (
            <a
                key={n.key}
                onClick={() => history.push('/work/' + n.target)}
                className="workNavigationList__item">
                {n.label}
            </a>
        );
    });

    return (
        <div className="container">
            <div className="workNavigationList">
                {items}
            </div>
        </div>
    );
};

export default Navigation;
