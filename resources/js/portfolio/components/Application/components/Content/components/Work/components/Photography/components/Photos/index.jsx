import React, { useEffect, useState } from 'react';
import Masonry from 'react-masonry-css';
import TrackVisibility from 'react-on-screen';
import { translate } from './../../../../../../../../../../../services/LocalizationService/index.js';
import ColumnItem from './components/ColumnItem/index.jsx';
import axios from 'axios';

const Photos = () => {
    const [workSectionList, setWorkSectionList] = useState([]);

    const [sections, setSections] = useState([]);

    const breakpointCols = {
        default: 3,
        576: 1,
        768: 2,
        992: 3,
        1200: 3
    };

    useEffect(() => {
        axios.post('/api/work-section/list', { length: 9999, order: [{ dir: 'asc', column: 4 }] })
            .then(response => setWorkSectionList(response.data.data.data));
    }, []);

    useEffect(() => {
        setSections(
            workSectionList.map(section => {
                return (
                    <TrackVisibility key={section.idWorkSection} className="visibility-tracker">
                        <ColumnItem
                            title={section.workSectionName}
                            description={translate(section.workSectionName, 'sk')}
                            section={section.workSectionSlug} />
                    </TrackVisibility>
                );
            })
        );
    }, [workSectionList]);

    return (
        <>
            <div className="container">
                <div>
                    <Masonry breakpointCols={breakpointCols} className="masonry" columnClassName="masonry__column">
                        {sections}
                    </Masonry>
                </div>
            </div>
        </>
    );
};

export default Photos;