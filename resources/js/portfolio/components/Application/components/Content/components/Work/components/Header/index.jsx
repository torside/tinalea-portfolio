import React from 'react';

const Header = () => {
    return (
        <div className="container">
            <div className="work__header">
                <div className="header__title">
                    THE SELECTED OF
                </div>
                <div className="header__description">
                    PORTFOLIO
                </div>
            </div>
        </div>
    );
};

export default Header;
