import React, { useContext } from 'react';
import classNames from 'classnames';
import * as PropTypes from 'prop-types';
import GalleryContext from './../../../../../../../../../../../../../providers/GalleryProvider/context.jsx';
import { useHistory } from 'react-router';

const ColumnItem = props => {
    const history = useHistory();

    const {
        getWorkListByParams
    } = useContext(GalleryContext);

    const {
        title,
        description,
        section,
        isVisible
    } = props;

    const workList = getWorkListByParams('photography', section);

    const defaultImage = workList.length > 0 ? workList[0] : null;

    const style = defaultImage === null ? {} : { backgroundImage: 'url("/images/image-service/work/md/' + defaultImage.imageFileName + '")' };

    return (
        <>
            <div
                className={classNames('column__item', { 'column__item--is-visible': isVisible })}
                style={style}
                onClick={() => {
                    if (defaultImage !== null) {
                        history.push('/work/photography/' + section + '/' + defaultImage.idWork);
                    }
                }}>
                {title && (
                    <div className="item__overlay">
                        <div className="overlay__title">
                            {title}
                        </div>
                        <div className="overlay__desc">
                            {description}
                        </div>
                    </div>
                )}
            </div>
        </>
    );
};

ColumnItem.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    section: PropTypes.string.isRequired,
    isVisible: PropTypes.bool
};

ColumnItem.defaultProps = {
    isVisible: false
};

export default ColumnItem;
