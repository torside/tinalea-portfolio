import React, { useContext, useEffect, useState } from 'react';
import { BrowserRouter } from 'react-router-dom';
import axios from 'axios';
import DocumentMeta from 'react-document-meta';
import Navigation from './components/Navigation/index.jsx';
import Content from './components/Content/index.jsx';
import HeadContext from './../../../providers/HeadProvider/context.jsx';

const Application = () => {
    const {
        head
    } = useContext(HeadContext);

    const [user, setUser] = useState(null);

    useEffect(() => {
        axios.get('/api/auth/get-user')
            .then(response => setUser(response.data.user))
            .catch()
            .then();
    }, []);

    return (
        <DocumentMeta {...head}>
            <BrowserRouter>
                <Navigation />
                <div className="navbar__content__spacing" />
                <Content />
            </BrowserRouter>
        </DocumentMeta>
    );
};

export default Application;
