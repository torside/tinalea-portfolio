import * as sk from './translations/sk.json';

const translate = (key, language) => {
    const languages = {
        sk: sk.default
    };

    if (
        languages[language] !== null
        && typeof languages[language] === 'object'
        && Array.isArray(languages[language]) === false
        && (
            typeof languages[language][key] === 'string'
            || languages[language][key] instanceof String
        )
    ) {
        return languages[language][key];
    }

    return key;
};

export {
    translate
}