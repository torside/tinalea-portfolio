import React, { useEffect, useState } from 'react';
import * as PropTypes from 'prop-types';
import Select from 'react-select';

const Offset = props => {
    const {
        loading,
        watermarkPosition,
        watermarkOffsetX,
        setWatermarkOffsetX,
        watermarkOffsetY,
        setWatermarkOffsetY,
        setChanged
    } = props;

    const [offsetXValue, setOffsetXValue] = useState(null);
    const [offsetYValue, setOffsetYValue] = useState(null);
    const [offsetOptions] = useState([0, 10, 50, 100, 200, 400, 500]);

    const isOffsetXDisabled = () => {
        return ['top', 'center', 'bottom'].indexOf(watermarkPosition) > -1;
    };

    const isOffsetYDisabled = () => {
        return ['left', 'center', 'right'].indexOf(watermarkPosition) > -1;
    };

    useEffect(() => {
        const newOffsetXValue = offsetOptions.find(o => watermarkOffsetX === o);

        if (typeof newOffsetXValue !== 'undefined') {
            setOffsetXValue({
                value: newOffsetXValue,
                label: newOffsetXValue + ' px'
            });

            return;
        }

        setOffsetXValue(null);
    }, [offsetOptions, watermarkOffsetX]);

    useEffect(() => {
        const newOffsetYValue = offsetOptions.find(o => watermarkOffsetY === o);

        if (typeof newOffsetYValue !== 'undefined') {
            setOffsetYValue({
                value: newOffsetYValue,
                label: newOffsetYValue + ' px'
            });

            return;
        }

        setOffsetYValue(null);
    }, [offsetOptions, watermarkOffsetY]);

    return (
        <>
            <div className="row">
                <div className="col-12 col-md-6">
                    <div className="form-group">
                        <label htmlFor="workEditor__offsetXSelect">
                            Horizontálny posun
                        </label>
                        <Select
                            id="workEditor__offsetXSelect"
                            isDisabled={loading || isOffsetXDisabled()}
                            isLoading={loading}
                            placeholder="Zvoľ hodnotu"
                            value={offsetXValue}
                            onChange={value => {
                                setWatermarkOffsetX(value.value);
                                setChanged(true);
                            }}
                            options={offsetOptions.map(o => {
                                return {
                                    value: o,
                                    label: o + ' px'
                                };
                            })} />
                    </div>
                </div>
                <div className="col-12 col-md-6">
                    <div className="form-group">
                        <label htmlFor="workEditor__offsetYSelect">
                            Vertikálny posun
                        </label>
                        <Select
                            id="workEditor__offsetYSelect"
                            isDisabled={loading || isOffsetYDisabled()}
                            isLoading={loading}
                            placeholder="Zvoľ hodnotu"
                            value={offsetYValue}
                            onChange={value => {
                                setWatermarkOffsetY(value.value);
                                setChanged(true);
                            }}
                            options={offsetOptions.map(o => {
                                return {
                                    value: o,
                                    label: o + ' px'
                                };
                            })} />
                    </div>
                </div>
            </div>
        </>
    );
};

Offset.propTypes = {
    loading: PropTypes.bool.isRequired,
    watermarkPosition: PropTypes.string.isRequired,
    watermarkOffsetX: PropTypes.number.isRequired,
    setWatermarkOffsetX: PropTypes.func.isRequired,
    watermarkOffsetY: PropTypes.number.isRequired,
    setWatermarkOffsetY: PropTypes.func.isRequired,
    setChanged: PropTypes.func.isRequired
};

export default Offset;
