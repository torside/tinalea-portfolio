import React, { useEffect, useState } from 'react';
import * as PropTypes from 'prop-types';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';

const Size = props => {
    const {
        loading,
        watermarkSize,
        setWatermarkSize,
        setChanged
    } = props;

    const [value, setValue] = useState(watermarkSize);

    useEffect(() => {
        setValue(watermarkSize);
    }, [watermarkSize]);

    return (
        <>
            <label htmlFor="workEditor__watermarkSizeInput">
                Veľkosť vodoznaku
            </label>
            <InputRange
                id="workEditor__watermarkSizeInput"
                formatLabel={v => v + ' %'}
                disabled={loading}
                step={5}
                maxValue={100}
                minValue={10}
                value={value}
                onChange={setValue}
                onChangeComplete={val => {
                    setWatermarkSize(val);
                    setChanged(true);
                }} />
        </>
    );
};

Size.propTypes = {
    loading: PropTypes.bool.isRequired,
    watermarkSize: PropTypes.number.isRequired,
    setWatermarkSize: PropTypes.func.isRequired,
    setChanged: PropTypes.func.isRequired
};

export default Size;
