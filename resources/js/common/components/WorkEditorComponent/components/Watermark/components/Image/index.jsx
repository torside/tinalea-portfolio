import React, { useEffect, useState } from 'react';
import * as PropTypes from 'prop-types';
import Select from 'react-select';

const Image = props => {
    const {
        loading,
        watermarkList,
        setChanged,
        idWatermark,
        setIdWatermark
    } = props;

    const [value, setValue] = useState(null);

    useEffect(() => {
        const newValue = watermarkList.find(w => idWatermark === w.idWatermark);

        if (newValue) {
            setValue({
                value: newValue.idWatermark,
                label: newValue.title
            });

            return;
        }

        setValue(null);
    }, [idWatermark, watermarkList]);

    const watermarkListOptions = watermarkList.map(w => {
        return {
            value: w.idWatermark,
            label: w.title
        };
    });

    return (
        <>
            <div className="form-group">
                <label htmlFor="workEditor__watermarkImageSelect">
                    Zvoľ vodoznak
                </label>
                <Select
                    id="workEditor__watermarkImageSelect"
                    isDisabled={loading}
                    isLoading={loading}
                    placeholder="Zvoľ vodoznak"
                    value={value}
                    onChange={newValue => {
                        setIdWatermark(newValue.value);
                        setChanged(true);
                    }}
                    options={watermarkListOptions} />
            </div>
        </>
    );
};

Image.propTypes = {
    loading: PropTypes.bool.isRequired,
    watermarkList: PropTypes.array.isRequired,
    setChanged: PropTypes.func.isRequired,
    idWatermark: PropTypes.number,
    setIdWatermark: PropTypes.func.isRequired
};

Image.defaultProps = {
    idWatermark: null
};

export default Image;
