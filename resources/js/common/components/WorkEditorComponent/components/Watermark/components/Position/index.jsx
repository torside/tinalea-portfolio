import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faExternalLinkSquareAlt,
    faCircle,
    faArrowUp,
    faArrowRight,
    faArrowDown,
    faArrowLeft
} from '@fortawesome/free-solid-svg-icons';

const Position = props => {
    const {
        loading,
        watermarkPosition,
        setWatermarkPosition,
        setChanged
    } = props;

    const positions = [
        {
            position: 'top-left',
            icon: faExternalLinkSquareAlt,
            rotation: 270
        },
        {
            position: 'top',
            icon: faArrowUp,
            rotation: null
        },
        {
            position: 'top-right',
            icon: faExternalLinkSquareAlt,
            rotation: null

        },
        {
            position: 'left',
            icon: faArrowLeft,
            rotation: null
        },
        {
            position: 'center',
            icon: faCircle,
            rotation: null
        },
        {
            position: 'right',
            icon: faArrowRight,
            rotation: null
        },
        {
            position: 'bottom-left',
            icon: faExternalLinkSquareAlt,
            rotation: 180
        },
        {
            position: 'bottom',
            icon: faArrowDown,
            rotation: null
        },
        {
            position: 'bottom-right',
            icon: faExternalLinkSquareAlt,
            rotation: 90
        }
    ];

    const buttons = positions.map(p => {
        return (
            <div
                key={p.position}
                className={classNames(
                    'positionGrid__item',
                    { 'positionGrid__item--active': p.position === watermarkPosition }
                )}
                onClick={() => {
                    if (loading === true) {
                        return;
                    }

                    setWatermarkPosition(p.position);
                    setChanged(true);
                }}>
                <FontAwesomeIcon
                    size="2x"
                    icon={p.icon}
                    rotation={p.rotation} />
            </div>
        );
    });

    return (
        <div className="form-group">
            <label htmlFor="workEditor__watermarkPositionGrid">
                Pozícia vodoznaku
            </label>
            <div
                id="workEditor__watermarkPositionGrid"
                className={classNames(
                    'positionGrid',
                    { 'positionGrid--disabled': loading }
                )}>
                {buttons}
            </div>
        </div>
    );
};

Position.propTypes = {
    loading: PropTypes.bool.isRequired,
    watermarkPosition: PropTypes.string.isRequired,
    setWatermarkPosition: PropTypes.func.isRequired,
    setChanged: PropTypes.func.isRequired
};

export default Position;
