import React from 'react';
import * as PropTypes from 'prop-types';

const Option = props => {
    const { data } = props;

    return (
        <>
            <div className="watermarkImage__option row p-4">
                <div className="col-12 col-sm-6 col-md-8">
                    {data.label}
                </div>
                <div className="col-12 col-sm-6 col-md-4">
                    <img
                        src={'/images/image-service/watermarks/2000/' + data.imageFileName}
                        alt={data.imageFileName}
                        width="100%" />
                </div>
            </div>
        </>
    );
};

Option.propTypes = {
    data: PropTypes.shape({
        value: PropTypes.number.isRequired,
        label: PropTypes.string.isRequired,
        imageFileName: PropTypes.string.isRequired
    }).isRequired
};

export default Option;
