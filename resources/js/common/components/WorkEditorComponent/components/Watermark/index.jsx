import React from 'react';
import * as PropTypes from 'prop-types';
import Position from './components/Position/index.jsx';
import Offset from './components/Offset/index.jsx';
import Size from './components/Size/index.jsx';
import Image from './components/Image/index.jsx';

const Watermark = props => {
    const {
        loading,
        watermarkList,
        idWatermark,
        setIdWatermark,
        watermarkPosition,
        setWatermarkPosition,
        watermarkOffsetX,
        setWatermarkOffsetX,
        watermarkOffsetY,
        setWatermarkOffsetY,
        watermarkSize,
        setWatermarkSize,
        setChanged
    } = props;

    return (
        <>
            <div className="row">
                <div className="col-12 col-md-6 mb-3">
                    <Image
                        loading={loading}
                        setChanged={setChanged}
                        watermarkList={watermarkList}
                        idWatermark={idWatermark}
                        setIdWatermark={setIdWatermark} />
                </div>
                <div className="col-12 col-md-6 mb-3">
                    <div className="form-group">
                        <label className="d-none d-md-inline-block" htmlFor="workEditor__watermarkImageDeselect">
                            &nbsp;
                        </label>
                        <button
                            disabled={loading || idWatermark === null}
                            className="btn btn-danger btn-block"
                            id="workEditor__watermarkImageDeselect"
                            onClick={() => {
                                setIdWatermark(null);
                                setChanged(true);
                            }}>
                            Odstrániť
                        </button>
                    </div>
                </div>
            </div>
            {null !== idWatermark && (
                <>
                    <div className="row">
                        <div className="col-12">
                            <Position
                                loading={loading}
                                setChanged={setChanged}
                                watermarkPosition={watermarkPosition}
                                setWatermarkPosition={setWatermarkPosition} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <Offset
                                loading={loading}
                                setChanged={setChanged}
                                watermarkPosition={watermarkPosition}
                                watermarkOffsetX={watermarkOffsetX}
                                setWatermarkOffsetX={setWatermarkOffsetX}
                                watermarkOffsetY={watermarkOffsetY}
                                setWatermarkOffsetY={setWatermarkOffsetY} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <Size
                                loading={loading}
                                setChanged={setChanged}
                                watermarkSize={watermarkSize}
                                setWatermarkSize={setWatermarkSize} />
                        </div>
                    </div>
                </>
            )}
        </>
    );
};

Watermark.propTypes = {
    loading: PropTypes.bool.isRequired,
    watermarkList: PropTypes.array.isRequired,
    idWatermark: PropTypes.number,
    setIdWatermark: PropTypes.func.isRequired,
    watermarkPosition: PropTypes.string,
    setWatermarkPosition: PropTypes.func.isRequired,
    watermarkOffsetX: PropTypes.number,
    setWatermarkOffsetX: PropTypes.func.isRequired,
    watermarkOffsetY: PropTypes.number,
    setWatermarkOffsetY: PropTypes.func.isRequired,
    watermarkSize: PropTypes.number,
    setWatermarkSize: PropTypes.func.isRequired,
    setChanged: PropTypes.func.isRequired
};

Watermark.defaultProps = {
    idWatermark: null,
    watermarkPosition: '',
    watermarkOffsetX: 0,
    watermarkOffsetY: 0,
    watermarkSize: 100
};

export default Watermark;
