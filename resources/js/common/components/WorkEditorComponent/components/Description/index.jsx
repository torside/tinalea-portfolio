import React from 'react';
import * as PropTypes from 'prop-types';

const Description = props => {
    const {
        loading,
        title,
        setTitle,
        description,
        setDescription,
        setChanged
    } = props;

    return (
        <>
            <div className="row">
                <div className="col-12">
                    <div className="form-group">
                        <label htmlFor="workEditor__titleInput">
                            Názov
                        </label>
                        <input
                            value={title}
                            disabled={loading}
                            onChange={e => {
                                setTitle(e.target.value);
                                setChanged(true);
                            }}
                            type="text"
                            className="form-control"
                            id="workEditor__titleInput"
                            placeholder="Zadaj názov..." />
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <div className="form-group">
                        <label htmlFor="workEditor__descriptionInput">
                            Popis
                        </label>
                        <input
                            value={description}
                            disabled={loading}
                            onChange={e => {
                                setDescription(e.target.value);
                                setChanged(true);
                            }}
                            type="text"
                            className="form-control"
                            id="workEditor__descriptionInput"
                            placeholder="Zadaj popis..." />
                    </div>
                </div>
            </div>
        </>
    );
};

Description.propTypes = {
    loading: PropTypes.bool.isRequired,
    title: PropTypes.string,
    setTitle: PropTypes.func.isRequired,
    description: PropTypes.string,
    setDescription: PropTypes.func.isRequired,
    setChanged: PropTypes.func.isRequired
};

Description.defaultProps = {
    title: '',
    description: ''
};

export default Description;
