import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

const TabNav = props => {
    const {
        tabSettings,
        defaultTab
    } = props;

    const tabs = tabSettings.map((t, i) => {
        return (
            <a
                key={t.id}
                className={classNames(
                    'nav-item',
                    'nav-link',
                    { 'active': i === defaultTab },
                    { 'd-none': t.enabled !== true }
                )}
                href={'#' + t.id}
                data-toggle="tab">
                {t.label}
            </a>
        );
    });

    return (
        <nav>
            <div className="nav nav-tabs" id="nav-tab">
                {tabs}
            </div>
        </nav>
    );
};

TabNav.propTypes = {
    defaultTab: PropTypes.number.isRequired,
    tabSettings: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string.isRequired,
            id: PropTypes.string.isRequired,
            content: PropTypes.element.isRequired
        }).isRequired
    ).isRequired
};

export default TabNav;
