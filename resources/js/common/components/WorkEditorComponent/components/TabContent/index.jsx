import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

const TabContent = props => {
    const {
        tabSettings,
        defaultTab
    } = props;

    const tabs = tabSettings.map((t, i) => {
        return (
            <div
                key={t.id}
                className={classNames(
                    'tab-pane',
                    'fade',
                    'show',
                    { 'active': i === defaultTab }
                )}
                id={t.id}>
                {t.content}
            </div>
        );
    });

    return (
        <div className="tab-content">
            {tabs}
        </div>
    );
};

TabContent.propTypes = {
    defaultTab: PropTypes.number.isRequired,
    tabSettings: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string.isRequired,
            id: PropTypes.string.isRequired,
            content: PropTypes.element.isRequired
        }).isRequired
    ).isRequired
};

export default TabContent;
