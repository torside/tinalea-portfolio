import React from 'react';
import * as PropTypes from 'prop-types';

const Image = props => {
    const {
        idWork,
        idWatermark,
        watermarkPosition,
        watermarkOffsetX,
        watermarkOffsetY,
        watermarkSize
    } = props;

    return (
        <img
            src={
                '/auth/images/work-preview/'
                + idWork
                + '?idWatermark='
                + idWatermark
                + '&watermarkPosition='
                + watermarkPosition
                + '&watermarkOffsetX='
                + watermarkOffsetX
                + '&watermarkOffsetY='
                + watermarkOffsetY
                + '&watermarkSize='
                + watermarkSize}
            alt={idWork}
            width="100%" />
    );
};

Image.propTypes = {
    idWork: PropTypes.number.isRequired,
    idWatermark: PropTypes.number,
    watermarkPosition: PropTypes.string.isRequired,
    watermarkOffsetX: PropTypes.number.isRequired,
    watermarkOffsetY: PropTypes.number.isRequired,
    watermarkSize: PropTypes.number.isRequired
};

Image.defaultProps = {
    idWatermark: null
};

export default Image;
