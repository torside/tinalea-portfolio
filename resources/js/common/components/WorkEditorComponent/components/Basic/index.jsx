import React, { useEffect, useState } from 'react';
import * as PropTypes from 'prop-types';
import Select from 'react-select';

const Basic = props => {
    const {
        loading,
        categoryList,
        sectionList,
        idWorkCategory,
        setIdWorkCategory,
        workSectionIds,
        setWorkSectionIds,
        setChanged,
        order,
        setOrder
    } = props;

    const [categoryValue, setCategoryValue] = useState(null);
    const [sectionValue, setSectionValue] = useState([]);

    const categoryOptions = categoryList.map(c => {
        return {
            value: c.idWorkCategory,
            label: c.workCategoryName
        }
    });

    const sectionOptions = sectionList.map(s => {
        return {
            value: s.idWorkSection,
            label: s.workSectionName
        }
    });

    useEffect(() => {
        const newCategoryValue = categoryList.find(c => idWorkCategory === c.idWorkCategory);

        if (newCategoryValue) {
            setCategoryValue({
                value: newCategoryValue.idWorkCategory,
                label: newCategoryValue.workCategoryName
            });

            return;
        }

        setCategoryValue(null);
    }, [idWorkCategory, categoryList]);

    useEffect(() => {
        const newSectionValue = [];

        workSectionIds.forEach(s => {
            const section = sectionList.find(si => s === si.idWorkSection);

            if (typeof section !== 'undefined') {
                newSectionValue.push({
                    value: section.idWorkSection,
                    label: section.workSectionName
                });
            }
        });

        setSectionValue(newSectionValue);
    }, [workSectionIds, sectionList]);

    return (
        <>
            <div className="row">
                <div className="col-12">
                    <div className="form-group">
                        <label htmlFor="workEditor__categorySelect">
                            Kategória
                        </label>
                        <Select
                            id="workEditor__categorySelect"
                            isDisabled={loading}
                            isLoading={loading}
                            placeholder="Zvoľ kategóriu"
                            noOptionsMessage={() => 'Žiadne ďalšie možnosti'}
                            value={categoryValue}
                            onChange={newCategoryValue => {
                                setIdWorkCategory(newCategoryValue.value);
                                setChanged(true);
                            }}
                            options={categoryOptions} />
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <div className="form-group">
                        <label htmlFor="workEditor__sectionSelect">
                            Sekcie
                        </label>
                        <Select
                            id="workEditor__sectionSelect"
                            closeMenuOnSelect={false}
                            isDisabled={loading}
                            isLoading={loading}
                            placeholder="Zvoľ sekciu"
                            noOptionsMessage={() => 'Žiadne ďalšie možnosti'}
                            isMulti={true}
                            value={sectionValue}
                            onChange={newSectionValue => {
                                setWorkSectionIds(newSectionValue.map(s => s.value));
                                setChanged(true);
                            }}
                            options={sectionOptions} />
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <div className="form-group">
                        <label htmlFor="workEditor__orderInput">
                            Poradie
                        </label>
                        <input
                            value={order}
                            disabled={loading}
                            onChange={e => {
                                setOrder(e.target.value);
                                setChanged(true);
                            }}
                            required={true}
                            type="number"
                            step={1}
                            className="form-control"
                            id="workEditor__orderInput"
                            placeholder="Zadaj poradie..." />
                    </div>
                </div>
            </div>
        </>
    );
};

Basic.propTypes = {
    loading: PropTypes.bool.isRequired,
    setChanged: PropTypes.func.isRequired,
    categoryList: PropTypes.array.isRequired,
    sectionList: PropTypes.array.isRequired,
    idWorkCategory: PropTypes.number,
    setIdWorkCategory: PropTypes.func.isRequired,
    workSectionIds: PropTypes.array,
    setWorkSectionIds: PropTypes.func.isRequired,
    order: PropTypes.number.isRequired,
    setOrder: PropTypes.func.isRequired
};

Basic.defaultProps = {
    idWorkCategory: null,
    workSectionIds: []
};

export default Basic;
