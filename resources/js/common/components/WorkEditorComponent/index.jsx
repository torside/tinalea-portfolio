import React, { useCallback, useEffect, useState } from 'react';
import * as PropTypes from 'prop-types';
import axios from 'axios';
import Image from './components/Image/index.jsx';
import TabNav from './components/TabNav/index.jsx';
import TabContent from './components/TabContent/index.jsx';
import Basic from './components/Basic/index.jsx';
import Description from './components/Description/index.jsx';
import Watermark from './components/Watermark/index.jsx';

const WorkEditorComponent = props => {
    const { idWork } = props;

    const [loading, setLoading] = useState(true);
    const [workLoading, setWorkLoading] = useState(true);
    const [categoryListLoading, setCategoryListLoading] = useState(true);
    const [sectionListLoading, setSectionListLoading] = useState(true);
    const [watermarkListLoading, setWatermarkListLoading] = useState(true);

    const [changed, setChanged] = useState(false);
    const [defaultTab] = useState(0);

    const [categoryList, setCategoryList] = useState([]);
    const [sectionList, setSectionList] = useState([]);
    const [watermarkList, setWatermarkList] = useState([]);

    const [order, setOrder] = useState(0);
    const [title, setTitle] = useState(null);
    const [description, setDescription] = useState(null);
    const [idWorkCategory, setIdWorkCategory] = useState(null);
    const [workSectionIds, setWorkSectionIds] = useState([]);
    const [idWatermark, setIdWatermark] = useState(null);
    const [watermarkPosition, setWatermarkPosition] = useState('top-left');
    const [watermarkOffsetX, setWatermarkOffsetX] = useState(0);
    const [watermarkOffsetY, setWatermarkOffsetY] = useState(0);
    const [watermarkSize, setWatermarkSize] = useState(100);

    const tabSettings = [
        {
            label: 'Základne',
            id: 'workEditor-tab-basic',
            content: <Basic
                loading={loading}
                setChanged={setChanged}
                categoryList={categoryList}
                sectionList={sectionList}
                idWorkCategory={idWorkCategory}
                setIdWorkCategory={setIdWorkCategory}
                workSectionIds={workSectionIds}
                setWorkSectionIds={setWorkSectionIds}
                order={order}
                setOrder={setOrder} />,
            enabled: true
        },
        {
            label: 'Popis',
            id: 'workEditor-tab-description',
            content: <Description
                loading={loading}
                title={title || ''}
                setTitle={setTitle}
                description={description || ''}
                setDescription={setDescription}
                setChanged={setChanged} />,
            enabled: true
        },
        {
            label: 'Vodoznak',
            id: 'workEditor-tab-watermark',
            content: <Watermark
                loading={loading}
                watermarkList={watermarkList}
                idWatermark={idWatermark}
                setIdWatermark={setIdWatermark}
                watermarkPosition={watermarkPosition}
                setWatermarkPosition={setWatermarkPosition}
                watermarkOffsetX={watermarkOffsetX}
                setWatermarkOffsetX={setWatermarkOffsetX}
                watermarkOffsetY={watermarkOffsetY}
                setWatermarkOffsetY={setWatermarkOffsetY}
                watermarkSize={watermarkSize}
                setWatermarkSize={setWatermarkSize}
                setChanged={setChanged} />,
            enabled: false
        }
    ];

    const deleteImage = useCallback(() => {
        const data = {
            idWork
        };

        setLoading(true);

        axios.post('/api/work/delete', data)
            .then(() => {
                setChanged(false);
                window.location.reload();
            })
            .catch(() => {
            })
            .then(() => setLoading(false));
    }, [idWork]);

    const generateFormats = useCallback(() => {
        const data = {
            idWork
        };

        setLoading(true);

        axios.post('/api/work/generate-formats', data)
            .then(() => setChanged(false))
            .catch(() => {
            })
            .then(() => setLoading(false));
    }, [idWork]);

    const update = useCallback(() => {
        const data = {
            idWork,
            order,
            title,
            description,
            idWorkCategory,
            workSectionIds,
            idWatermark,
            watermarkPosition,
            watermarkOffsetX,
            watermarkOffsetY,
            watermarkSize
        };

        setLoading(true);

        axios.post('/api/work/update', data)
            .then(() => setChanged(false))
            .catch(() => {
            })
            .then(() => setLoading(false));
    }, [
        idWork,
        order,
        title,
        description,
        idWorkCategory,
        workSectionIds,
        idWatermark,
        watermarkPosition,
        watermarkOffsetX,
        watermarkOffsetY,
        watermarkSize
    ]);

    const loadCategoryList = useCallback(() => {
        setCategoryListLoading(true);

        const data = {
            length: 50,
            order: [
                {
                    dir: 'desc',
                    column: 0
                }
            ]
        };

        axios.post('/api/work-category/list', data)
            .then(response => setCategoryList(response.data.data.data))
            .catch(() => {
            })
            .then(() => setCategoryListLoading(false));
    }, []);

    const loadSectionList = useCallback(() => {
        setSectionListLoading(true);

        const data = {
            length: 50,
            order: [
                {
                    dir: 'desc',
                    column: 0
                }
            ]
        };

        axios.post('/api/work-section/list', data)
            .then(response => setSectionList(response.data.data.data))
            .catch(() => {
            })
            .then(() => setSectionListLoading(false));
    }, []);

    const loadWatermarkList = useCallback(() => {
        setWatermarkListLoading(true);

        const data = {
            length: 50,
            order: [
                {
                    dir: 'desc',
                    column: 0
                }
            ]
        };

        axios.post('/api/watermark/list', data)
            .then(response => setWatermarkList(response.data.data.data))
            .catch(() => {
            })
            .then(() => setWatermarkListLoading(false));
    }, []);

    useEffect(() => {
        setLoading(
            workLoading
            || categoryListLoading
            || sectionListLoading
            || watermarkListLoading
        );
    }, [workLoading, categoryListLoading, sectionListLoading, watermarkListLoading]);

    useEffect(() => {
        loadCategoryList();
        loadSectionList();
        loadWatermarkList();
    }, [loadCategoryList, loadSectionList, loadWatermarkList]);

    useEffect(() => {
        setWorkLoading(true);

        axios.get('/api/work/index/' + idWork)
            .then(r => {
                setIdWorkCategory(r.data.data.idWorkCategory);
                setWorkSectionIds(r.data.data.work_sections.map(s => s.idWorkSection));
                setOrder(r.data.data.order);
                setTitle(r.data.data.title);
                setDescription(r.data.data.description);

                if (null !== r.data.data.work_watermark) {
                    const watermark = r.data.data.work_watermark;
                    setIdWatermark(watermark.idWatermark);
                    setWatermarkPosition(watermark.position);
                    setWatermarkOffsetX(watermark.offsetX);
                    setWatermarkOffsetY(watermark.offsetY);
                    setWatermarkSize(watermark.size);
                }
            })
            .catch()
            .then(() => setWorkLoading(false));
    }, [idWork]);

    return (
        <div className="workEditor">
            <div className="row mb-3">
                <div className="col-12">
                    <TabNav defaultTab={defaultTab} tabSettings={tabSettings} />
                </div>
            </div>
            <div className="row">
                <div className="col-12 col-sm-6 col-md-4 mb-5">
                    <Image
                        idWork={idWork}
                        idWatermark={idWatermark}
                        watermarkPosition={watermarkPosition}
                        watermarkOffsetX={watermarkOffsetX}
                        watermarkOffsetY={watermarkOffsetY}
                        watermarkSize={watermarkSize} />
                </div>
                <div className="col-12 col-sm-6 col-md-8 mb-5">
                    <TabContent defaultTab={defaultTab} tabSettings={tabSettings} />
                </div>
            </div>
            <div className="row">
                <div className="col-12 col-md-4 mb-3">
                    <button
                        className="btn btn-danger btn-block"
                        onClick={deleteImage}>
                        Zmazať
                    </button>
                </div>
                <div className="col-12 col-md-4 mb-3">
                    <button
                        className="btn btn-light btn-block"
                        onClick={generateFormats}>
                        Pregenerovať
                    </button>
                </div>
                <div className="col-12 col-md-4 mb-3">
                    <button
                        disabled={changed === false || loading === true}
                        className="btn btn-primary btn-block"
                        onClick={update}>
                        Uložiť
                    </button>
                </div>
            </div>
        </div>
    );
};

WorkEditorComponent.propTypes = {
    idWork: PropTypes.number.isRequired
};

export default WorkEditorComponent;
