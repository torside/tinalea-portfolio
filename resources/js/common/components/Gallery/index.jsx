import React, { useCallback, useContext, useEffect, useState } from 'react';
import classNames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faExpand, faCompress } from '@fortawesome/free-solid-svg-icons';
import ReactSwipeEvents from 'react-swipe-events';
import Slider from './components/Slider/index.jsx';
import HeadContext from './../../../providers/HeadProvider/context.jsx';
import GalleryContext from './../../../providers/GalleryProvider/context.jsx';
import { useHistory } from 'react-router';

const Gallery = () => {
    const history = useHistory();

    const {
        setThemeColor
    } = useContext(HeadContext);

    const {
        activeWorkList,
        category,
        section,
        idWork
    } = useContext(GalleryContext);

    const [fullscreen, setFullscreen] = useState(false);
    const [inverted] = useState(false);

    const closeGallery = useCallback(() => {
        history.push('/work/' + category);
        setThemeColor('#252525');
    }, [history, category, setThemeColor]);

    const moveToPrevImage = useCallback(() => {
        const currentIndex = activeWorkList.findIndex(w => w.idWork === parseInt(idWork));
        const lastIndex = activeWorkList.length - 1;

        if (currentIndex === null) {
            closeGallery();
            return;
        }

        if (currentIndex === 0) {
            history.push('/work/' + category + '/' + section + '/' + activeWorkList[lastIndex].idWork);
            return;
        }

        history.push('/work/' + category + '/' + section + '/' + activeWorkList[currentIndex - 1].idWork);
    }, [history, activeWorkList, closeGallery, category, section, idWork]);

    const moveToNextImage = useCallback(() => {
        const currentIndex = activeWorkList.findIndex(w => w.idWork === parseInt(idWork));
        const lastIndex = activeWorkList.length - 1;

        if (currentIndex === null) {
            closeGallery();
            return;
        }

        if (currentIndex === lastIndex) {
            history.push('/work/' + category + '/' + section + '/' + activeWorkList[0].idWork);
            return;
        }

        history.push('/work/' + category + '/' + section + '/' + activeWorkList[currentIndex + 1].idWork);
    }, [history, activeWorkList, closeGallery, category, section, idWork]);

    const handleKeyboardControl = useCallback(e => {
        switch (e.keyCode) {
            case 27:
                closeGallery();
                return;
            case 37:
                moveToPrevImage();
                return;
            case 39:
                moveToNextImage();
                return;
            default:
                return;
        }
    }, [closeGallery, moveToPrevImage, moveToNextImage]);

    let currentImage = activeWorkList.find(w => w.idWork === parseInt(idWork));

    if (typeof currentImage === 'undefined') {
        currentImage = activeWorkList[0];
    }

    useEffect(() => {
        document.body.style.overflow = 'hidden';

        return () => {
            document.body.style.overflow = 'scroll';
        };
    }, []);

    useEffect(() => {
        setThemeColor('#000000');
    }, [setThemeColor]);

    useEffect(() => {
        document.addEventListener('keydown', handleKeyboardControl);

        return () => {
            document.removeEventListener('keydown', handleKeyboardControl);
        };
    }, [handleKeyboardControl]);

    return (
        <>
            <ReactSwipeEvents onSwipedLeft={() => moveToNextImage()} onSwipedRight={() => moveToPrevImage()}>
                <div
                    className={classNames(
                        'gallery',
                        { 'gallery--fullscreen': fullscreen },
                        { 'gallery--inverted': inverted }
                    )}>
                    <div className="gallery__target" onClick={closeGallery} />
                    <div className="gallery__header">
                        <div className="header__state">
                            <span className="state__counts">
                                {(activeWorkList.findIndex(w => w.idWork === parseInt(idWork)) + 1)} of {activeWorkList.length}
                            </span>
                        </div>
                        <div className="header__about">
                            <div className="about__title">
                                {currentImage.title}
                            </div>
                            <div className="about__description">
                                {currentImage.description}
                            </div>
                        </div>
                        <div className="header__icons">
                            <span className="icons__icon" onClick={() => setFullscreen(!fullscreen)}>
                                <FontAwesomeIcon icon={fullscreen ? faCompress : faExpand} size="2x" />
                            </span>
                            <span className="icons__icon" onClick={closeGallery}>
                                <FontAwesomeIcon icon={faTimes} size="2x" />
                            </span>
                        </div>
                    </div>
                    <div
                        className="gallery__image"
                        style={{ backgroundImage: 'url(/images/image-service/work/xl/' + currentImage.imageFileName + ')' }} />
                    <div className={classNames(
                        'gallery__slider',
                        { 'd-none': fullscreen }
                    )}>
                        <Slider workList={activeWorkList} currentImage={currentImage} />
                    </div>
                </div>
            </ReactSwipeEvents>
        </>
    );
};

export default Gallery;
