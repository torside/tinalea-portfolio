import React, { useContext, useEffect } from 'react';
import classNames from 'classnames';
import GalleryContext from './../../../../../providers/GalleryProvider/context.jsx';
import { useHistory } from 'react-router';
import * as PropTypes from 'prop-types';

const Slider = props => {
    const history = useHistory();

    const {
        activeWorkList,
        category,
        section,
        idWork
    } = useContext(GalleryContext);

    const {
        currentImage
    } = props;

    useEffect(() => {
        document.getElementById('items__thumbnail__' + currentImage.idWork)
            .scrollIntoViewIfNeeded(true);
    }, [currentImage]);

    const sliderItems = activeWorkList.map(w => {
        return (
            <div
                key={w.idWork}
                className={classNames(
                    'items__thumbnail',
                    { 'items__thumbnail--active': (w.idWork === parseInt(idWork)) }
                )}
                style={{ backgroundImage: 'url("/images/image-service/work/xs/' + w.imageFileName + '")' }}
                id={'items__thumbnail__' + w.idWork}
                onClick={() => history.push('/work/' + category + '/' + section + '/' + w.idWork)} />
        );
    });

    return (
        <div className="slider">
            <div className="slider__items">
                {sliderItems}
            </div>
        </div>
    );
};

Slider.propTypes = {
    currentImage: PropTypes.object.isRequired
};

export default Slider;
