import React, { useEffect, useState } from 'react';
import HeadContext from './context.jsx';
import * as PropTypes from 'prop-types';

const HeadProvider = ({ children }) => {
    const defaultHead = {
        'title': 'Tinalea Portfolio',
        'description': 'My personal portfolio.',
        'canonical': 'tinalea.com',
        'meta': {
            'name': {
                'keywords': 'Tinalea,Portfolio',
                'theme-color': '#252525',
                'msapplication-navbutton-color': '#252525',
                'apple-mobile-web-app-status-bar-style': '#252525',
            },
        },
    };

    const [head, setHead] = useState(defaultHead);
    const [titleParts, setTitleParts] = useState([]);
    const [title, setTitle] = useState(defaultHead.title);
    const [themeColor, setThemeColor] = useState(defaultHead.meta.name['theme-color']);

    useEffect(() => {
        setTitle(
            titleParts.filter(p => [null, undefined, NaN, 'default'].indexOf(p) === -1)
                .map(p => {
                    const words = p.split('-');

                    for (let i = 0; i < words.length; i++) {
                        words[i] = words[i][0].toUpperCase() + words[i].substr(1);
                    }

                    return words.join(' ');
                })
                .join(' | ')
        );
    }, [titleParts]);

    useEffect(() => {
        setHead(prevState => {
            return {
                ...prevState,
                title: title,
                meta: {
                    ...prevState.meta || {},
                },
            }
        });
    }, [title]);

    useEffect(() => {
        setHead(prevState => {
            return {
                ...prevState,
                'meta': {
                    ...prevState.meta,
                    'name': {
                        ...prevState.meta.name,
                        'keywords': prevState.meta.name.keywords,
                        'theme-color': themeColor,
                        'msapplication-navbutton-color': themeColor,
                        'apple-mobile-web-app-status-bar-style': themeColor
                    }
                }
            }
        });
    }, [themeColor]);

    return (
        <HeadContext.Provider value={{
            head,
            setTitleParts,
            setThemeColor
        }}>
            {children}
        </HeadContext.Provider>
    );
};

HeadProvider.propTypes = {
    children: PropTypes.node.isRequired
};

export default HeadProvider;
