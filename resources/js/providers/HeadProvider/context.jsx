import { createContext } from 'react';

const HeadContext = createContext({
    head: {},
    setTitleParts: () => {
    },
    setThemeColor: () => {
    }
});

export default HeadContext;
