import { createContext } from 'react';

const GalleryContext = createContext({
    works: {},
    activeWorkList: [],
    category: '',
    section: '',
    idWork: null,
    setActiveWorkList: () => {
    },
    getWorkListByParams: () => {
    }
});

export default GalleryContext;