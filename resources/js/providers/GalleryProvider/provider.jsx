import React, { useEffect, useState, useCallback } from 'react';
import GalleryContext from './context.jsx';
import * as PropTypes from 'prop-types';
import { useParams } from 'react-router';
import axios from 'axios';

const GalleryProvider = ({ children }) => {
    const [works, setWorks] = useState({});
    const [activeWorkList, setActiveWorkList] = useState([]);

    const {
        category,
        section,
        idWork
    } = useParams();

    const getWorkListByParams = useCallback((categoryParam, sectionParam) => {
        if (
            typeof works[categoryParam] === 'undefined'
            || typeof works[categoryParam][sectionParam] === 'undefined'
        ) {
            return [];
        }

        return works[categoryParam][sectionParam];
    }, [works]);

    useEffect(() => {
        axios.get('/api/work/portfolio')
            .then(response => setWorks(response.data.data))
            .catch()
            .then();
    }, []);

    useEffect(() => {
        let newActiveWorkList = getWorkListByParams(category, section);

        if (newActiveWorkList.findIndex(w => w.idWork === parseInt(idWork)) === -1) {
            newActiveWorkList = [];
        }

        setActiveWorkList(newActiveWorkList);
    }, [getWorkListByParams, category, section, idWork]);

    return (
        <GalleryContext.Provider value={{
            works,
            activeWorkList,
            category,
            section,
            idWork,
            setActiveWorkList,
            getWorkListByParams
        }}>
            {children}
        </GalleryContext.Provider>
    );
};

GalleryProvider.propTypes = {
    children: PropTypes.node.isRequired
};

export default GalleryProvider;
