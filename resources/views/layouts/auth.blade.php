<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.auth.head')
@include('layouts.auth.body')
</html>
