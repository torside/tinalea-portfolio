<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm px-4">
    <div class="container">
        @include('layouts.auth.body.app.nav.brand')
        @include('layouts.auth.body.app.nav.toggler')
        @include('layouts.auth.body.app.nav.collapse')
    </div>
</nav>
