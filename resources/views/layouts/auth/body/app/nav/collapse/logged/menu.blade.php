<li class="nav-item">
    <a class="nav-link" href="{{ route('home') }}">Domov</a>
</li>

<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Práca</a>
    <div class="dropdown-menu">
        <a class="dropdown-item" href="{{ route('auth.work') }}">Galéria</a>
    </div>
</li>

<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Nastavenia</a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('auth.work-categories') }}">Kategórie</a>
        <a class="dropdown-item" href="{{ route('auth.work-sections') }}">Sekcie</a>
        <a class="dropdown-item d-none" href="{{ route('auth.watermarks') }}">Vodoznaky</a>
    </div>
</li>
