<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav">
        @guest
            @include('layouts.auth.body.app.nav.collapse.guest')
        @else
            @include('layouts.auth.body.app.nav.collapse.logged')
        @endguest
    </ul>
</div>
