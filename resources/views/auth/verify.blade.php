@extends('layouts.auth')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Overenie mailovej adresy</div>

                    <div class="card-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                Verifikačný mail bol odoslaný na vašu adresu.
                            </div>
                        @endif

                        Pred pokračovaním prosím skontrolujte váš mail.
                        Ak ste neobdržali verifikačný mail,
                        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                            @csrf
                            <button type="submit" class="btn btn-link p-0 m-0 align-baseline">
                                kliknite sem pre opätovne zaslanie mailu.
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
