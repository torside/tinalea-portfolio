@extends('layouts.auth')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Vodoznaky</div>
                    <div class="card-body">
                        <div id="watermarks" class="watermarks"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
