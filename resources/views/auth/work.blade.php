@extends('layouts.auth')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Práca</div>
                    <div class="card-body">
                        <div id="work"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
