<?php
/**
 * @var string $title
 * @var string $ogImage
 * @var string $ogDescription
 */
?>
    <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-3Y3NNEM9W3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'G-3Y3NNEM9W3');
    </script>
    <title>{{ $title }}</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
    <link href="{{ mix('/css/portfolio/style.css') }}" rel="stylesheet">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:title" content="{{ $title }}" />
    <meta property="og:image" content="{{ $ogImage }}" />
    <meta property="og:description" content="{{ $ogDescription }}">
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ url()->full() }}" />
</head>
<body>
<div id="application">
</div>
<script src="{{ mix('/js/portfolio/index.js') }}"></script>
</body>
</html>

