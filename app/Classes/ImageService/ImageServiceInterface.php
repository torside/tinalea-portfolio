<?php

namespace App\Classes\ImageService;

use Illuminate\Filesystem\FilesystemAdapter as Filesystem;
use Illuminate\Http\UploadedFile;

interface ImageServiceInterface
{
    /**
     * ImageServiceInterface constructor.
     *
     * @param Filesystem $publicStorage
     * @param Filesystem $privateStorage
     * @param string $name
     * @param array $formats
     */
    public function __construct(Filesystem $publicStorage, Filesystem $privateStorage, string $name, array $formats);

    /**
     * @param UploadedFile $uploadedFile
     * @param string $filenamePrefix
     *
     * @return string
     */
    public function storeOriginalImage(UploadedFile $uploadedFile, $filenamePrefix = ''): string;

    /**
     * @param string $filename
     */
    public function storeFormattedImages(string $filename);

    /**
     * @param string $filename
     *
     * @return string
     */
    public function getOriginalImageFilePath(string $filename): string;
}
