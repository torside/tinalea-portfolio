<?php

namespace App\Classes\ImageService\Implementation;

use App\Classes\ImageService\AbstractImageService;
use App\Repositories\Watermark\Watermark;
use App\Repositories\Watermark\WatermarkService;
use Intervention\Image\Image as InterventionImage;

final class WatermarkImageService extends AbstractImageService
{
    /** @var WatermarkService $watermarkService */
    private $watermarkService;

    /**
     * @param string $filename
     */
    protected function afterStoreOriginalImage(string $filename)
    {
        if (is_null($this->watermarkService)) {
            return;
        }

        $this->watermarkService
            ->create([
                Watermark::COLUMN_IMAGE_FILE_NAME => $filename
            ]);

        $this->storeFormattedImages($filename);
    }

    /**
     * @param InterventionImage $interventionImage
     */
    protected function beforeFormatModifier(InterventionImage $interventionImage)
    {
    }

    /**
     * @param InterventionImage $interventionImage
     */
    protected function afterFormatModifier(InterventionImage $interventionImage)
    {
    }

    /**
     * @param WatermarkService $watermarkService
     *
     * @return WatermarkImageService
     */
    public function setWatermarkService(WatermarkService $watermarkService): WatermarkImageService
    {
        $this->watermarkService = $watermarkService;
        return $this;
    }
}
