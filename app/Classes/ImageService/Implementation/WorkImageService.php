<?php

namespace App\Classes\ImageService\Implementation;

use App\Classes\ImageService\AbstractImageService;
use App\Jobs\WorkGenerateFormats;
use App\Repositories\Work\Work;
use App\Repositories\Work\WorkService;
use Intervention\Image\Image as InterventionImage;

final class WorkImageService extends AbstractImageService
{
    /** @var WorkService $workService */
    private $workService;

    /**
     * @param string $filename
     */
    protected function afterStoreOriginalImage(string $filename)
    {
        if (is_null($this->workService)) {
            return;
        }

        /** @var Work $work */
        $work = $this->workService
            ->create([
                Work::COLUMN_IMAGE_FILE_NAME => $filename
            ]);

        WorkGenerateFormats::dispatch($work);
    }

    /**
     * @param InterventionImage $interventionImage
     */
    protected function beforeFormatModifier(InterventionImage $interventionImage)
    {
        $interventionImage->orientate();
    }

    /**
     * @param InterventionImage $interventionImage
     */
    protected function afterFormatModifier(InterventionImage $interventionImage)
    {
    }

    /**
     * @param WorkService $workService
     *
     * @return WorkImageService
     */
    public function setWorkService(WorkService $workService): WorkImageService
    {
        $this->workService = $workService;
        return $this;
    }
}
