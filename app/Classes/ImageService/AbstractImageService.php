<?php

namespace App\Classes\ImageService;

use Illuminate\Filesystem\FilesystemAdapter as Filesystem;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Image as InterventionImage;
use Intervention\Image\ImageManagerStatic;

abstract class AbstractImageService implements ImageServiceInterface
{
    const ORIGINAL_IMAGE_DIRECTORY = 'original';

    /** @var Filesystem $publicStorage */
    private $publicStorage;

    /** @var Filesystem $privateStorage */
    private $privateStorage;

    /** @var string $name */
    private $name;

    /** @var array $formats */
    private $formats;

    /**
     * AbstractImageService constructor.
     *
     * @param Filesystem $publicStorage
     * @param Filesystem $privateStorage
     * @param string $name
     * @param array $formats
     */
    public function __construct(Filesystem $publicStorage, Filesystem $privateStorage, string $name, array $formats)
    {
        $this->publicStorage = $publicStorage;
        $this->privateStorage = $privateStorage;
        $this->name = $name;
        $this->formats = $formats;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @param string $filenamePrefix
     *
     * @return string
     */
    public function storeOriginalImage(UploadedFile $uploadedFile, $filenamePrefix = ''): string
    {
        $this->init();

        /** @var string $filename */
        $filename = $this->generateNewImageFileName($uploadedFile, $filenamePrefix);

        @file_put_contents(
            $this->getOriginalImageFilePath($filename),
            file_get_contents($uploadedFile->getPathname())
        );

        $this->afterStoreOriginalImage($filename);

        return $filename;
    }

    /**
     * @param string $filename
     * @param bool $overwrite
     */
    public function storeFormattedImages(string $filename, $overwrite = false)
    {
        $this->init();

        /**
         * @var string $format
         * @var int $width
         */
        foreach ($this->formats as $format => $width) {
            /** @var string $imageFilePath */
            $imageFilePath = $this->generateNewImageFilePath($format, $filename);

            if ($this->publicStorage->exists($imageFilePath) === true && $overwrite === false) {
                continue;
            }

            /** @var string $originalImagePath */
            $originalImagePath = $this->getOriginalImageFilePath($filename);

            if (file_exists($originalImagePath) !== true) {
                continue;
            }

            /** @var InterventionImage $interventionImage */
            $interventionImage = ImageManagerStatic::make($originalImagePath);

            $this->beforeFormatModifier($interventionImage);

            $interventionImage->resize($width, null, function (Constraint $constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $this->afterFormatModifier($interventionImage);

            $interventionImage->save(
                $this->publicStorage
                    ->path($imageFilePath)
            );

            $interventionImage->destroy();
        }
    }

    /**
     * @param string $filename
     *
     * @return string
     */
    public function getOriginalImageFilePath(string $filename): string
    {
        return $this->privateStorage
            ->path(
                $this->generateNewImageFilePath(
                    self::ORIGINAL_IMAGE_DIRECTORY,
                    $filename
                )
            );
    }

    /**
     * @param string $filename
     *
     * @return bool
     */
    public function deleteOriginalImage(string $filename): bool
    {
        return $this->privateStorage->delete(
            $this->generateNewImageFilePath(
                self::ORIGINAL_IMAGE_DIRECTORY,
                $filename
            )
        );
    }

    /**
     * @param string $filename
     */
    protected abstract function afterStoreOriginalImage(string $filename);

    /**
     * @param InterventionImage $interventionImage
     */
    protected abstract function beforeFormatModifier(InterventionImage $interventionImage);

    /**
     * @param InterventionImage $interventionImage
     */
    protected abstract function afterFormatModifier(InterventionImage $interventionImage);

    /**
     * @param string $format
     * @param string $filename
     *
     * @return string
     */
    private function generateNewImageFilePath(string $format, string $filename): string
    {
        return $this->name
            . DIRECTORY_SEPARATOR
            . $format
            . DIRECTORY_SEPARATOR
            . $filename;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @param string $filenamePrefix
     *
     * @return string
     */
    private function generateNewImageFileName(UploadedFile $uploadedFile, string $filenamePrefix): string
    {
        /** @var string $filename */
        $filename = Str::slug(str_replace(".{$uploadedFile->getClientOriginalExtension()}", '', $uploadedFile->getClientOriginalName()));

        return ($filenamePrefix !== '' ? "{$filenamePrefix}_" : '') . $filename . '.' . $uploadedFile->getClientOriginalExtension();
    }

    /**
     *
     */
    private function init()
    {
        /** @var string $originalDirectory */
        $originalDirectory = $this->name . DIRECTORY_SEPARATOR . self::ORIGINAL_IMAGE_DIRECTORY;

        if (true !== $this->privateStorage->exists($originalDirectory)) {
            $this->privateStorage
                ->createDir($originalDirectory);
        }

        /**
         * @var string $format
         * @var int $width
         */
        foreach ($this->formats as $format => $width) {
            /** @var string $formatsDirectory */
            $formatsDirectory = $this->name . DIRECTORY_SEPARATOR . $format;

            if (true !== $this->publicStorage->exists($formatsDirectory)) {
                $this->publicStorage
                    ->createDir($formatsDirectory);
            }
        }
    }
}
