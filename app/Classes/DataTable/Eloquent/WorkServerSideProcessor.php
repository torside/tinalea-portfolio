<?php

namespace App\Classes\DataTable\Eloquent;

use App\Repositories\Work\Work;
use App\Repositories\WorkCategory\WorkCategory;
use App\Repositories\WorkSection\WorkSection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

final class WorkServerSideProcessor extends ServerSideProcessor
{
    /** @var array array */
    protected $columns = [
        ['column' => Work::PRIMARY_KEY],
        ['column' => Work::COLUMN_IMAGE_FILE_NAME],
        ['column' => Work::COLUMN_ACTIVE],
        ['column' => Work::COLUMN_ORDER],
        ['column' => Work::COLUMN_CREATED_AT],
        ['column' => Work::COLUMN_UPDATED_AT]
    ];

    /**
     * @return Builder
     */
    protected function createQuery(): Builder
    {
        return $this->model::query()
            ->with(Work::RELATION_WORK_SECTIONS);
    }

    /**
     * @param Builder $builder
     * @param Request $request
     */
    protected function whereConditionsCallback(Builder $builder, Request $request)
    {
        /** @var int|null $idCategory */
        $idCategory = $this->getIdCategory($request);

        if (!is_null($idCategory)) {
            $builder->where(WorkCategory::PRIMARY_KEY, $idCategory);
        }

        $workSectionIds = $this->getWorkSectionIds($request);

        if (!is_null($workSectionIds)) {
            $builder->wherehas(Work::RELATION_WORK_SECTIONS, function (Builder $builder) use ($workSectionIds) {
                $builder->whereIn('work_section_pairings.' . WorkSection::PRIMARY_KEY, $workSectionIds);
            });
        }
    }

    /**
     * @param Request $request
     *
     * @return int|null
     */
    protected function getIdCategory(Request $request)
    {
        /** @var int|null $idCategory */
        $idCategory = $request->request
            ->get('idCategory', null);

        return is_null($idCategory) ? null : (int)$idCategory;
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    protected function getWorkSectionIds(Request $request)
    {
        /** @var array|null $workSectionIds */
        $workSectionIds = $request->request
            ->get('workSectionIds', null);

        return is_array($workSectionIds) ? $workSectionIds : null;
    }
}
