<?php

namespace App\Classes\DataTable\Eloquent;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ServerSideProcessor
{
    /** @var string $model */
    protected $model;

    /** @var array $columns */
    protected $columns = [];

    /** @var int $defaultStart */
    private $defaultStart = 0;

    /** @var int $defaultLength */
    private $defaultLength = 3;

    /**
     * ServerSideProcessor constructor.
     *
     * @param string $model
     *
     * @throws Exception
     */
    public function __construct(string $model)
    {
        if (!is_subclass_of($model, Model::class)) {
            throw new Exception("Class {$model} is not an child of " . Model::class);
        }

        $this->model = $model;
    }

    /**
     * @return Builder
     */
    protected function createQuery(): Builder
    {
        return $this->model::query();
    }

    /**
     * @param Builder $builder
     * @param Request $request
     */
    protected function whereConditionsCallback(Builder $builder, Request $request)
    {
    }

    /**
     * @param Builder $builder
     * @param Request $request
     */
    protected function fulltextRulesCallback(Builder $builder, Request $request)
    {
    }

    /**
     * @param array $results
     *
     * @return array
     */
    protected function resultsMapper(array $results): array
    {
        return $results;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function search(Request $request): array
    {
        /** @var array $result */
        $result = [
            'draw' => $this->getDraw($request)
        ];

        /** @var Builder $builder */
        $builder = $this->createQuery();

        $builder->where(function (Builder $builder) use ($request) {
            $this->whereConditionsCallback($builder, $request);
        });

        $result['recordsTotal'] = $builder->count();

        if ('' !== $this->getSearchValue($request)) {
            $builder->where(function (Builder $builder) use ($request) {
                $this->fulltextRulesCallback($builder, $request);
            });
        }

        foreach ($this->getOrder($request) as $o) {
            if (is_null($o['column'])) {
                continue;
            }

            $builder->orderBy($o['column'], $o['dir']);
        }

        $result['recordsFiltered'] = $builder->count();

        $builder = $builder->offset($this->getStart($request))
            ->limit($this->getLength($request));

        /** @var array $results */
        $results = $builder->get()
            ->toArray();

        $result['data'] = $this->resultsMapper($results);

        return $result;
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    protected function getSearchValue(Request $request): string
    {
        /** @var array $search */
        $search = (array)$request->request
            ->get('search', []);

        if (empty($search['value'])) {
            return '';
        }

        /** @var string $searchValue */
        $searchValue = (string)$search['value'];

        if (empty($searchValue)) {
            $searchValue = '';
        }

        return $searchValue;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    protected function getOrder(Request $request): array
    {
        /** @var array $order */
        $order = array_filter((array)$request->request
            ->get('order', []));

        return array_map(function (array $o) {
            return [
                'dir' => $o['dir'],
                'column' => $this->columns[(int)$o['column']]['column']
            ];
        }, $order);
    }

    /**
     * @param Request $request
     *
     * @return int
     */
    protected function getDraw(Request $request): int
    {
        /** @var int $draw */
        $draw = (int)$request->request
            ->get('draw', 1);

        if (empty($draw)) {
            $draw = 1;
        }

        return $draw;
    }

    /**
     * @param Request $request
     *
     * @return int
     */
    protected function getStart(Request $request): int
    {
        /** @var int $start */
        $start = (int)$request->request
            ->get('start', $this->defaultStart);

        if (empty($start)) {
            $start = $this->defaultStart;
        }

        return $start;
    }

    /**
     * @param Request $request
     *
     * @return int
     */
    protected function getLength(Request $request): int
    {
        /** @var int $length */
        $length = (int)$request->request
            ->get('length', $this->defaultLength);

        if (empty($length)) {
            $length = $this->defaultLength;
        }

        return $length;
    }

    /**
     * @param int $defaultStart
     *
     * @return ServerSideProcessor
     */
    public function setDefaultStart(int $defaultStart): ServerSideProcessor
    {
        $this->defaultStart = $defaultStart;
        return $this;
    }

    /**
     * @param int $defaultLength
     *
     * @return ServerSideProcessor
     */
    public function setDefaultLength(int $defaultLength): ServerSideProcessor
    {
        $this->defaultLength = $defaultLength;
        return $this;
    }
}
