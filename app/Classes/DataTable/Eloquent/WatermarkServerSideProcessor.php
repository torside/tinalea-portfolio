<?php

namespace App\Classes\DataTable\Eloquent;

use App\Repositories\Watermark\Watermark;
use Illuminate\Database\Eloquent\Builder;

final class WatermarkServerSideProcessor extends ServerSideProcessor
{
    /** @var array array */
    protected $columns = [
        ['column' => Watermark::PRIMARY_KEY],
        ['column' => Watermark::COLUMN_IMAGE_FILE_NAME],
        ['column' => Watermark::COLUMN_ACTIVE],
        ['column' => Watermark::COLUMN_ORDER],
        ['column' => Watermark::COLUMN_CREATED_AT],
        ['column' => Watermark::COLUMN_UPDATED_AT]
    ];

    /**
     * @return Builder
     */
    protected function createQuery(): Builder
    {
        return $this->model::query();
    }
}
