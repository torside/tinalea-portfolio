<?php

namespace App\Classes\DataTable\Eloquent;

use App\Repositories\WorkCategory\WorkCategory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

final class WorkCategoryServerSideProcessor extends ServerSideProcessor
{
    /** @var array array */
    protected $columns = [
        ['column' => WorkCategory::PRIMARY_KEY],
        ['column' => WorkCategory::COLUMN_WORK_CATEGORY_NAME],
        ['column' => WorkCategory::COLUMN_WORK_CATEGORY_SLUG],
        ['column' => WorkCategory::COLUMN_ACTIVE],
        ['column' => WorkCategory::COLUMN_ORDER]
    ];

    /**
     * @param Builder $builder
     * @param Request $request
     */
    protected function whereConditionsCallback(Builder $builder, Request $request)
    {
        /** @var bool|null $active */
        $active = $request->request
            ->get('active', null);

        if (!is_null($active)) {
            $builder->where(WorkCategory::COLUMN_ACTIVE, $active === true);
        }
    }
}
