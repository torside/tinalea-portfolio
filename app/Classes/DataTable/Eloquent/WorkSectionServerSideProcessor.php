<?php

namespace App\Classes\DataTable\Eloquent;

use App\Repositories\WorkSection\WorkSection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

final class WorkSectionServerSideProcessor extends ServerSideProcessor
{
    /** @var array array */
    protected $columns = [
        ['column' => WorkSection::PRIMARY_KEY],
        ['column' => WorkSection::COLUMN_WORK_SECTION_NAME],
        ['column' => WorkSection::COLUMN_WORK_SECTION_SLUG],
        ['column' => WorkSection::COLUMN_ACTIVE],
        ['column' => WorkSection::COLUMN_ORDER]
    ];

    /**
     * @param Builder $builder
     * @param Request $request
     */
    protected function whereConditionsCallback(Builder $builder, Request $request)
    {
        /** @var bool|null $active */
        $active = $request->request
            ->get('active', null);

        if (!is_null($active)) {
            $builder->where(WorkSection::COLUMN_ACTIVE, $active === true);
        }
    }
}
