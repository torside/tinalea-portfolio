<?php

namespace App\Classes\MigrationService;

use Illuminate\Filesystem\FilesystemAdapter;

abstract class MigrationService
{
    const DATABASE_FOLDER_NAME = 'database';
    const IMAGES_FOLDER_NAME = 'images';
    const MANIFEST_JSON_PATH = 'manifest.json';
    const MODEL_TABLE_NAME_CONSTANT_SUFIX = '::TABLE_NAME';

    /** @var FilesystemAdapter $backupStorage */
    protected $backupStorage;

    /** @var FilesystemAdapter $publicImageStorage */
    protected $publicImageStorage;

    /** @var FilesystemAdapter $privateImageStorage */
    protected $privateImageStorage;

    /** @var array $models */
    protected $models = [];

    /**
     * MigrationService constructor.
     *
     * @param FilesystemAdapter $backupStorage
     * @param FilesystemAdapter $publicImageStorage
     * @param FilesystemAdapter $privateImageStorage
     * @param array $models
     */
    public function __construct(FilesystemAdapter $backupStorage, FilesystemAdapter $publicImageStorage, FilesystemAdapter $privateImageStorage, array $models)
    {
        $this->backupStorage = $backupStorage;
        $this->publicImageStorage = $publicImageStorage;
        $this->privateImageStorage = $privateImageStorage;
        $this->models = array_unique($models);
    }

    /**
     * @return array
     */
    public function getBackupFileList(): array
    {
        return $this->backupStorage
            ->files();
    }

    /**
     * @param string $backupName
     */
    protected function deleteFolder(string $backupName)
    {
        $this->backupStorage
            ->deleteDirectory($backupName);
    }

    /**
     * @param string $model
     *
     * @return mixed|null
     */
    protected function getTableNameFromModelNamespace(string $model)
    {
        if (!defined($model . self::MODEL_TABLE_NAME_CONSTANT_SUFIX)) {
            return null;
        }

        return constant($model . self::MODEL_TABLE_NAME_CONSTANT_SUFIX);
    }
}
