<?php

namespace App\Classes\MigrationService;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use ZipArchive;

final class BackupService extends MigrationService
{
    /**
     * @param string $backupName
     * @param string $backupDesc
     *
     * @return string
     */
    public function backup(string $backupName, string $backupDesc = ''): string
    {
        $backupName = $backupName . '-' . rand(1000, 9999);

        $this->createManifest($backupName, $backupDesc);
        $this->backupDatabase($backupName);
        $this->backupImages($backupName);
        $this->createZip($backupName);
        $this->deleteFolder($backupName);

        return $backupName;
    }

    /**
     * @param string $backupName
     * @param string $backupDesc
     */
    private function createManifest(string $backupName, string $backupDesc = '')
    {
        /** @var string $path */
        $path = sprintf('%s%s%s', $backupName, DIRECTORY_SEPARATOR, self::MANIFEST_JSON_PATH);

        /** @var array $data */
        $data = [
            'datetime' => Carbon::now(),
            'models' => $this->models,
            'backupName' => $backupName,
            'backupDesc' => $backupDesc
        ];

        $this->backupStorage
            ->put($path, json_encode($data, JSON_PRETTY_PRINT));
    }

    /**
     * @param string $backupName
     */
    private function backupDatabase(string $backupName)
    {
        /** @var string $model */
        foreach ($this->models as $model) {
            /** @var string|null $tableName */
            $tableName = $this->getTableNameFromModelNamespace($model);

            if (is_null($tableName)) {
                continue;
            }

            /** @var string $fileName */
            $fileName = sprintf('%s%s%s%s%s.json', $backupName, DIRECTORY_SEPARATOR, self::DATABASE_FOLDER_NAME, DIRECTORY_SEPARATOR, $tableName);

            /** @var Collection $collection */
            $collection = $model::all()
                ->map(function (Model $modelInstance) {
                    return $modelInstance->setVisible([]);
                });

            $this->backupStorage
                ->put($fileName, $collection->toJson(JSON_PRETTY_PRINT));
        }
    }

    /**
     * @param string $backupName
     */
    private function backupImages(string $backupName)
    {
        /** @var string $from */
        $from = $this->privateImageStorage
            ->path('');

        /** @var string $to */
        $to = $this->backupStorage
            ->path(
                sprintf(
                    '%s%s%s',
                    $backupName,
                    DIRECTORY_SEPARATOR,
                    self::IMAGES_FOLDER_NAME
                )
            );

        File::copyDirectory($from, $to);
    }

    /**
     * @param string $backupName
     */
    private function createZip(string $backupName)
    {
        /** @var string $destination */
        $destination = $this->backupStorage
            ->path("{$backupName}.zip");

        /** @var ZipArchive $zipArchive */
        $zipArchive = new ZipArchive();

        /** @var array $files */
        $files = $this->backupStorage
            ->allFiles($backupName);

        /** @var mixed $open */
        $open = $zipArchive->open($destination, ZipArchive::CREATE);

        if ($open) {
            /** @var string $file */
            foreach ($files as $file) {
                /** @var string $path */
                $path = $this->backupStorage
                    ->path($file);

                $zipArchive->addFile($path, $file);
            }

            $zipArchive->close();
        }
    }
}