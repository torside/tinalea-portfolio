<?php

namespace App\Classes\MigrationService;

use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use ZipArchive;

final class RestoreService extends MigrationService
{
    /**
     * @param string $backupName
     *
     * @throws Exception
     *
     * @todo Create custom exception.
     */
    public function restore(string $backupName)
    {
        /** @var bool $open */
        $open = $this->extractZip($backupName);

        if ($open) {
            $this->copyImages($backupName);
            $this->restoreDatabase($backupName);
            $this->deleteFolder($backupName);
        }
    }

    /**
     * @param string $backupName
     *
     * @return bool
     *
     * @throws Exception
     */
    private function extractZip(string $backupName): bool
    {
        /** @var string $fileName */
        $fileName = "{$backupName}.zip";

        /** @var bool $exists */
        $exists = $this->backupStorage
            ->exists($fileName);

        if ($exists === false) {
            throw new Exception("File {$fileName} doesn't exist.");
        }

        /** @var ZipArchive $zipArchive */
        $zipArchive = new ZipArchive();

        /** @var string $source */
        $source = $this->backupStorage
            ->path($fileName);

        /** @var mixed $open */
        $open = $zipArchive->open($source);

        if ($open) {
            /** @var string $path */
            $path = $this->backupStorage
                ->path('');

            $zipArchive->extractTo($path);
            $zipArchive->close();

            $this->cleanPrivateImageStorage();
            $this->cleanPublicImageStorage();
        }

        return $open === true;
    }

    /**
     *
     */
    private function cleanPrivateImageStorage()
    {
        /** @var array $allDirectories */
        $allDirectories = $this->privateImageStorage
            ->allDirectories();

        /** @var string $directory */
        foreach ($allDirectories as $directory) {
            $this->privateImageStorage
                ->deleteDirectory($directory);
        }
    }

    /**
     *
     */
    private function cleanPublicImageStorage()
    {
        /** @var array $allDirectories */
        $allDirectories = $this->publicImageStorage
            ->allDirectories();

        /** @var string $directory */
        foreach ($allDirectories as $directory) {
            $this->publicImageStorage
                ->deleteDirectory($directory);
        }
    }

    /**
     * @param string $backupName
     */
    private function copyImages(string $backupName)
    {
        /** @var string $from */
        $from = $this->backupStorage
            ->path(
                sprintf(
                    '%s%s%s',
                    $backupName,
                    DIRECTORY_SEPARATOR,
                    self::IMAGES_FOLDER_NAME
                )
            );

        /** @var string $to */
        $to = $this->privateImageStorage
            ->path('');

        File::copyDirectory($from, $to);
    }

    /**
     * @param string $backupName
     */
    private function restoreDatabase(string $backupName)
    {
        /** @var string $model */
        foreach ($this->models as $model) {
            /** @var string|null $tableName */
            $tableName = $this->getTableNameFromModelNamespace($model);

            if (is_null($tableName)) {
                continue;
            }

            /** @var string $fileName */
            $fileName = sprintf('%s%s%s%s%s.json', $backupName, DIRECTORY_SEPARATOR, self::DATABASE_FOLDER_NAME, DIRECTORY_SEPARATOR, $tableName);

            if ($this->backupStorage->exists($fileName) === false) {
                continue;
            }

            /** @var array $rows */
            try {
                $rows = json_decode($this->backupStorage->get($fileName), true);
            } catch (FileNotFoundException $e) {
                return;
            }

            if (!is_array($rows)) {
                continue;
            }

            if (is_subclass_of($model, Model::class) === true) {
                $model::query()->truncate();

                /** @var array $row */
                foreach ($rows as $row) {
                    if (!is_array($row)) {
                        continue;
                    }

                    /** @var Model $modelInstance */
                    $modelInstance = new $model;
                    $modelInstance->fillable(array_keys($row))
                        ->fill($row)
                        ->save();
                }
            }
        }
    }
}