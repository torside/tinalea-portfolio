<?php

namespace App\Repositories\WorkCategory;

use Illuminate\Database\Eloquent\Collection;

final class WorkCategoryRepository implements WorkCategoryRepositoryInterface
{
    /**
     * @param int $idWorkCategory
     *
     * @return WorkCategory|null
     */
    public function find(int $idWorkCategory)
    {
        return WorkCategory::find($idWorkCategory);
    }

    /**
     * @param array $attributes
     *
     * @return WorkCategory
     */
    public function firstOrNew(array $attributes): WorkCategory
    {
        return WorkCategory::firstOrNew($attributes);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return WorkCategory::query()
            ->orderBy(WorkCategory::PRIMARY_KEY, 'desc')
            ->get();
    }

    /**
     * @param array $attributes
     *
     * @return WorkCategory
     */
    public function create(array $attributes): WorkCategory
    {
        return WorkCategory::create($attributes);
    }
}
