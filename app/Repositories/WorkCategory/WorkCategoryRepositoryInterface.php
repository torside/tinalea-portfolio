<?php

namespace App\Repositories\WorkCategory;

use Illuminate\Database\Eloquent\Collection;

interface WorkCategoryRepositoryInterface
{
    /**
     * @param int $idWorkCategory
     *
     * @return WorkCategory|null
     */
    public function find(int $idWorkCategory);

    /**
     * @param array $attributes
     *
     * @return WorkCategory
     */
    public function firstOrNew(array $attributes): WorkCategory;

    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @param array $attributes
     *
     * @return WorkCategory
     */
    public function create(array $attributes): WorkCategory;
}
