<?php

namespace App\Repositories\WorkCategory;

use Illuminate\Database\Eloquent\Collection;

final class WorkCategoryService
{
    /** @var WorkCategoryRepositoryInterface $workCategoryRepository */
    private $workCategoryRepository;

    /**
     * WorkCategoryService constructor.
     *
     * @param WorkCategoryRepositoryInterface $workCategoryRepository
     */
    public function __construct(WorkCategoryRepositoryInterface $workCategoryRepository)
    {
        $this->workCategoryRepository = $workCategoryRepository;
    }

    /**
     * @param int $idWorkCategory
     *
     * @return WorkCategory|null
     */
    public function find(int $idWorkCategory)
    {
        return $this->workCategoryRepository
            ->find($idWorkCategory);
    }

    /**
     * @param array $attributes
     *
     * @return WorkCategory
     */
    public function firstOrNew(array $attributes): WorkCategory
    {
        return $this->workCategoryRepository
            ->firstOrNew($attributes);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->workCategoryRepository
            ->all();
    }

    /**
     * @param array $attributes
     *
     * @return WorkCategory
     */
    public function create(array $attributes): WorkCategory
    {
        return $this->workCategoryRepository
            ->create($attributes);
    }
}
