<?php

namespace App\Repositories\WorkCategory;

use App\Repositories\Work\Work;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $idWorkCategory
 * @property string $workCategoryName
 * @property string $workCategorySlug
 * @property bool $active
 * @property int $order
 * @property Collection $work
 */
final class WorkCategory extends Model
{
    const TABLE_NAME = 'work_categories';
    const PRIMARY_KEY = 'idWorkCategory';

    const COLUMN_WORK_CATEGORY_NAME = 'workCategoryName';
    const COLUMN_WORK_CATEGORY_SLUG = 'workCategorySlug';
    const COLUMN_ACTIVE = 'active';
    const COLUMN_ORDER = 'order';

    const RELATION_WORK = 'work';

    /** @var string $table */
    protected $table = self::TABLE_NAME;

    /** @var string $primaryKey */
    protected $primaryKey = self::PRIMARY_KEY;

    /** @var array $visible */
    protected $visible = [
        self::PRIMARY_KEY,
        self::COLUMN_WORK_CATEGORY_NAME,
        self::COLUMN_WORK_CATEGORY_SLUG,
        self::COLUMN_ACTIVE,
        self::COLUMN_ORDER,
        self::RELATION_WORK
    ];

    /** @var array $fillable */
    protected $fillable = [
        self::COLUMN_WORK_CATEGORY_NAME,
        self::COLUMN_WORK_CATEGORY_SLUG,
        self::COLUMN_ACTIVE,
        self::COLUMN_ORDER,
        self::RELATION_WORK
    ];

    /** @var array $casts */
    protected $casts = [
        self::COLUMN_ACTIVE => 'bool'
    ];

    /** @var bool $timestamps */
    public $timestamps = false;

    /**
     * @return HasMany
     */
    public function work(): HasMany
    {
        return $this->hasMany(Work::class, self::PRIMARY_KEY, self::PRIMARY_KEY);
    }
}
