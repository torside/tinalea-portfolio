<?php

namespace App\Repositories\WorkSection;

use Illuminate\Database\Eloquent\Collection;

final class WorkSectionService
{
    /** @var WorkSectionRepositoryInterface $workSectionRepository */
    private $workSectionRepository;

    /**
     * WorkSectionService constructor.
     *
     * @param WorkSectionRepositoryInterface $workSectionRepository
     */
    public function __construct(WorkSectionRepositoryInterface $workSectionRepository)
    {
        $this->workSectionRepository = $workSectionRepository;
    }

    /**
     * @param int $idWorkSection
     *
     * @return WorkSection|null
     */
    public function find(int $idWorkSection)
    {
        return $this->workSectionRepository
            ->find($idWorkSection);
    }

    /**
     * @param array $attributes
     *
     * @return WorkSection
     */
    public function firstOrNew(array $attributes): WorkSection
    {
        return $this->workSectionRepository
            ->firstOrNew($attributes);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->workSectionRepository
            ->all();
    }

    /**
     * @param array $workSectionIds
     *
     * @return Collection
     */
    public function findByWorkSectionIds(array $workSectionIds): Collection
    {
        return $this->workSectionRepository
            ->findByWorkSectionIds($workSectionIds);
    }
}
