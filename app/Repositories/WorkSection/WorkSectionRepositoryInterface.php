<?php

namespace App\Repositories\WorkSection;

use Illuminate\Database\Eloquent\Collection;

interface WorkSectionRepositoryInterface
{
    /**
     * @param int $idWorkSection
     *
     * @return WorkSection|null
     */
    public function find(int $idWorkSection);

    /**
     * @param array $attributes
     *
     * @return WorkSection
     */
    public function firstOrNew(array $attributes): WorkSection;

    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @param array $workSectionIds
     *
     * @return Collection
     */
    public function findByWorkSectionIds(array $workSectionIds): Collection;
}
