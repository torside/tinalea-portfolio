<?php

namespace App\Repositories\WorkSection;

use Illuminate\Database\Eloquent\Collection;

final class WorkSectionRepository implements WorkSectionRepositoryInterface
{
    /**
     * @param int $idWorkSection
     *
     * @return WorkSection|null
     */
    public function find(int $idWorkSection)
    {
        return WorkSection::find($idWorkSection);
    }

    /**
     * @param array $attributes
     *
     * @return WorkSection
     */
    public function firstOrNew(array $attributes): WorkSection
    {
        return WorkSection::firstOrNew($attributes);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return WorkSection::query()
            //->orderBy(WorkSection::COLUMN_ORDER, 'ASC')
            ->get();
    }

    /**
     * @param array $workSectionIds
     *
     * @return Collection
     */
    public function findByWorkSectionIds(array $workSectionIds): Collection
    {
        return WorkSection::query()
            ->whereIn(WorkSection::PRIMARY_KEY, $workSectionIds)
            ->get();
    }
}
