<?php

namespace App\Repositories\WorkSection;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idWorkSection
 * @property string $workSectionName
 * @property string $workSectionSlug
 * @property bool $active
 * @property int $order
 */
final class WorkSection extends Model
{
    const TABLE_NAME = 'work_sections';
    const PRIMARY_KEY = 'idWorkSection';

    const COLUMN_WORK_SECTION_NAME = 'workSectionName';
    const COLUMN_WORK_SECTION_SLUG = 'workSectionSlug';
    const COLUMN_ACTIVE = 'active';
    const COLUMN_ORDER = 'order';

    /** @var string $table */
    protected $table = self::TABLE_NAME;

    /** @var string $primaryKey */
    protected $primaryKey = self::PRIMARY_KEY;

    /** @var array $visible */
    protected $visible = [
        self::PRIMARY_KEY,
        self::COLUMN_WORK_SECTION_NAME,
        self::COLUMN_WORK_SECTION_SLUG,
        self::COLUMN_ACTIVE,
        self::COLUMN_ORDER
    ];

    /** @var array $fillable */
    protected $fillable = [
        self::COLUMN_WORK_SECTION_NAME,
        self::COLUMN_WORK_SECTION_SLUG,
        self::COLUMN_ACTIVE,
        self::COLUMN_ORDER
    ];

    /** @var array $casts */
    protected $casts = [
        self::COLUMN_ACTIVE => 'bool'
    ];

    /** @var bool $timestamps */
    public $timestamps = false;
}
