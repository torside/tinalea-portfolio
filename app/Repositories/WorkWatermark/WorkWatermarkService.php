<?php

namespace App\Repositories\WorkWatermark;

final class WorkWatermarkService
{
    /** @var WorkWatermarkRepositoryInterface $workWatermarkRepository */
    private $workWatermarkRepository;

    /**
     * WorkWatermarkService constructor.
     *
     * @param WorkWatermarkRepositoryInterface $workWatermarkRepository
     */
    public function __construct(WorkWatermarkRepositoryInterface $workWatermarkRepository)
    {
        $this->workWatermarkRepository = $workWatermarkRepository;
    }
}
