<?php

namespace App\Repositories\WorkWatermark;

use App\Repositories\Watermark\Watermark;
use App\Repositories\Work\Work;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $idWorkWatermark
 * @property int $idWork
 * @property int $idWatermark
 * @property string $position
 * @property int offsetX
 * @property int offsetY
 * @property int size
 * @property Watermark $watermark
 */
final class WorkWatermark extends Model
{
    const TABLE_NAME = 'work_watermarks';
    const PRIMARY_KEY = 'idWorkWatermark';

    const COLUMN_POSITION = 'position';
    const COLUMN_OFFSET_X = 'offsetX';
    const COLUMN_OFFSET_Y = 'offsetY';
    const COLUMN_SIZE = 'size';

    const RELATION_WATERMARK = 'watermark';

    const POSITION_TOP_LEFT = 'top-left';
    const POSITION_TOP = 'top';
    const POSITION_TOP_RIGHT = 'top-right';
    const POSITION_LEFT = 'left';
    const POSITION_CENTER = 'center';
    const POSITION_RIGHT = 'right';
    const POSITION_BOTTOM_LEFT = 'bottom-left';
    const POSITION_BOTTOM = 'bottom';
    const POSITION_BOTTOM_RIGHT = 'bottom-right';

    const DEFAULT_POSITION = self::POSITION_TOP_LEFT;

    const ALLOWED_POSITIONS = [
        self::POSITION_TOP_LEFT,
        self::POSITION_TOP,
        self::POSITION_TOP_RIGHT,
        self::POSITION_LEFT,
        self::POSITION_CENTER,
        self::POSITION_RIGHT,
        self::POSITION_BOTTOM_LEFT,
        self::POSITION_BOTTOM,
        self::POSITION_BOTTOM_RIGHT
    ];

    /** @var string $table */
    protected $table = self::TABLE_NAME;

    /** @var string $primaryKey */
    protected $primaryKey = self::PRIMARY_KEY;

    /** @var bool $timestamps */
    public $timestamps = false;

    /** @var array $visible */
    protected $visible = [
        self::PRIMARY_KEY,
        Work::PRIMARY_KEY,
        Watermark::PRIMARY_KEY,
        self::COLUMN_POSITION,
        self::COLUMN_OFFSET_X,
        self::COLUMN_OFFSET_Y,
        self::COLUMN_SIZE,
        self::RELATION_WATERMARK
    ];

    /** @var array $fillable */
    protected $fillable = [
        Work::PRIMARY_KEY,
        Watermark::PRIMARY_KEY,
        self::COLUMN_POSITION,
        self::COLUMN_OFFSET_X,
        self::COLUMN_OFFSET_Y,
        self::COLUMN_SIZE,
        self::RELATION_WATERMARK
    ];

    /**
     * @return HasOne
     */
    public function watermark()
    {
        return $this->hasOne(Watermark::class, Watermark::PRIMARY_KEY, Watermark::PRIMARY_KEY);
    }
}
