<?php

namespace App\Repositories\Watermark;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idWatermark
 * @property string $imageFileName
 * @property bool $active
 * @property int $order
 * @property string $title
 * @property Carbon $createdAt
 * @property Carbon $updatedAt
 */
final class Watermark extends Model
{
    const TABLE_NAME = 'watermarks';
    const PRIMARY_KEY = 'idWatermark';

    const COLUMN_IMAGE_FILE_NAME = 'imageFileName';
    const COLUMN_ACTIVE = 'active';
    const COLUMN_ORDER = 'order';
    const COLUMN_TITLE = 'title';
    const COLUMN_CREATED_AT = 'createdAt';
    const COLUMN_UPDATED_AT = 'updatedAt';

    const CREATED_AT = self::COLUMN_CREATED_AT;
    const UPDATED_AT = self::COLUMN_UPDATED_AT;

    /** @var string $table */
    protected $table = self::TABLE_NAME;

    /** @var string $primaryKey */
    protected $primaryKey = self::PRIMARY_KEY;

    /** @var array $visible */
    protected $visible = [
        self::PRIMARY_KEY,
        self::COLUMN_IMAGE_FILE_NAME,
        self::COLUMN_ACTIVE,
        self::COLUMN_ORDER,
        self::COLUMN_TITLE,
        self::COLUMN_CREATED_AT,
        self::COLUMN_UPDATED_AT
    ];

    /** @var array $fillable */
    protected $fillable = [
        self::COLUMN_IMAGE_FILE_NAME,
        self::COLUMN_ACTIVE,
        self::COLUMN_ORDER,
        self::COLUMN_TITLE,
        self::COLUMN_CREATED_AT,
        self::COLUMN_UPDATED_AT
    ];

    /** @var array $casts */
    protected $casts = [
        self::COLUMN_ACTIVE => 'bool'
    ];
}
