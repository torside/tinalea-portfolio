<?php

namespace App\Repositories\Watermark;

use Illuminate\Database\Eloquent\Collection;

final class WatermarkRepository implements WatermarkRepositoryInterface
{
    /**
     * @param int $idWatermark
     *
     * @return Watermark|null
     */
    public function find(int $idWatermark)
    {
        return Watermark::find($idWatermark);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return Watermark::all();
    }

    /**
     * @param array $attributes
     *
     * @return Watermark
     */
    public function create(array $attributes): Watermark
    {
        return Watermark::create($attributes);
    }
}
