<?php

namespace App\Repositories\Watermark;

use Illuminate\Database\Eloquent\Collection;

final class WatermarkService
{
    /** @var WatermarkRepositoryInterface $watermarkRepository */
    private $watermarkRepository;

    /**
     * WatermarkService constructor.
     *
     * @param WatermarkRepositoryInterface $watermarkRepository
     */
    public function __construct(WatermarkRepositoryInterface $watermarkRepository)
    {
        $this->watermarkRepository = $watermarkRepository;
    }

    /**
     * @param int $idWatermark
     *
     * @return Watermark|null
     */
    public function find(int $idWatermark)
    {
        return $this->watermarkRepository
            ->find($idWatermark);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->watermarkRepository
            ->all();
    }

    /**
     * @param array $attributes
     *
     * @return Watermark
     */
    public function create(array $attributes): Watermark
    {
        return $this->watermarkRepository
            ->create($attributes);
    }
}
