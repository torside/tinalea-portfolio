<?php

namespace App\Repositories\Watermark;

use Illuminate\Database\Eloquent\Collection;

interface WatermarkRepositoryInterface
{
    /**
     * @param int $idWatermark
     *
     * @return Watermark|null
     */
    public function find(int $idWatermark);

    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @param array $attributes
     *
     * @return Watermark
     */
    public function create(array $attributes): Watermark;
}
