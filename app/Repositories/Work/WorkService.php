<?php

namespace App\Repositories\Work;

use App\Repositories\WorkSectionPairing\WorkSectionPairing;
use App\Repositories\WorkWatermark\WorkWatermark;
use Illuminate\Database\Eloquent\Collection;

final class WorkService
{
    /** @var WorkRepositoryInterface $workRepository */
    private $workRepository;

    /**
     * WorkService constructor.
     *
     * @param WorkRepositoryInterface $workRepository
     */
    public function __construct(WorkRepositoryInterface $workRepository)
    {
        $this->workRepository = $workRepository;
    }

    /**
     * @param int $idWork
     *
     * @return Work|null
     */
    public function find(int $idWork)
    {
        return $this->workRepository
            ->find($idWork);
    }

    /**
     * @param array $attributes
     *
     * @return Work
     */
    public function firstOrNew(array $attributes): Work
    {
        return $this->workRepository
            ->firstOrNew($attributes);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->workRepository
            ->all();
    }

    /**
     * @param array $attributes
     *
     * @return Work
     */
    public function create(array $attributes): Work
    {
        return $this->workRepository
            ->create($attributes);
    }

    /**
     * @param Work $work
     */
    public function delete(Work $work)
    {
        WorkSectionPairing::query()
            ->where(Work::PRIMARY_KEY, $work->idWork)
            ->delete();

        WorkWatermark::query()
            ->where(Work::PRIMARY_KEY, $work->idWork)
            ->delete();

        Work::query()
            ->where(Work::PRIMARY_KEY, $work->idWork)
            ->delete();
    }
}
