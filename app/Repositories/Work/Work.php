<?php

namespace App\Repositories\Work;

use App\Repositories\WorkCategory\WorkCategory;
use App\Repositories\WorkSection\WorkSection;
use App\Repositories\WorkSectionPairing\WorkSectionPairing;
use App\Repositories\WorkWatermark\WorkWatermark;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $idWork
 * @property string $imageFileName
 * @property bool $active
 * @property int $order
 * @property string $title
 * @property string $description
 * @property int $idWorkCategory
 * @property Carbon $createdAt
 * @property Carbon $updatedAt
 * @property Collection $workSections
 * @property WorkCategory $workCategory
 * @property WorkWatermark|null $workWatermark
 */
final class Work extends Model
{
    const TABLE_NAME = 'works';
    const PRIMARY_KEY = 'idWork';

    const COLUMN_IMAGE_FILE_NAME = 'imageFileName';
    const COLUMN_ACTIVE = 'active';
    const COLUMN_ORDER = 'order';
    const COLUMN_TITLE = 'title';
    const COLUMN_DESCRIPTION = 'description';
    const COLUMN_ID_WORK_CATEGORY = 'idWorkCategory';
    const COLUMN_CREATED_AT = 'createdAt';
    const COLUMN_UPDATED_AT = 'updatedAt';

    const CREATED_AT = self::COLUMN_CREATED_AT;
    const UPDATED_AT = self::COLUMN_UPDATED_AT;

    const RELATION_WORK_SECTIONS = 'workSections';
    const RELATION_WORK_CATEGORY = 'workCategory';
    const RELATION_WORK_WATERMARK = 'workWatermark';

    /** @var string $table */
    protected $table = self::TABLE_NAME;

    /** @var string $primaryKey */
    protected $primaryKey = self::PRIMARY_KEY;

    /** @var array $visible */
    protected $visible = [
        self::PRIMARY_KEY,
        self::COLUMN_IMAGE_FILE_NAME,
        self::COLUMN_ACTIVE,
        self::COLUMN_ORDER,
        self::COLUMN_TITLE,
        self::COLUMN_DESCRIPTION,
        WorkCategory::PRIMARY_KEY,
        self::RELATION_WORK_SECTIONS,
        self::RELATION_WORK_CATEGORY,
        self::RELATION_WORK_WATERMARK
    ];

    /** @var array $fillable */
    protected $fillable = [
        self::COLUMN_IMAGE_FILE_NAME,
        self::COLUMN_ACTIVE,
        self::COLUMN_ORDER,
        self::COLUMN_TITLE,
        self::COLUMN_DESCRIPTION,
        WorkCategory::PRIMARY_KEY,
        self::RELATION_WORK_SECTIONS,
        self::RELATION_WORK_CATEGORY,
        self::RELATION_WORK_WATERMARK
    ];

    /** @var array $casts */
    protected $casts = [
        self::COLUMN_ACTIVE => 'bool'
    ];

    /**
     * @return HasManyThrough
     */
    public function workSections(): HasManyThrough
    {
        return $this->hasManyThrough(
            WorkSection::class,
            WorkSectionPairing::class,
            self::PRIMARY_KEY,
            WorkSection::PRIMARY_KEY,
            self::PRIMARY_KEY,
            WorkSection::PRIMARY_KEY
        );
    }

    /**
     * @return BelongsTo
     */
    public function workCategory(): BelongsTo
    {
        return $this->belongsTo(WorkCategory::class, WorkCategory::PRIMARY_KEY, WorkCategory::PRIMARY_KEY);
    }

    /**
     * @return HasOne
     */
    public function workWatermark()
    {
        return $this->hasOne(WorkWatermark::class, self::PRIMARY_KEY, self::PRIMARY_KEY);
    }
}
