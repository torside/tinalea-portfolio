<?php

namespace App\Repositories\Work;

use Illuminate\Database\Eloquent\Collection;

interface WorkRepositoryInterface
{
    /**
     * @param int $idWork
     *
     * @return Work|null
     */
    public function find(int $idWork);

    /**
     * @param array $attributes
     *
     * @return Work
     */
    public function firstOrNew(array $attributes): Work;

    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @param array $attributes
     *
     * @return Work
     */
    public function create(array $attributes): Work;
}
