<?php

namespace App\Repositories\Work;

use Illuminate\Database\Eloquent\Collection;

final class WorkRepository implements WorkRepositoryInterface
{
    /**
     * @param int $idWork
     *
     * @return Work|null
     */
    public function find(int $idWork)
    {
        return Work::find($idWork);
    }

    /**
     * @param array $attributes
     *
     * @return Work
     */
    public function firstOrNew(array $attributes): Work
    {
        return Work::firstOrNew($attributes);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return Work::query()
            ->orderBy(Work::PRIMARY_KEY, 'desc')
            ->get();
    }

    /**
     * @param array $attributes
     *
     * @return Work
     */
    public function create(array $attributes): Work
    {
        return Work::create($attributes);
    }
}
