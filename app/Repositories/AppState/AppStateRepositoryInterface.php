<?php

namespace App\Repositories\AppState;

use Carbon\Carbon;

interface AppStateRepositoryInterface
{
    /**
     * return @void
     */
    public function markLastUpdateAt();

    /**
     * @return Carbon|null
     */
    public function getLastUpdateAt();

    /**
     * return @void
     */
    public function markLastBackupAt();

    /**
     * @return Carbon|null
     */
    public function getLastBackupAt();
}