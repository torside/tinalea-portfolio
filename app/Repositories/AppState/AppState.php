<?php

namespace App\Repositories\AppState;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idAppState
 * @property string $appStateKey
 * @property string $appStateValue
 */
final class AppState extends Model
{
    const TABLE_NAME = 'app_states';
    const PRIMARY_KEY = 'idAppState';

    const COLUMN_APP_STATE_KEY = 'appStateKey';
    const COLUMN_APP_STATE_VALUE = 'appStateValue';

    const KEY_LAST_UPDATE_AT = 'lastUpdateAt';
    const VALUE_LAST_UPDATE_AT_DATETIME_FORMAT = 'Y-m-d H:i:s';

    const KEY_LAST_BACKUP_AT = 'lastBackupAt';
    const VALUE_LAST_BACKUP_AT_DATETIME_FORMAT = 'Y-m-d H:i:s';

    /** @var string $table */
    protected $table = self::TABLE_NAME;

    /** @var string $primaryKey */
    protected $primaryKey = self::PRIMARY_KEY;

    /** @var bool $timestamps */
    public $timestamps = false;

    /** @var array $fillable */
    protected $fillable = [
        self::COLUMN_APP_STATE_KEY,
        self::COLUMN_APP_STATE_VALUE
    ];
}
