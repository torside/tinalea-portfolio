<?php

namespace App\Repositories\AppState;

use Carbon\Carbon;

final class AppStateService
{
    /** @var AppStateRepositoryInterface $appStateRepository */
    private $appStateRepository;

    /**
     * AppStateService constructor.
     *
     * @param AppStateRepositoryInterface $appStateRepository
     */
    public function __construct(AppStateRepositoryInterface $appStateRepository)
    {
        $this->appStateRepository = $appStateRepository;
    }

    /**
     * return @void
     */
    public function markLastUpdateAt()
    {
        return $this->appStateRepository
            ->markLastUpdateAt();
    }

    /**
     * @return Carbon|null
     */
    public function getLastUpdateAt()
    {
        return $this->appStateRepository
            ->getLastUpdateAt();
    }

    /**
     * return @void
     */
    public function markLastBackupAt()
    {
        return $this->appStateRepository
            ->markLastBackupAt();
    }

    /**
     * @return Carbon|null
     */
    public function getLastBackupAt()
    {
        return $this->appStateRepository
            ->getLastBackupAt();
    }
}