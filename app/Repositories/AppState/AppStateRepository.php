<?php

namespace App\Repositories\AppState;

use Carbon\Carbon;
use DateTimeZone;
use Throwable;

final class AppStateRepository implements AppStateRepositoryInterface
{
    /**
     * return @void
     */
    public function markLastUpdateAt()
    {
        /** @var AppState $appState */
        $appState = AppState::query()
            ->firstOrNew([
                AppState::COLUMN_APP_STATE_KEY => AppState::KEY_LAST_UPDATE_AT
            ]);

        $appState->appStateValue = Carbon::now(
            new DateTimeZone('Europe/Bratislava')
        )
            ->format(AppState::VALUE_LAST_UPDATE_AT_DATETIME_FORMAT);

        $appState->save();
    }

    /**
     * @return Carbon|null
     */
    public function getLastUpdateAt()
    {
        /** @var AppState|null $appState */
        $appState = AppState::query()
            ->where(AppState::COLUMN_APP_STATE_KEY, AppState::KEY_LAST_UPDATE_AT)
            ->get()
            ->first();

        if (is_null($appState)) {
            return null;
        }

        try {
            return Carbon::createFromFormat(AppState::VALUE_LAST_UPDATE_AT_DATETIME_FORMAT, $appState->appStateValue, new DateTimeZone('Europe/Bratislava'));
        } catch (Throwable $e) {
            return null;
        }
    }

    /**
     * return @void
     */
    public function markLastBackupAt()
    {
        /** @var AppState $appState */
        $appState = AppState::query()
            ->firstOrNew([
                AppState::COLUMN_APP_STATE_KEY => AppState::KEY_LAST_BACKUP_AT
            ]);

        $appState->appStateValue = Carbon::now(
            new DateTimeZone('Europe/Bratislava')
        )
            ->format(AppState::VALUE_LAST_BACKUP_AT_DATETIME_FORMAT);

        $appState->save();
    }

    /**
     * @return Carbon|null
     */
    public function getLastBackupAt()
    {
        /** @var AppState|null $appState */
        $appState = AppState::query()
            ->where(AppState::COLUMN_APP_STATE_KEY, AppState::KEY_LAST_BACKUP_AT)
            ->get()
            ->first();

        if (is_null($appState)) {
            return null;
        }

        try {
            return Carbon::createFromFormat(AppState::VALUE_LAST_BACKUP_AT_DATETIME_FORMAT, $appState->appStateValue, new DateTimeZone('Europe/Bratislava'));
        } catch (Throwable $e) {
            return null;
        }
    }
}