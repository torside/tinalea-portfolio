<?php

namespace App\Repositories\WorkSectionPairing;

interface WorkSectionPairingRepositoryInterface
{
    /**
     * @param array $attributes
     *
     * @return WorkSectionPairing
     */
    public function create(array $attributes): WorkSectionPairing;

    /**
     * @param int $idWork
     */
    public function deleteByIdWork(int $idWork);
}
