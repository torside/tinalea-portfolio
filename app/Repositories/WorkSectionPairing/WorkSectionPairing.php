<?php

namespace App\Repositories\WorkSectionPairing;

use App\Repositories\Work\Work;
use App\Repositories\WorkSection\WorkSection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idWorkSectionPairing
 * @property int $idWork
 * @property int $idWorkSection
 */
final class WorkSectionPairing extends Model
{
    const TABLE_NAME = 'work_section_pairings';
    const PRIMARY_KEY = 'idWorkSectionPairing';

    /** @var string $table */
    protected $table = self::TABLE_NAME;

    /** @var string $primaryKey */
    protected $primaryKey = self::PRIMARY_KEY;

    /** @var array $visible */
    protected $visible = [
        self::PRIMARY_KEY,
        Work::PRIMARY_KEY,
        WorkSection::PRIMARY_KEY
    ];

    /** @var array $fillable */
    protected $fillable = [
        Work::PRIMARY_KEY,
        WorkSection::PRIMARY_KEY
    ];

    /** @var bool $timestamps */
    public $timestamps = false;
}
