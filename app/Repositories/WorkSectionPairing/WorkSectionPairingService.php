<?php

namespace App\Repositories\WorkSectionPairing;

final class WorkSectionPairingService
{
    /** @var WorkSectionPairingRepositoryInterface $workSectionPairingRepository */
    private $workSectionPairingRepository;

    /**
     * WorkSectionPairingService constructor.
     *
     * @param WorkSectionPairingRepositoryInterface $workSectionPairingRepository
     */
    public function __construct(WorkSectionPairingRepositoryInterface $workSectionPairingRepository)
    {
        $this->workSectionPairingRepository = $workSectionPairingRepository;
    }

    /**
     * @param array $attributes
     *
     * @return WorkSectionPairing
     */
    public function create(array $attributes): WorkSectionPairing
    {
        return $this->workSectionPairingRepository
            ->create($attributes);
    }

    /**
     * @param int $idWork
     */
    public function deleteByIdWork(int $idWork)
    {
        $this->workSectionPairingRepository
            ->deleteByIdWork($idWork);
    }
}
