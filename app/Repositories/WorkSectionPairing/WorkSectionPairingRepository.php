<?php

namespace App\Repositories\WorkSectionPairing;

use App\Repositories\Work\Work;

final class WorkSectionPairingRepository implements WorkSectionPairingRepositoryInterface
{
    /**
     * @param array $attributes
     *
     * @return WorkSectionPairing
     */
    public function create(array $attributes): WorkSectionPairing
    {
        return WorkSectionPairing::create($attributes);
    }

    /**
     * @param int $idWork
     */
    public function deleteByIdWork(int $idWork)
    {
        WorkSectionPairing::query()
            ->where(Work::PRIMARY_KEY, $idWork)
            ->delete();
    }
}
