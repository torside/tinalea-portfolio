<?php

namespace App\Providers;

use App\Classes\MigrationService\BackupService;
use App\Classes\ImageService\Implementation\WatermarkImageService;
use App\Classes\ImageService\Implementation\WorkImageService;
use App\Classes\MigrationService\RestoreService;
use App\Repositories\Watermark\WatermarkService;
use App\Repositories\Work\WorkService;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic;

class AppServiceProvider extends ServiceProvider
{
    const IMAGE_SERVICE_PUBLIC = 'image-service-public';
    const IMAGE_SERVICE_PRIVATE = 'image-service-private';
    const BACKUP = 'backup';

    public function register()
    {
        ImageManagerStatic::configure([
            'driver' => config('intervention-image.driver')
        ]);

        $this->app->singleton(self::IMAGE_SERVICE_PUBLIC, function () {
            return Storage::disk(self::IMAGE_SERVICE_PUBLIC);
        });

        $this->app->singleton(self::IMAGE_SERVICE_PRIVATE, function () {
            return Storage::disk(self::IMAGE_SERVICE_PRIVATE);
        });

        $this->app->singleton(self::BACKUP, function () {
            return Storage::disk(self::BACKUP);
        });

        $this->app->singleton(WorkImageService::class, function (Container $container) {
            /** @var string $name */
            $name = 'work';

            /** @var WorkImageService $workImageService */
            $workImageService = new WorkImageService(
                $container->get(self::IMAGE_SERVICE_PUBLIC),
                $container->get(self::IMAGE_SERVICE_PRIVATE),
                $name,
                config("image-service.{$name}")
            );

            return $workImageService->setWorkService($container->get(WorkService::class));
        });

        $this->app->singleton(WatermarkImageService::class, function (Container $container) {
            /** @var string $name */
            $name = 'watermarks';

            /** @var WatermarkImageService $watermarkImageService */
            $watermarkImageService = new WatermarkImageService(
                $container->get(self::IMAGE_SERVICE_PUBLIC),
                $container->get(self::IMAGE_SERVICE_PRIVATE),
                $name,
                config("image-service.{$name}")
            );

            return $watermarkImageService->setWatermarkService($container->get(WatermarkService::class));
        });

        $this->app->singleton(BackupService::class, function (Container $container) {
            return new BackupService(
                $container->get(self::BACKUP),
                $container->get(self::IMAGE_SERVICE_PUBLIC),
                $container->get(self::IMAGE_SERVICE_PRIVATE),
                config('migration.models')
            );
        });

        $this->app->singleton(RestoreService::class, function (Container $container) {
            return new RestoreService(
                $container->get(self::BACKUP),
                $container->get(self::IMAGE_SERVICE_PUBLIC),
                $container->get(self::IMAGE_SERVICE_PRIVATE),
                config('migration.models')
            );
        });
    }
}