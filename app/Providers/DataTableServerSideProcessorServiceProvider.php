<?php

namespace App\Providers;

use App\Classes\DataTable\Eloquent\WatermarkServerSideProcessor;
use App\Classes\DataTable\Eloquent\WorkCategoryServerSideProcessor;
use App\Classes\DataTable\Eloquent\WorkSectionServerSideProcessor;
use App\Classes\DataTable\Eloquent\WorkServerSideProcessor;
use App\Repositories\Watermark\Watermark;
use App\Repositories\Work\Work;
use App\Repositories\WorkCategory\WorkCategory;
use App\Repositories\WorkSection\WorkSection;
use Illuminate\Support\ServiceProvider;

class DataTableServerSideProcessorServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(WatermarkServerSideProcessor::class, function () {
            return new WatermarkServerSideProcessor(Watermark::class);
        });

        $this->app->singleton(WorkServerSideProcessor::class, function () {
            return new WorkServerSideProcessor(Work::class);
        });

        $this->app->singleton(WorkCategoryServerSideProcessor::class, function () {
            return new WorkCategoryServerSideProcessor(WorkCategory::class);
        });

        $this->app->singleton(WorkSectionServerSideProcessor::class, function () {
            return new WorkSectionServerSideProcessor(WorkSection::class);
        });
    }
}
