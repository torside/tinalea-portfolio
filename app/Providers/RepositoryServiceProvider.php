<?php

namespace App\Providers;

use App\Repositories\AppState\AppStateRepository;
use App\Repositories\AppState\AppStateRepositoryInterface;
use App\Repositories\AppState\AppStateService;
use App\Repositories\Watermark\WatermarkRepository;
use App\Repositories\Watermark\WatermarkRepositoryInterface;
use App\Repositories\Watermark\WatermarkService;
use App\Repositories\Work\WorkRepository;
use App\Repositories\Work\WorkRepositoryInterface;
use App\Repositories\Work\WorkService;
use App\Repositories\WorkCategory\WorkCategoryRepository;
use App\Repositories\WorkCategory\WorkCategoryRepositoryInterface;
use App\Repositories\WorkCategory\WorkCategoryService;
use App\Repositories\WorkSection\WorkSectionRepository;
use App\Repositories\WorkSection\WorkSectionRepositoryInterface;
use App\Repositories\WorkSection\WorkSectionService;
use App\Repositories\WorkSectionPairing\WorkSectionPairingRepository;
use App\Repositories\WorkSectionPairing\WorkSectionPairingRepositoryInterface;
use App\Repositories\WorkSectionPairing\WorkSectionPairingService;
use App\Repositories\WorkWatermark\WorkWatermarkRepository;
use App\Repositories\WorkWatermark\WorkWatermarkRepositoryInterface;
use App\Repositories\WorkWatermark\WorkWatermarkService;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(AppStateRepositoryInterface::class, function () {
            return new AppStateRepository();
        });

        $this->app->singleton(AppStateService::class, function (Container $app) {
            return new AppStateService($app->get(AppStateRepositoryInterface::class));
        });

        $this->app->singleton(WorkRepositoryInterface::class, function () {
            return new WorkRepository();
        });

        $this->app->singleton(WorkService::class, function (Container $app) {
            return new WorkService($app->get(WorkRepositoryInterface::class));
        });

        $this->app->singleton(WorkCategoryRepositoryInterface::class, function () {
            return new WorkCategoryRepository();
        });

        $this->app->singleton(WorkCategoryService::class, function (Container $app) {
            return new WorkCategoryService($app->get(WorkCategoryRepositoryInterface::class));
        });

        $this->app->singleton(WorkSectionRepositoryInterface::class, function () {
            return new WorkSectionRepository();
        });

        $this->app->singleton(WorkSectionService::class, function (Container $app) {
            return new WorkSectionService($app->get(WorkSectionRepositoryInterface::class));
        });

        $this->app->singleton(WorkSectionPairingRepositoryInterface::class, function () {
            return new WorkSectionPairingRepository();
        });

        $this->app->singleton(WorkSectionPairingService::class, function (Container $app) {
            return new WorkSectionPairingService($app->get(WorkSectionPairingRepositoryInterface::class));
        });

        $this->app->singleton(WatermarkRepositoryInterface::class, function () {
            return new WatermarkRepository();
        });

        $this->app->singleton(WatermarkService::class, function (Container $app) {
            return new WatermarkService($app->get(WatermarkRepositoryInterface::class));
        });

        $this->app->singleton(WorkWatermarkRepositoryInterface::class, function () {
            return new WorkWatermarkRepository();
        });

        $this->app->singleton(WorkWatermarkService::class, function (Container $app) {
            return new WorkWatermarkService($app->get(WorkWatermarkRepositoryInterface::class));
        });
    }
}
