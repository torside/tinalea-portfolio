<?php

namespace App\Console\Commands;

use App\Repositories\User\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

final class UserCreate extends Command
{
    protected $signature = 'user:create';

    public function handle()
    {
        /** @var string $name */
        $name = $this->askForName();

        /** @var string $email */
        $email = $this->askForEmail();

        /** @var string $password */
        $password = $this->askForPassword();

        DB::table('users')->insert([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
            'created_at' => date_create()->format('Y-m-d H:i:s'),
            'updated_at' => date_create()->format('Y-m-d H:i:s')
        ]);

        $this->info("Account created for '{$name}'.");
    }

    /**
     * @return string
     */
    private function askForName(): string
    {
        /** @var string $name */
        $name = $this->ask('Name');

        if (empty($name)) {
            $this->error('Name is required.');
            return $this->askForName();
        }

        /** @var Collection $nameCheck */
        $nameCheck = User::query()
            ->where('name', $name)
            ->get();

        if ($nameCheck->count() > 0) {
            $this->error("User with name '{$name}' already exists.");
            return $this->askForName();
        }

        return $name;
    }

    /**
     * @return string
     */
    private function askForEmail(): string
    {
        /** @var string $email */
        $email = $this->ask('Email');

        if (empty($email)) {
            $this->error('Email is required.');
            return $this->askForEmail();
        }

        /** @var Collection $emailCheck */
        $emailCheck = User::query()
            ->where('email', $email)
            ->get();

        if ($emailCheck->count() > 0) {
            $this->error("User with email '{$email}' already exists.");
            return $this->askForEmail();
        }

        return $email;
    }

    /**
     * @return string
     */
    private function askForPassword(): string
    {
        /** @var string $password */
        $password = $this->secret('Password');

        if (empty($password)) {
            $this->error('Password is required.');
            return $this->askForPassword();
        }

        /** @var string $confirmPassword */
        $confirmPassword = $this->secret('Confirm password');

        if ($password !== $confirmPassword) {
            $this->error('Passwords don\'t match.');
            return $this->askForPassword();
        }

        return $password;
    }
}
