<?php

namespace App\Console\Commands;

use App\Classes\MigrationService\BackupService;
use App\Repositories\AppState\AppStateService;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

final class Backup extends Command
{
    /** @var string $signature */
    protected $signature = 'backup {--desc=} {--remote} {--force}';

    /** @var string $description */
    protected $description = 'Backup';

    /** @var BackupService $backupService */
    private $backupService;

    /** @var AppStateService $appStateService */
    private $appStateService;

    /**
     * Backup constructor.
     *
     * @param BackupService $backupService
     * @param AppStateService $appStateService
     */
    public function __construct(BackupService $backupService, AppStateService $appStateService)
    {
        $this->backupService = $backupService;
        $this->appStateService = $appStateService;

        parent::__construct();
    }

    /**
     *
     */
    public function handle()
    {
        /** @var string $desc */
        $desc = (string)$this->option('desc');

        /** @var bool $remote */
        $remote = $this->option('remote');

        if ($this->performBackup() === true) {
            if (empty($desc)) {
                $desc = $this->ask('Zadaj popis zálohy', '');
            }

            /** @var string $date */
            $date = Carbon::now(new DateTimeZone('Europe/Bratislava'))
                ->format('Y_m_d-H_i_s');

            /** @var string $name */
            $name = $date . '-' . Str::substr(Str::slug($desc), 0, 30);

            /** @var string $backupFileName */
            $backupFileName = $this->backupService
                ->backup($name, $desc);

            if ($remote === true) {
            }

            $this->appStateService
                ->markLastBackupAt();
        }
    }

    /**
     * @return bool
     */
    private function performBackup(): bool
    {
        /** @var bool $force */
        $force = $this->option('force');

        /** @var Carbon|null $lastBackupAt */
        $lastBackupAt = $this->appStateService
            ->getLastBackupAt();

        if ($force === true || is_null($lastBackupAt)) {
            return true;
        }

        /** @var Carbon|null $lastUpdateAt */
        $lastUpdateAt = $this->appStateService
            ->getLastUpdateAt();

        if (is_null($lastUpdateAt)) {
            return false;
        }

        if ($lastUpdateAt->copy()->addMinutes(3)->lessThanOrEqualTo(Carbon::now(new DateTimeZone('Europe/Bratislava')))) {
            return $lastUpdateAt->greaterThanOrEqualTo($lastBackupAt);
        }

        return false;
    }
}
