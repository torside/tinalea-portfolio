<?php

namespace App\Console\Commands;

use App\Classes\MigrationService\RestoreService;
use App\Events\AppStateUpdatedEvent;
use Exception;
use Illuminate\Console\Command;

final class Restore extends Command
{
    /** @var string $signature */
    protected $signature = 'restore';

    /** @var string $description */
    protected $description = 'Restore';

    /** @var RestoreService $restoreService */
    private $restoreService;

    /**
     * Restore constructor.
     *
     * @param RestoreService $restoreService
     */
    public function __construct(RestoreService $restoreService)
    {
        $this->restoreService = $restoreService;

        parent::__construct();
    }

    /**
     *
     */
    public function handle()
    {
        /** @var int $start */
        $start = time();

        $this->info("Obnova zo zálohy.");

        /** @var array $backupFiles */
        $backupFiles = array_map(
            function (string $backupFile) {
                return basename($backupFile, '.zip');
            }, $this->restoreService
            ->getBackupFileList()
        );


        /** @var string $backupName */
        $backupName = $this->choice('Zvoľ súbor pre obnovu', $backupFiles);

        try {
            $this->info("Obnovujem z '{$backupName}'.");

            $this->restoreService
                ->restore($backupName);
        } catch (Exception $e) {
            $this->error("Chyba pri pokuse o obnovu: '{$e->getMessage()}'");
        }


        $this->info("Generujem formaty pre 'Work' záznamy.");
        $this->call('work:generate-formats', ['--overwrite' => true]);

        $this->info("Generujem formaty pre 'Watermark' záznamy.");
        $this->call('watermark:generate-formats', ['--overwrite' => true]);

        $this->info("Obnova zo zálohy bola dokončená za " . (time() - $start) . " sekúnd.");
        $this->call('fix:rights');

        event(new AppStateUpdatedEvent());
    }
}
