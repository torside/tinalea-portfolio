<?php

namespace App\Console\Commands;

use App\Classes\ImageService\Implementation\WorkImageService;
use App\Repositories\Work\Work;
use App\Repositories\Work\WorkService;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

final class WorkGenerateFormats extends Command
{
    /** @var string $signature */
    protected $signature = 'work:generate-formats
                            {--overwrite : Overwrite existing files.}';

    /** @var string $description */
    protected $description = 'Generates formatted files for work images.';

    /** @var WorkService $workService */
    private $workService;

    /** @var WorkImageService $workImageService */
    private $workImageService;

    /**
     * WorkGenerateFormats constructor.
     *
     * @param WorkService $workService
     * @param WorkImageService $workImageService
     */
    public function __construct(WorkService $workService, WorkImageService $workImageService)
    {
        ini_set('memory_limit','1024M');

        parent::__construct();

        $this->workService = $workService;
        $this->workImageService = $workImageService;
    }

    /**
     *
     */
    public function handle()
    {
        /** @var Collection $works */
        $works = $this->workService
            ->all();

        if ($works->count() === 0) {
            return;
        }

        $bar = $this->output
            ->createProgressBar($works->count());

        $bar->setMaxSteps($works->count());
        $bar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%');
        $bar->start();

        $works->each(function (Work $work) use ($bar) {
            $this->workImageService
                ->storeFormattedImages($work->imageFileName, $this->option('overwrite'));

            $bar->advance();
        });

        $bar->finish();

        $this->line(PHP_EOL);
    }
}
