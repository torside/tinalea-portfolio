<?php

namespace App\Console\Commands;

use App\Repositories\User\User;
use Illuminate\Console\Command;

final class UserChangePassword extends Command
{
    protected $signature = 'user:change-password';

    public function handle()
    {
        $this->table(
            ['Name', 'Email'],
            User::all(['name', 'email'])->toArray()
        );

        /** @var string[] $emails */
        $emails = User::all()
            ->map(function (User $user) {
                return $user->email;
            })
            ->toArray();

        /** @var string $email */
        $email = $this->anticipate('Select user', $emails);

        if (!in_array($email, $emails)) {
            $this->error('Invalid email selected.');
            exit;
        }

        /** @var string $password */
        $password = $this->askForPassword();

        /** @var User $user */
        $user = User::query()
            ->where('email', $email)
            ->get()
            ->first();

        $user->password = bcrypt($password);
        $user->save();


        $this->info("Password changed for '{$email}'.");
    }

    /**
     * @return string
     */
    private function askForPassword(): string
    {
        /** @var string $password */
        $password = $this->secret('Password');

        if (empty($password)) {
            $this->error('Password is required.');
            return $this->askForPassword();
        }

        /** @var string $confirmPassword */
        $confirmPassword = $this->secret('Confirm password');

        if ($password !== $confirmPassword) {
            $this->error('Passwords don\'t match.');
            return $this->askForPassword();
        }

        return $password;
    }
}
