<?php

namespace App\Console\Commands;

use App\Classes\ImageService\Implementation\WatermarkImageService;
use App\Repositories\Watermark\Watermark;
use App\Repositories\Watermark\WatermarkService;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Symfony\Component\Console\Helper\ProgressBar;

final class WatermarkGenerateFormats extends Command
{
    /** @var string $signature */
    protected $signature = 'watermark:generate-formats
                            {--overwrite : Overwrite existing files.}';

    /** @var string $description */
    protected $description = 'Generates formatted files for watermark images.';

    /** @var WatermarkService $watermarkService */
    private $watermarkService;

    /** @var WatermarkImageService $watermarkImageService */
    private $watermarkImageService;

    /**
     * WatermarkGenerateFormats constructor.
     *
     * @param WatermarkService $watermarkService
     * @param WatermarkImageService $watermarkImageService
     */
    public function __construct(WatermarkService $watermarkService, WatermarkImageService $watermarkImageService)
    {
        ini_set('memory_limit','1024M');

        parent::__construct();

        $this->watermarkService = $watermarkService;
        $this->watermarkImageService = $watermarkImageService;
    }

    /**
     *
     */
    public function handle()
    {
        /** @var Collection $watermarks */
        $watermarks = $this->watermarkService
            ->all();

        if ($watermarks->count() === 0) {
            return;
        }

        /** @var ProgressBar $bar */
        $bar = $this->output
            ->createProgressBar($watermarks->count());

        $bar->setMaxSteps($watermarks->count());
        $bar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%');
        $bar->start();

        $watermarks->each(function (Watermark $watermark) use ($bar) {
            $this->watermarkImageService
                ->storeFormattedImages($watermark->imageFileName, $this->option('overwrite'));

            $bar->advance();
        });

        $bar->finish();

        $this->line(PHP_EOL);
    }
}
