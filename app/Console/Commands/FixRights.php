<?php

namespace App\Console\Commands;

use App\Providers\AppServiceProvider;
use DirectoryIterator;
use Illuminate\Console\Command;
use Illuminate\Filesystem\FilesystemAdapter as Filesystem;
use Illuminate\Support\Facades\Storage;
use SplFileInfo;

class FixRights extends Command
{
    /** @var string $signature */
    protected $signature = 'fix:rights';

    /** @var Filesystem $publicStorage */
    private $publicStorage;

    /** @var Filesystem $privateStorage */
    private $privateStorage;

    public function __construct()
    {
        $this->publicStorage = Storage::disk(AppServiceProvider::IMAGE_SERVICE_PUBLIC);
        $this->privateStorage = Storage::disk(AppServiceProvider::IMAGE_SERVICE_PRIVATE);

        parent::__construct();
    }

    /**
     *
     */
    public function handle()
    {
        //$this->rChmod($this->publicStorage->path(''), 0777);
        //$this->rChmod($this->privateStorage->path(''), 0777);
        $this->rChmod(storage_path(''), 0777);
        $this->rChmod(public_path('images' . DIRECTORY_SEPARATOR . 'image-service'), 0777);
    }

    /**
     * @param string $path
     * @param int $rights
     */
    private function rChmod(string $path, int $rights)
    {
        /** @var DirectoryIterator $dir */
        $dir = new DirectoryIterator($path);

        foreach ($dir as $item) {
            try {
                chmod($item->getPathname(), $rights);
            } catch (\Throwable $e) {
                continue;
            }

            if ($item->isDir() && !$item->isDot()) {
                $this->rChmod($item->getPathname(), $rights);
            }
        }
    }
}
