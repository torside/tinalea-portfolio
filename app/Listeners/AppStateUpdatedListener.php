<?php

namespace App\Listeners;

use App\Events\AppStateUpdatedEvent;
use App\Repositories\AppState\AppStateService;

final class AppStateUpdatedListener
{
    /** @var AppStateService $appStateService */
    private $appStateService;

    /**
     * AppStateUpdatedListener constructor.
     *
     * @param AppStateService $appStateService
     */
    public function __construct(AppStateService $appStateService)
    {
        $this->appStateService = $appStateService;
    }

    /**
     * @param AppStateUpdatedEvent $event
     */
    public function handle(AppStateUpdatedEvent $event)
    {
        $this->appStateService
            ->markLastUpdateAt();
    }
}