<?php

namespace App\Jobs;

use App\Classes\ImageService\Implementation\WorkImageService;
use App\Repositories\Work\Work;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class WorkGenerateFormats implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var int $tries */
    public $tries = 10;

    /** @var Work $work */
    private $work;

    /**
     * Create a new job instance.
     *
     * @param Work $work
     */
    public function __construct(Work $work)
    {
        $this->work = $work;
    }

    /**
     * Execute the job.
     *
     * @param WorkImageService $workImageService
     */
    public function handle(WorkImageService $workImageService)
    {
        ini_set('memory_limit','1024M');

        $workImageService
            ->storeFormattedImages($this->work->imageFileName);
    }
}
