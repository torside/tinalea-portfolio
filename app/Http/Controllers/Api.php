<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response;

class Api extends Controller
{
    /**
     * @param int $code
     * @param string $message
     *
     * @return JsonResponse
     */
    protected function emptyJsonResponse(int $code, string $message = ''): JsonResponse
    {
        if (isset(Response::$statusTexts[$code]) === false) {
            return response()
                ->json([
                    'code' => Response::HTTP_BAD_REQUEST,
                    'message' => Response::$statusTexts[Response::HTTP_BAD_REQUEST]
                ], Response::HTTP_BAD_REQUEST);
        }

        if ('' === $message) {
            $message = Response::$statusTexts[$code];
        }

        return response()
            ->json([
                'code' => $code,
                'message' => $message
            ], $code);
    }

    /**
     * @param array $data
     *
     * @return JsonResponse
     */
    protected function jsonResponse(array $data): JsonResponse
    {
        return response()
            ->json([
                'data' => $data
            ]);
    }

    /**
     * @throws UnauthorizedException
     */
    protected function checkAuth()
    {
        if (is_null(Auth::user())) {
            throw new UnauthorizedException();
        }
    }
}
