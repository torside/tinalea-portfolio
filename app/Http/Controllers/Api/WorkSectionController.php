<?php

namespace App\Http\Controllers\Api;

use App\Classes\DataTable\Eloquent\WorkSectionServerSideProcessor;
use App\Http\Controllers\Api;
use App\Repositories\WorkSection\WorkSection;
use App\Repositories\WorkSection\WorkSectionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response;

final class WorkSectionController extends Api
{
    /** @var Request $request */
    private $request;

    /** @var WorkSectionService $workSectionService */
    private $workSectionService;

    /** @var WorkSectionServerSideProcessor $workSectionServerSideProcessor */
    private $workSectionServerSideProcessor;

    /**
     * WorkSectionController constructor.
     *
     * @param Request $request
     * @param WorkSectionService $workSectionService
     * @param WorkSectionServerSideProcessor $workSectionServerSideProcessor
     */
    public function __construct(Request $request, WorkSectionService $workSectionService, WorkSectionServerSideProcessor $workSectionServerSideProcessor)
    {
        $this->request = $request;
        $this->workSectionService = $workSectionService;
        $this->workSectionServerSideProcessor = $workSectionServerSideProcessor;
    }

    /**
     * @return JsonResponse
     */
    public function create(): JsonResponse
    {
        try {
            $this->checkAuth();
        } catch (UnauthorizedException $e) {
            return $this->emptyJsonResponse(Response::HTTP_UNAUTHORIZED);
        }

        /** @var string $workSectionName */
        $workSectionName = (string)$this->request
            ->get('workSectionName', '');

        if ('' === $workSectionName) {
            return $this->emptyJsonResponse(Response::HTTP_BAD_REQUEST);
        }

        /** @var string $workSectionNameSlug */
        $workSectionNameSlug = Str::slug($workSectionName);

        /** @var WorkSection $workSection */
        $workSection = $this->workSectionService
            ->firstOrNew([
                WorkSection::COLUMN_WORK_SECTION_SLUG => $workSectionNameSlug
            ]);

        if ($workSection->exists === true) {
            return $this->emptyJsonResponse(Response::HTTP_CONFLICT);
        }

        $workSection->workSectionName = $workSectionName;
        $workSection->active = true;
        $workSection->save();

        return $this->emptyJsonResponse(Response::HTTP_OK);
    }

    /**
     * @return JsonResponse
     */
    public function update(): JsonResponse
    {
        try {
            $this->checkAuth();
        } catch (UnauthorizedException $e) {
            return $this->emptyJsonResponse(Response::HTTP_UNAUTHORIZED);
        }

        /** @var int $idWorkSection */
        $idWorkSection = (int)$this->request
            ->get('idWorkSection', 0);

        /** @var WorkSection|null $workSection */
        $workSection = $this->workSectionService
            ->find($idWorkSection);

        if (is_null($workSection)) {
            return $this->emptyJsonResponse(Response::HTTP_NOT_FOUND);
        }

        /** @var bool|null $active */
        $active = $this->request
            ->get('active', null);

        if (is_null($active) === false) {
            $workSection->active = $active === true;
        }

        /** @var int|null $order */
        $order = $this->request
            ->get('order', null);

        if (is_null($order) === false) {
            $workSection->order = (int)$order;
        }

        /** @var string|null $workSectionName */
        $workSectionName = $this->request
            ->get('workSectionName', null);

        if (is_null($workSectionName) === false) {
            /**
             * @todo - Check for duplicity in slug
             */

            $workSection->workSectionName = $workSectionName;
            $workSection->workSectionSlug = Str::slug($workSectionName);
        }

        $workSection->save();

        return $this->emptyJsonResponse(Response::HTTP_OK);
    }

    /**
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        return $this->jsonResponse(
            $this->workSectionServerSideProcessor
                ->search($this->request)
        );
    }
}
