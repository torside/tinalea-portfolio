<?php

namespace App\Http\Controllers\Api;

use App\Classes\DataTable\Eloquent\WatermarkServerSideProcessor;
use App\Classes\ImageService\Implementation\WatermarkImageService;
use App\Http\Controllers\Api;
use App\Repositories\Watermark\Watermark;
use App\Repositories\Watermark\WatermarkService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response;

final class WatermarkController extends Api
{
    /** @var Request $request */
    private $request;

    /** @var WatermarkService $watermarkService */
    private $watermarkService;

    /** @var WatermarkImageService $watermarkImageService */
    private $watermarkImageService;

    /** @var WatermarkServerSideProcessor $watermarkServerSideProcessor */
    private $watermarkServerSideProcessor;

    /**
     * WatermarkController constructor.
     *
     * @param Request $request
     * @param WatermarkService $watermarkService
     * @param WatermarkImageService $watermarkImageService
     * @param WatermarkServerSideProcessor $watermarkServerSideProcessor
     */
    public function __construct(
        Request $request,
        WatermarkService $watermarkService,
        WatermarkImageService $watermarkImageService,
        WatermarkServerSideProcessor $watermarkServerSideProcessor
    )
    {
        $this->request = $request;
        $this->watermarkService = $watermarkService;
        $this->watermarkImageService = $watermarkImageService;
        $this->watermarkServerSideProcessor = $watermarkServerSideProcessor;
    }

    /**
     * @return JsonResponse
     */
    public function update(): JsonResponse
    {
        try {
            $this->checkAuth();
        } catch (UnauthorizedException $e) {
            return $this->emptyJsonResponse(Response::HTTP_UNAUTHORIZED);
        }

        /** @var int $idWatermark */
        $idWatermark = (int)$this->request
            ->get('idWatermark', 0);

        /** @var Watermark|null $watermark */
        $watermark = $this->watermarkService
            ->find($idWatermark);

        if (is_null($watermark)) {
            return $this->emptyJsonResponse(Response::HTTP_NOT_FOUND);
        }

        $watermark->title = (string)$this->request
            ->get('title', '');

        $watermark->save();

        return $this->emptyJsonResponse(Response::HTTP_OK);
    }

    /**
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        return $this->jsonResponse(
            $this->watermarkServerSideProcessor
                ->search($this->request)
        );
    }

    /**
     * @return JsonResponse
     */
    public function uploadImage(): JsonResponse
    {
        try {
            $this->checkAuth();
        } catch (UnauthorizedException $e) {
            return $this->emptyJsonResponse(Response::HTTP_UNAUTHORIZED);
        }

        /** @var array $images */
        $images = $this->request
            ->file('images');

        /** @var UploadedFile $image */
        foreach ($images as $image) {
            $this->watermarkImageService
                ->storeOriginalImage($image, Str::random(8));
        }

        return $this->emptyJsonResponse(Response::HTTP_OK);
    }
}
