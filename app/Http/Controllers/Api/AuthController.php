<?php

namespace App\Http\Controllers\Api;

use App\Classes\DataTable\Eloquent\WorkCategoryServerSideProcessor;
use App\Http\Controllers\Api;
use App\Repositories\User\User;
use App\Repositories\WorkCategory\WorkCategory;
use App\Repositories\WorkCategory\WorkCategoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response;

final class AuthController extends Api
{
    /** @var Request $request */
    private $request;

    /**
     * AuthController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return JsonResponse
     */
    public function getUser(): JsonResponse
    {
        /** @var User $user */
        $user = Auth::user();

        return $this->jsonResponse([
            'user' => is_null($user) ? null : $user->toArray()
        ]);
    }
}
