<?php

namespace App\Http\Controllers\Api;

use App\Classes\DataTable\Eloquent\WorkCategoryServerSideProcessor;
use App\Http\Controllers\Api;
use App\Repositories\WorkCategory\WorkCategory;
use App\Repositories\WorkCategory\WorkCategoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response;

final class WorkCategoryController extends Api
{
    /** @var Request $request */
    private $request;

    /** @var WorkCategoryService $workCategoryService */
    private $workCategoryService;

    /** @var WorkCategoryServerSideProcessor $workCategoryServerSideProcessor */
    private $workCategoryServerSideProcessor;

    /**
     * WorkCategoryController constructor.
     *
     * @param Request $request
     * @param WorkCategoryService $workCategoryService
     * @param WorkCategoryServerSideProcessor $workCategoryServerSideProcessor
     */
    public function __construct(Request $request, WorkCategoryService $workCategoryService, WorkCategoryServerSideProcessor $workCategoryServerSideProcessor)
    {
        $this->request = $request;
        $this->workCategoryService = $workCategoryService;
        $this->workCategoryServerSideProcessor = $workCategoryServerSideProcessor;
    }

    /**
     * @return JsonResponse
     */
    public function create(): JsonResponse
    {
        try {
            $this->checkAuth();
        } catch (UnauthorizedException $e) {
            return $this->emptyJsonResponse(Response::HTTP_UNAUTHORIZED);
        }

        /** @var string $workCategoryName */
        $workCategoryName = (string)$this->request
            ->get('workCategoryName', '');

        if ('' === $workCategoryName) {
            return $this->emptyJsonResponse(Response::HTTP_BAD_REQUEST);
        }

        /** @var string $workCategoryNameSlug */
        $workCategoryNameSlug = Str::slug($workCategoryName);

        /** @var WorkCategory $workCategory */
        $workCategory = $this->workCategoryService
            ->firstOrNew([
                WorkCategory::COLUMN_WORK_CATEGORY_SLUG => $workCategoryNameSlug
            ]);

        if ($workCategory->exists === true) {
            return $this->emptyJsonResponse(Response::HTTP_CONFLICT);
        }

        $workCategory->workCategoryName = $workCategoryName;
        $workCategory->active = true;
        $workCategory->save();

        return $this->emptyJsonResponse(Response::HTTP_OK);
    }

    /**
     * @return JsonResponse
     */
    public function update(): JsonResponse
    {
        try {
            $this->checkAuth();
        } catch (UnauthorizedException $e) {
            return $this->emptyJsonResponse(Response::HTTP_UNAUTHORIZED);
        }

        /** @var int $idWorkCategory */
        $idWorkCategory = (int)$this->request
            ->get('idWorkCategory', 0);

        /** @var WorkCategory|null $workCategory */
        $workCategory = $this->workCategoryService
            ->find($idWorkCategory);

        if (is_null($workCategory)) {
            return $this->emptyJsonResponse(Response::HTTP_NOT_FOUND);
        }

        /** @var bool|null $active */
        $active = $this->request
            ->get('active', null);

        if (is_null($active) === false) {
            $workCategory->active = $active === true;
        }

        /** @var int|null $order */
        $order = $this->request
            ->get('order', null);

        if (is_null($order) === false) {
            $workCategory->order = (int)$order;
        }

        /** @var string|null $workCategoryName */
        $workCategoryName = $this->request
            ->get('workCategoryName', null);

        if (is_null($workCategoryName) === false) {
            /**
             * @todo - Check for duplicity in slug
             */

            $workCategory->workCategoryName = $workCategoryName;
            $workCategory->workCategorySlug = Str::slug($workCategoryName);
        }

        $workCategory->save();

        return $this->emptyJsonResponse(Response::HTTP_OK);
    }

    /**
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        return $this->jsonResponse(
            $this->workCategoryServerSideProcessor
                ->search($this->request)
        );
    }
}
