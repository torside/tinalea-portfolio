<?php

namespace App\Http\Controllers\Api;

use App\Classes\DataTable\Eloquent\WorkServerSideProcessor;
use App\Classes\ImageService\Implementation\WorkImageService;
use App\Http\Controllers\Api;
use App\Jobs\WorkGenerateFormats;
use App\Repositories\Watermark\Watermark;
use App\Repositories\Work\Work;
use App\Repositories\Work\WorkService;
use App\Repositories\WorkCategory\WorkCategory;
use App\Repositories\WorkSection\WorkSection;
use App\Repositories\WorkSection\WorkSectionService;
use App\Repositories\WorkSectionPairing\WorkSectionPairingService;
use App\Repositories\WorkWatermark\WorkWatermark;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response;

final class WorkController extends Api
{
    /** @var Request $request */
    private $request;

    /** @var WorkService $workService */
    private $workService;

    /** @var WorkImageService $workImageService */
    private $workImageService;

    /** @var WorkServerSideProcessor $workServerSideProcessor */
    private $workServerSideProcessor;

    /** @var WorkSectionService $workSectionService */
    private $workSectionService;

    /** @var WorkSectionPairingService $workSectionPairingService */
    private $workSectionPairingService;

    /**
     * WorkController constructor.
     *
     * @param Request $request
     * @param WorkService $workService
     * @param WorkImageService $workImageService
     * @param WorkServerSideProcessor $workServerSideProcessor
     * @param WorkSectionService $workSectionService
     * @param WorkSectionPairingService $workSectionPairingService
     */
    public function __construct(
        Request $request,
        WorkService $workService,
        WorkImageService $workImageService,
        WorkServerSideProcessor $workServerSideProcessor,
        WorkSectionService $workSectionService,
        WorkSectionPairingService $workSectionPairingService
    )
    {
        $this->request = $request;
        $this->workService = $workService;
        $this->workImageService = $workImageService;
        $this->workServerSideProcessor = $workServerSideProcessor;
        $this->workSectionService = $workSectionService;
        $this->workSectionPairingService = $workSectionPairingService;
    }

    /**
     * @param int $idWork
     *
     * @return JsonResponse
     */
    public function index(int $idWork)
    {
        /** @var Work|null $work */
        $work = $this->workService
            ->find($idWork);

        if (is_null($work)) {
            return $this->emptyJsonResponse(Response::HTTP_NOT_FOUND);
        }

        $work->load([
            Work::RELATION_WORK_SECTIONS,
            Work::RELATION_WORK_WATERMARK
        ]);

        return $this->jsonResponse($work->toArray());
    }

    /**
     * @return JsonResponse
     */
    public function create(): JsonResponse
    {
        try {
            $this->checkAuth();
        } catch (UnauthorizedException $e) {
            return $this->emptyJsonResponse(Response::HTTP_UNAUTHORIZED);
        }

        return $this->emptyJsonResponse(Response::HTTP_OK);
    }

    /**
     * @return JsonResponse
     */
    public function update(): JsonResponse
    {
        try {
            $this->checkAuth();
        } catch (UnauthorizedException $e) {
            return $this->emptyJsonResponse(Response::HTTP_UNAUTHORIZED);
        }

        /** @var int $idWork */
        $idWork = (int)$this->request
            ->get(Work::PRIMARY_KEY, 0);

        /** @var Work|null $work */
        $work = $this->workService
            ->find($idWork);

        if (is_null($work)) {
            return $this->emptyJsonResponse(Response::HTTP_NOT_FOUND);
        }

        /** @var array $workSectionIds */
        $workSectionIds = $this->request
            ->get('workSectionIds', null);

        if (is_array($workSectionIds)) {
            $this->workSectionPairingService
                ->deleteByIdWork($work->idWork);

            /** @var int $idWorkSection */
            foreach ($workSectionIds as $idWorkSection) {
                /** @var WorkSection|null $workSection */
                $workSection = $this->workSectionService
                    ->find($idWorkSection);

                if (!is_null($workSection)) {
                    $this->workSectionPairingService
                        ->create([
                            Work::PRIMARY_KEY => $work->idWork,
                            WorkSection::PRIMARY_KEY => $workSection->idWorkSection
                        ]);
                }
            }
        }

        /** @var int $idWorkCategory */
        $idWorkCategory = $this->request
            ->get(WorkCategory::PRIMARY_KEY, null);

        if (!is_null($idWorkCategory)) {
            $work->idWorkCategory = $idWorkCategory;
        }

        $work->order = (int)$this->request
            ->get(Work::COLUMN_ORDER, 0);

        $work->title = (string)$this->request
            ->get(Work::COLUMN_TITLE, '');

        $work->description = (string)$this->request
            ->get(Work::COLUMN_DESCRIPTION, '');

        if ($this->request->has(Watermark::PRIMARY_KEY)) {
            /** @var int|null $idWatermark */
            $idWatermark = $this->request
                ->get(Watermark::PRIMARY_KEY, null);

            /** @var WorkWatermark|null $workWatermark */
            $workWatermark = $work->workWatermark;

            if (is_null($idWatermark) && !is_null($workWatermark)) {
                $workWatermark->delete();
                WorkGenerateFormats::dispatch($work);
            }

            if (!is_null($idWatermark)) {
                /** @var string $watermarkPosition */
                $watermarkPosition = $this->request
                    ->get('watermarkPosition', WorkWatermark::DEFAULT_POSITION);

                if (!in_array($watermarkPosition, WorkWatermark::ALLOWED_POSITIONS)) {
                    $watermarkPosition = WorkWatermark::DEFAULT_POSITION;
                }

                /** @var int $watermarkOffsetX */
                $watermarkOffsetX = (int)$this->request
                    ->get('watermarkOffsetX', 0);

                /** @var int $watermarkOffsetY */
                $watermarkOffsetY = (int)$this->request
                    ->get('watermarkOffsetY', 0);

                /** @var int $watermarkSize */
                $watermarkSize = (int)$this->request
                    ->get('watermarkSize', 100);

                if (is_null($workWatermark)) {
                    $workWatermark = new WorkWatermark();
                }

                $workWatermark->idWork = $idWork;
                $workWatermark->idWatermark = $idWatermark;
                $workWatermark->position = $watermarkPosition;
                $workWatermark->offsetX = $watermarkOffsetX;
                $workWatermark->offsetY = $watermarkOffsetY;
                $workWatermark->size = $watermarkSize;


                $workWatermark->save();

                if ($workWatermark->wasRecentlyCreated === true || count($workWatermark->getChanges()) > 0) {
                    WorkGenerateFormats::dispatch($work);
                }
            }
        }

        $work->save();

        return $this->emptyJsonResponse(Response::HTTP_OK);
    }

    /**
     * @return JsonResponse
     */
    public function delete(): JsonResponse
    {
        try {
            $this->checkAuth();
        } catch (UnauthorizedException $e) {
            return $this->emptyJsonResponse(Response::HTTP_UNAUTHORIZED);
        }

        /** @var int $idWork */
        $idWork = (int)$this->request
            ->get(Work::PRIMARY_KEY, 0);

        /** @var Work|null $work */
        $work = $this->workService
            ->find($idWork);

        if (is_null($work)) {
            return $this->emptyJsonResponse(Response::HTTP_NOT_FOUND);
        }

        /** @var string $imageFileName */
        $imageFileName = $work->imageFileName;

        $this->workService
            ->delete($work);

        $this->workImageService
            ->deleteOriginalImage($imageFileName);

        return $this->emptyJsonResponse(Response::HTTP_OK);
    }

    /**
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        return $this->jsonResponse(
            $this->workServerSideProcessor
                ->search($this->request)
        );
    }

    /**
     * @return JsonResponse
     */
    public function portfolio(): JsonResponse
    {
        /** @var Collection $works */
        $works = Work::query()
            ->with([
                Work::RELATION_WORK_CATEGORY,
                Work::RELATION_WORK_SECTIONS
            ])
            ->where(Work::COLUMN_ACTIVE, true)
            ->orderBy(Work::COLUMN_ORDER, 'ASC')
            ->get();

        /** @var array $results */
        $results = [];

        /** @var Work $work */
        foreach ($works as $work) {
            if (is_null($work->workCategory)) {
                $results['default']['default'][] = $work->toArray();

                continue;
            }

            if ($work->workSections->count() > 0) {
                /** @var WorkSection $workSection */
                foreach ($work->workSections as $workSection) {
                    $results[$work->workCategory->workCategorySlug][$workSection->workSectionSlug][] = $work->toArray();
                }

                continue;
            }

            $results[$work->workCategory->workCategorySlug]['default'][] = $work->toArray();
        }

        return $this->jsonResponse($results);
    }

    /**
     * @return JsonResponse
     */
    public function uploadImage(): JsonResponse
    {
        try {
            $this->checkAuth();
        } catch (UnauthorizedException $e) {
            return $this->emptyJsonResponse(Response::HTTP_UNAUTHORIZED);
        }

        /** @var array $images */
        $images = $this->request
            ->file('images', []);

        /** @var UploadedFile $image */
        foreach ($images as $image) {
            $this->workImageService
                ->storeOriginalImage($image, Str::random(8));
        }

        return $this->emptyJsonResponse(Response::HTTP_OK);
    }

    /**
     * @return JsonResponse
     */
    public function generateFormats(): JsonResponse
    {
        try {
            $this->checkAuth();
        } catch (UnauthorizedException $e) {
            return $this->emptyJsonResponse(Response::HTTP_UNAUTHORIZED);
        }

        /** @var int $idWork */
        $idWork = (int)$this->request
            ->get(Work::PRIMARY_KEY, 0);

        /** @var Work|null $work */
        $work = $this->workService
            ->find($idWork);

        if (is_null($work)) {
            return $this->emptyJsonResponse(Response::HTTP_NOT_FOUND);
        }

        WorkGenerateFormats::dispatch($work);

        return $this->emptyJsonResponse(Response::HTTP_OK);
    }
}
