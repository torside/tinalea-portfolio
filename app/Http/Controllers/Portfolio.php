<?php

namespace App\Http\Controllers;

use App\Repositories\Work\Work;
use App\Repositories\WorkCategory\WorkCategory;
use App\Repositories\WorkSection\WorkSection;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Illuminate\View\View;

final class Portfolio extends Controller
{
    const DEFAULT_OG_IMAGE = 'images/portfolio/about/avatar.jpg';
    const OG_IMAGE_PATH_PREFIX = 'images/image-service/work/md/';
    const DEFAULT_OG_DESCRIPTION = 'My personal portfolio.';

    /**
     * @param string $pageSlug
     * @param string|null $categorySlug
     * @param string|null $sectionSlug
     * @param int|null $idWork
     *
     * @return Application|RedirectResponse|Redirector|View
     */
    public function __invoke(string $pageSlug, ?string $categorySlug = null, ?string $sectionSlug = null, ?int $idWork = null)
    {
        /** @var string[] $titleParts */
        $titleParts = ['Tinalea Portfolio', Str::ucfirst($pageSlug)];

        /** @var string $ogImage */
        $ogImage = config('app.url', '') . self::DEFAULT_OG_IMAGE;

        /** @var string $ogDescription */
        $ogDescription = self::DEFAULT_OG_DESCRIPTION;

        if ($categorySlug) {
            /** @var WorkCategory $category */
            $category = $this->getCategory($categorySlug);
            $titleParts[] = $category->workCategoryName;

            if ($sectionSlug && $sectionSlug !== 'default') {
                /** @var WorkSection $section */
                $section = $this->getSection($sectionSlug);
                $titleParts[] = $section->workSectionName;
            }

            if ($idWork) {
                /** @var Work $work */
                $work = $this->getWork($idWork);
                $ogImage = config('app.url', '') . self::OG_IMAGE_PATH_PREFIX . $work->imageFileName;
            }
        }

        /** @var string $ogTitle */
        $ogTitle = implode(' | ', array_filter($titleParts));

        if (isset($work)) {
            if ($work->title) {
                $ogTitle = $work->title;
            }

            if ($work->description) {
                $ogDescription = $work->description;
            }
        }

        return view('portfolio', [
            'title' => $ogTitle,
            'ogImage' => $ogImage,
            'ogDescription' => $ogDescription,
        ]);
    }

    /**
     * @param string $categorySlug
     *
     * @return WorkCategory
     */
    private function getCategory(string $categorySlug): WorkCategory
    {
        /** @var WorkCategory|null $category */
        $category = WorkCategory::query()
            ->where(WorkCategory::COLUMN_WORK_CATEGORY_SLUG, $categorySlug)
            ->where(WorkCategory::COLUMN_ACTIVE, true)
            ->get()
            ->first();

        if (is_null($category)) {
            $this->abort();
        }

        return $category;
    }

    /**
     * @param string $sectionSlug
     *
     * @return WorkSection
     */
    private function getSection(string $sectionSlug): WorkSection
    {
        /** @var WorkSection|null $section */
        $section = WorkSection::query()
            ->where(WorkSection::COLUMN_WORK_SECTION_SLUG, $sectionSlug)
            ->where(WorkSection::COLUMN_ACTIVE, true)
            ->get()
            ->first();

        if (is_null($section)) {
            $this->abort();
        }

        return $section;
    }

    /**
     * @param int $idWork
     *
     * @return Work
     */
    private function getWork(int $idWork): Work
    {
        /** @var Work|null $work */
        $work = Work::query()
            ->where(Work::PRIMARY_KEY, $idWork)
            ->where(Work::COLUMN_ACTIVE, true)
            ->get()
            ->first();

        if (is_null($work)) {
            $this->abort();
        }

        return $work;
    }

    /**
     *
     */
    private function abort()
    {
        App::abort(302, '', ['Location' => route('default')]);
    }
}
