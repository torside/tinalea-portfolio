<?php

namespace App\Http\Controllers\Auth\Images;

use App\Classes\ImageService\Implementation\WatermarkImageService;
use App\Classes\ImageService\Implementation\WorkImageService;
use App\Http\Controllers\Controller;
use App\Repositories\Watermark\Watermark;
use App\Repositories\Watermark\WatermarkService;
use App\Repositories\Work\Work;
use App\Repositories\Work\WorkService;
use Illuminate\Http\Request;
use Intervention\Image\Constraint;
use Intervention\Image\Image as InterventionImage;
use Intervention\Image\ImageManagerStatic;

final class WorkPreviewController extends Controller
{
    const ALLOWED_WATERMARK_POSITIONS = [
        'top-left',
        'top',
        'top-right',
        'left',
        'center',
        'right',
        'bottom-left',
        'bottom',
        'bottom-right',
    ];

    /** @var Request $request */
    private $request;

    /** @var WorkService $workService */
    private $workService;

    /** @var WorkImageService $workImageService */
    private $workImageService;

    /** @var WatermarkService $watermarkService */
    private $watermarkService;

    /** @var WatermarkImageService $watermarkImageService */
    private $watermarkImageService;

    public function __construct(
        Request $request,
        WorkService $workService,
        WorkImageService $workImageService,
        WatermarkService $watermarkService,
        WatermarkImageService $watermarkImageService
    )
    {
        $this->request = $request;
        $this->workService = $workService;
        $this->workImageService = $workImageService;
        $this->watermarkService = $watermarkService;
        $this->watermarkImageService = $watermarkImageService;
    }

    public function index(int $idWork)
    {
        /** @var Work|null $work */
        $work = $this->workService
            ->find($idWork);

        if (is_null($work)) {
            // 404
        }

        /** @var int $idWatermark */
        $idWatermark = (int)$this->request
            ->get('idWatermark', 0);

        /** @var string $watermarkPosition */
        $watermarkPosition = $this->request
            ->get('watermarkPosition', '');

        /** @var int $watermarkOffsetX */
        $watermarkOffsetX = (int)$this->request
            ->get('watermarkOffsetX', 0);

        /** @var int $watermarkOffsetY */
        $watermarkOffsetY = (int)$this->request
            ->get('watermarkOffsetY', 0);

        /** @var int $watermarkSize */
        $watermarkSize = (int)$this->request
            ->get('watermarkSize', 100);

        /** @var Watermark|null $watermark */
        $watermark = $this->watermarkService
            ->find($idWatermark);

        /** @var InterventionImage $outputImage */
        $outputImage = ImageManagerStatic::make($this->workImageService->getOriginalImageFilePath($work->imageFileName));
        $outputImage->orientate();

        if (!is_null($watermark)) {
            /** @var InterventionImage $watermarkImage */
            $watermarkImage = ImageManagerStatic::make($this->watermarkImageService->getOriginalImageFilePath($watermark->imageFileName));

            if ($watermarkSize < 100 && $watermarkSize > 0) {
                $watermarkImage->resize((($watermarkImage->getWidth() / 100) * $watermarkSize), null, function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }

            $outputImage->insert(
                $watermarkImage,
                in_array($watermarkPosition, self::ALLOWED_WATERMARK_POSITIONS)
                    ? $watermarkPosition
                    : self::ALLOWED_WATERMARK_POSITIONS[0],
                $watermarkOffsetX,
                $watermarkOffsetY
            );
        }

        $outputImage->resize(800, null, function (Constraint $constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        return $outputImage->response();
    }
}
