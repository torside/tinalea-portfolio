<?php

namespace App\Http\Middleware;

use App\Events\AppStateUpdatedEvent;
use Closure;
use Illuminate\Http\Request;

final class AppStateUpdate
{
    /**
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        event(new AppStateUpdatedEvent());

        return $next($request);
    }
}