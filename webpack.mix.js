const mix = require('laravel-mix');

mix.copy('resources/images/portfolio', 'public/images/portfolio');

mix.react('resources/js/auth/index.jsx', 'public/js/auth')
    .sass('resources/sass/auth/style.scss', 'public/css/auth');

mix.react('resources/js/portfolio/index.jsx', 'public/js/portfolio')
    .sass('resources/sass/portfolio/style.scss', 'public/css/portfolio');

mix.version();
