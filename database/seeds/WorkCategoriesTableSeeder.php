<?php

use Illuminate\Database\Seeder;

class WorkCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('work_categories')->delete();
        
        \DB::table('work_categories')->insert(array (
            0 => 
            array (
                'idWorkCategory' => 1,
                'workCategoryName' => 'Photography',
                'workCategorySlug' => 'photography',
                'active' => 1,
                'order' => 1,
            ),
            1 => 
            array (
                'idWorkCategory' => 2,
                'workCategoryName' => 'Graphic Design',
                'workCategorySlug' => 'graphic-design',
                'active' => 1,
                'order' => 2,
            ),
            2 => 
            array (
                'idWorkCategory' => 3,
                'workCategoryName' => 'Art',
                'workCategorySlug' => 'art',
                'active' => 1,
                'order' => 3,
            ),
            3 => 
            array (
                'idWorkCategory' => 4,
                'workCategoryName' => 'Project',
                'workCategorySlug' => 'project',
                'active' => 1,
                'order' => 4,
            ),
        ));
        
        
    }
}