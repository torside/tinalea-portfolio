<?php

use Illuminate\Database\Seeder;

class WorkSectionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('work_sections')->delete();
        
        \DB::table('work_sections')->insert(array (
            0 => 
            array (
                'idWorkSection' => 1,
                'workSectionName' => 'Animals',
                'workSectionSlug' => 'animals',
                'active' => 1,
                'order' => 3,
            ),
            1 => 
            array (
                'idWorkSection' => 2,
                'workSectionName' => 'Weddings',
                'workSectionSlug' => 'weddings',
                'active' => 1,
                'order' => 4,
            ),
            2 => 
            array (
                'idWorkSection' => 3,
                'workSectionName' => 'Portraits',
                'workSectionSlug' => 'portraits',
                'active' => 1,
                'order' => 5,
            ),
            3 => 
            array (
                'idWorkSection' => 4,
                'workSectionName' => 'Product',
                'workSectionSlug' => 'product',
                'active' => 1,
                'order' => 1,
            ),
            4 => 
            array (
                'idWorkSection' => 5,
                'workSectionName' => 'Travel',
                'workSectionSlug' => 'travel',
                'active' => 1,
                'order' => 2,
            ),
            5 => 
            array (
                'idWorkSection' => 6,
                'workSectionName' => 'Life',
                'workSectionSlug' => 'life',
                'active' => 1,
                'order' => 6,
            ),
            6 => 
            array (
                'idWorkSection' => 7,
                'workSectionName' => 'People',
                'workSectionSlug' => 'people',
                'active' => 1,
                'order' => 7,
            ),
            7 => 
            array (
                'idWorkSection' => 8,
                'workSectionName' => 'Snapshots',
                'workSectionSlug' => 'snapshots',
                'active' => 1,
                'order' => 8,
            ),
            8 => 
            array (
                'idWorkSection' => 9,
                'workSectionName' => 'Other',
                'workSectionSlug' => 'other',
                'active' => 1,
                'order' => 9,
            ),
        ));
        
        
    }
}