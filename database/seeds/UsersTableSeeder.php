<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'torside',
                'email' => 'torside@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$ciHu/fO1MYh.KOsjjbDXFu56t8dNkprQKwMwVjnNVckTPXKb.Wtuy',
                'remember_token' => 'bj0yfQAWMz6XgJ8vTmQF4fNvMgy3ASwlalLjmF6H7v8Zur7HDuJunOaw551d',
                'created_at' => '2020-04-13 17:22:13',
                'updated_at' => '2020-04-13 17:22:13',
            ),
        ));
        
        
    }
}