<?php

use App\Repositories\Watermark\Watermark;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateWatermarkTable extends Migration
{
    public function up()
    {
        Schema::create(Watermark::TABLE_NAME, function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_slovak_ci';

            $table->bigIncrements(Watermark::PRIMARY_KEY);
            $table->string(Watermark::COLUMN_IMAGE_FILE_NAME, 255);
            $table->boolean(Watermark::COLUMN_ACTIVE)->default(true);
            $table->unsignedMediumInteger(Watermark::COLUMN_ORDER)->default(0);
            $table->string(Watermark::COLUMN_TITLE, 255)->default('');
            $table->timestamp(Watermark::COLUMN_CREATED_AT)->nullable();
            $table->timestamp(Watermark::COLUMN_UPDATED_AT)->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists(Watermark::TABLE_NAME);
    }
}
