<?php

use App\Repositories\AppState\AppState;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateAppStateTable extends Migration
{
    public function up()
    {
        Schema::create(AppState::TABLE_NAME, function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_slovak_ci';

            $table->bigIncrements(AppState::PRIMARY_KEY);
            $table->string(AppState::COLUMN_APP_STATE_KEY, 255)->index();
            $table->string(AppState::COLUMN_APP_STATE_VALUE, 255);

            $table->unique([
                AppState::COLUMN_APP_STATE_KEY,
                AppState::COLUMN_APP_STATE_VALUE
            ]);
        });
    }

    public function down()
    {
        Schema::dropIfExists(AppState::TABLE_NAME);
    }
}
