<?php

use App\Repositories\WorkSection\WorkSection;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateWorkSectionTable extends Migration
{
    public function up()
    {
        Schema::create(WorkSection::TABLE_NAME, function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_slovak_ci';

            $table->bigIncrements(WorkSection::PRIMARY_KEY);
            $table->string(WorkSection::COLUMN_WORK_SECTION_NAME, 255);
            $table->string(WorkSection::COLUMN_WORK_SECTION_SLUG, 255)->index();
            $table->boolean(WorkSection::COLUMN_ACTIVE)->default(true);
            $table->unsignedMediumInteger(WorkSection::COLUMN_ORDER)->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists(WorkSection::TABLE_NAME);
    }
}
