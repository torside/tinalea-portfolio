<?php

use App\Repositories\Watermark\Watermark;
use App\Repositories\Work\Work;
use App\Repositories\WorkWatermark\WorkWatermark;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateWorkWatermarkTable extends Migration
{
    public function up()
    {
        Schema::create(WorkWatermark::TABLE_NAME, function (Blueprint $table) {
            $table->bigIncrements(WorkWatermark::PRIMARY_KEY);
            $table->bigInteger(Work::PRIMARY_KEY);
            $table->bigInteger(Watermark::PRIMARY_KEY);
            $table->string(WorkWatermark::COLUMN_POSITION, 80);
            $table->unsignedSmallInteger(WorkWatermark::COLUMN_OFFSET_X);
            $table->unsignedSmallInteger(WorkWatermark::COLUMN_OFFSET_Y);
            $table->unsignedTinyInteger(WorkWatermark::COLUMN_SIZE);
        });
    }

    public function down()
    {
        Schema::dropIfExists(WorkWatermark::TABLE_NAME);
    }
}
