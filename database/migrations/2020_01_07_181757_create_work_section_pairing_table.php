<?php

use App\Repositories\Work\Work;
use App\Repositories\WorkSection\WorkSection;
use App\Repositories\WorkSectionPairing\WorkSectionPairing;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateWorkSectionPairingTable extends Migration
{
    public function up()
    {
        Schema::create(WorkSectionPairing::TABLE_NAME, function (Blueprint $table) {
            $table->bigIncrements(WorkSectionPairing::PRIMARY_KEY);
            $table->unsignedBigInteger(Work::PRIMARY_KEY);
            $table->unsignedBigInteger(WorkSection::PRIMARY_KEY);

            $table->unique([
                Work::PRIMARY_KEY,
                WorkSection::PRIMARY_KEY
            ]);
        });
    }

    public function down()
    {
        Schema::dropIfExists(WorkSectionPairing::TABLE_NAME);
    }
}
