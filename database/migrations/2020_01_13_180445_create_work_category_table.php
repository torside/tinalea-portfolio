<?php

use App\Repositories\WorkCategory\WorkCategory;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateWorkCategoryTable extends Migration
{
    public function up()
    {
        Schema::create(WorkCategory::TABLE_NAME, function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_slovak_ci';

            $table->bigIncrements(WorkCategory::PRIMARY_KEY);
            $table->string(WorkCategory::COLUMN_WORK_CATEGORY_NAME, 255);
            $table->string(WorkCategory::COLUMN_WORK_CATEGORY_SLUG, 255)->index();
            $table->boolean(WorkCategory::COLUMN_ACTIVE)->default(true);
            $table->unsignedMediumInteger(WorkCategory::COLUMN_ORDER)->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists(WorkCategory::TABLE_NAME);
    }
}
