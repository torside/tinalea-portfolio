<?php

use App\Repositories\Work\Work;
use App\Repositories\WorkCategory\WorkCategory;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateWorkTable extends Migration
{
    public function up()
    {
        Schema::create(Work::TABLE_NAME, function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_slovak_ci';

            $table->bigIncrements(Work::PRIMARY_KEY);
            $table->string(Work::COLUMN_IMAGE_FILE_NAME, 255);
            $table->boolean(Work::COLUMN_ACTIVE)->default(true);
            $table->unsignedMediumInteger(Work::COLUMN_ORDER)->default(999);
            $table->string(Work::COLUMN_TITLE, 255)->default('');
            $table->string(Work::COLUMN_DESCRIPTION, 255)->default('');
            $table->unsignedBigInteger(WorkCategory::PRIMARY_KEY)->nullable()->index();
            $table->timestamp(Work::COLUMN_CREATED_AT)->nullable();
            $table->timestamp(Work::COLUMN_UPDATED_AT)->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists(Work::TABLE_NAME);
    }
}
