<?php

return [
    'work' => [
        'xs' => 160,
        'md' => 480,
        'xl' => 960,
        //'xxxl' => 2000
    ],
    'watermarks' => [
        '2000' => 2000
    ]
];