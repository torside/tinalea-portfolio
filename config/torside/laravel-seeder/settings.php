<?php

return [
    'models' => [
        App\Repositories\User\User::class,
        App\Repositories\WorkCategory\WorkCategory::class,
        App\Repositories\WorkSection\WorkSection::class
    ]
];