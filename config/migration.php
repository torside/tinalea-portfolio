<?php

return [
    'models' => [
        App\Repositories\Watermark\Watermark::class,
        App\Repositories\Work\Work::class,
        App\Repositories\WorkCategory\WorkCategory::class,
        App\Repositories\WorkSection\WorkSection::class,
        App\Repositories\WorkSectionPairing\WorkSectionPairing::class,
        App\Repositories\WorkWatermark\WorkWatermark::class
    ]
];